## Système de design de l'État - Plugin DSFR pour le CMS Wordpress

### Disclaimer

**ATTENTION**: Ce design système a uniquement vocation à être utilisé pour des sites officiels de l'état.
Son but est de rendre la parole de l'état clairement identifiable. [Consulter les CGU](https://www.systeme-de-design.gouv.fr/utilisation-et-organisation/perimetre-d-application)

### Version du plugin

| historique des versions | date       | version DSFR | version Wordpress | commentaire |
|-------------------------|------------|--------------|-------------------|-------------|
| 1.629.18.1.0            | 01/01/2022 | 1.8          | 6.2.1             |             |
| 2.629.185.1.0           | 01/07/2022 | 1.8.5        | 6.2.9             |             |
| 2.631.1101.3.2          | 15/09/2023 | 1.10.1       | 6.3.1             |             |

Chef de projet / développeur : Mikaël Folio
Testeur : Jean-Philippe Simonnet

### Compatibilité Wordpress

Le plugin DSFR est compatible avec [Wordpress 6.3.1](https://fr.wordpress.org/download/).

### À propos de notre projet

Notre projet vise à mettre en place les mécanismes du Système de Design de l'État français (DSFR) sur un site Wordpress en émulant un thème. Nous cherchons à créer une expérience utilisateur cohérente et accessible, en conformité avec les normes de l'État français. Ce projet est le fruit d'un travail collaboratif entre développeurs, designers et contributeurs passionnés par l'amélioration de l'accessibilité et de l'ergonomie des sites web.

### Qu'est-ce que le DSFR ?

Le DSFR est le volet numérique de la marque de l'État français. Il offre une cohérence graphique et une meilleure expérience utilisateur à travers tous les sites de l'État. Le DSFR comprend un ensemble de composants réutilisables qui répondent à des standards et à une gouvernance spécifiques, et qui peuvent être assemblés pour créer des sites Internet accessibles et ergonomiques. Pour en savoir plus, consultez le [site officiel du DSFR](https://www.systeme-de-design.gouv.fr/).

#### Fondamentaux

Les fondamentaux du DSFR comprennent les couleurs, les grilles et les icônes. Ces éléments de base sont essentiels pour construire votre site ou votre application. Ils sont conçus pour être facilement réutilisables et pour fournir une cohérence visuelle à travers votre site.

#### Composants

Le DSFR propose 30 composants et modèles documentés. Ces composants sont disponibles sur Sketch et Figma, ainsi qu'en HTML/CSS via NPM. Ils comprennent des éléments tels que des boutons, des formulaires, des cartes, des menus, et bien plus encore. Chaque composant est conçu pour être facilement réutilisable et pour répondre à des standards d'accessibilité et d'ergonomie.

### Comment nous utilisons le DSFR dans notre projet

Dans notre projet, nous utilisons une variété de composants DSFR pour construire notre site. Ces composants sont documentés et disponibles sur Sketch et Figma, ainsi qu'en HTML/CSS via NPM. Nous avons choisi ces composants pour leur accessibilité, leur ergonomie et leur conformité avec les normes de l'État français. Chaque composant utilisé dans notre projet est décrit en détail dans la section "Composants utilisés" de ce wiki.

#### L'utilisation des composants du DSFR dans le plugin

Le DSFR propose une variété de composants réutilisables qui sont conçus pour être accessibles, ergonomiques et conformes aux normes de l'État français. Voici quelques-uns des composants clés (x composant mis en place dans ce plugin): 

- [ ] [Accordéon - Accordion](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/accordeon/)
- [ ] [Ajout de fichier - Upload](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/ajout-de-fichier)
- [ ] [Alerte - Alert](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/alerte)
- [ ] [Badge - Badge](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/badge)
- [X] [Bandeau d'information importante - Notice](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/bandeau-d-information-importante)
- [X] [Barre de recherche - Search](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/barre-de-recherche)
- [ ] [Bouton - Button](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/bouton)  
- [ ] [Groupe de boutons - Button group](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/groupe-de-boutons)
- [ ] [Bouton FranceConnect](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/bouton-franceconnect)
- [ ] [Bouton radio - Radio button](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/bouton-radio)
- [ ] [Bouton radio riche - Radio rich](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/bouton-radio-riche)
- [ ] [Case à cocher - Checkbox](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/case-a-cocher)
- [ ] [Carte - Card](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/carte)
- [ ] [Champ de saisie - Input](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/champ-de-saisie)
- [ ] [Citation - Quote](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/citation)
- [ ] [Contenu médias - Content](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/contenu-medias)
- [X] [En-tête - Header](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/en-tete)
- [X] [Fil d'Ariane - Breadcrumb](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/fil-d-ariane)
- [X] [Gestionnaire de consentement - Consent banner](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/gestionnaire-de-consentement)
- [ ] [Indicateur d'étapes - Stepper](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/indicateur-d-etapes)
- [ ] [Infobulle - Tooltip](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/infobulle)
- [ ] [Interrupteur - Toggle](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/interrupteur)
- [X] [Lettre d'information et réseaux sociaux - Newsletter and follow us](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/lettre-d-information-et-reseaux-sociaux)
- [ ] [Lien - Link](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/lien)
- [X] [Lien d'évitement - Skiplink](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/lien-d-evitement)
- [ ] [Liste déroulante - Select](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/liste-deroulante)
- [ ] [Menu latéral - Sidemenu](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/menu-lateral)
- [ ] [Mise en avant - Callout](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/mise-en-avant)
- [ ] [Mise en exergue - Highlight](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/mise-en-exergue)
- [ ] [Modale - Modal](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/modale)
- [ ] [Mot de passe - Password](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/mot-de-passe)
- [X] [Navigation principale - Navigation](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/navigation-principale)
- [ ] [Onglet - Tab](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/onglet)
- [ ] [Pagination - Pagination](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/pagination)
- [X] [Paramètre d'affichage - Display](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/parametre-d-affichage)
- [X] [Partage - Share](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/partage)
- [X] [Pied de page - Footer](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/pied-de-page)
- [X] [Retour en haut de page - Back to top](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/retour-en-haut-de-page)
- [ ] [Sélecteur de langue - Translate](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/selecteur-de-langue)
- [ ] [Sommaire - Summary](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/sommaire)
- [ ] [Tableau - Table](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/tableau)
- [ ] [Tag - Tag](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/tag)
- [ ] [Téléchargement de fichier - Download](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/telechargement-de-fichier)
- [ ] [Transcription - Transcription](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/transcription)
- [ ] [Tuile - Tile](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/tuile)

Les composants non cochés seront intégrés ultérieurement sous forme de modèles (dans la mesure du possible) pour être utilisés en éditorial.

#### Installation et configuration du thème(en cours)

- **Installation** : [Voir la page du Wiki destiné à cet effet](#)
- **Configuration** : [Voir la page du Wiki destiné à cet effet](#)

### Contribuer à notre projet

Nous accueillons les contributions de tous ceux qui sont intéressés par notre projet. Si vous souhaitez contribuer, veuillez consulter nos directives de contribution. Nous avons une variété de tâches disponibles pour les contributeurs, allant de la documentation à la programmation. Toute aide est appréciée et nous sommes impatients de travailler avec vous !

Remontée de bugs ou demande de d'ajouts ou de modifications : [Tickets](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/cgdd-public/bun-poleweb/systeme-de-design-de-l-etat/cms-worpdress/-/issues)

### Communauté

La communauté du DSFR est une ressource précieuse pour les développeurs et les designers. Elle est composée de professionnels qui apportent leur expertise et leur soutien à ceux qui utilisent le DSFR. En faisant partie de cette communauté, vous pouvez obtenir de l'aide, partager vos idées, et contribuer à l'amélioration du DSFR.

### Ressources

- [Site officiel du DSFR](https://www.systeme-de-design.gouv.fr/)
- [Documentation des composants DSFR](https://www.systeme-de-design.gouv.fr/utilisation-et-organisation)
- [Communauté DSFR](https://www.systeme-de-design.gouv.fr/communaute)
- [CMS Wordpress](https://fr.wordpress.org/download/)
- [Wiki de ce thème](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/cgdd-public/bun-poleweb/systeme-de-design-de-l-etat/cms-worpdress/-/wikis/home)

### Contact

Si vous avez des questions ou des commentaires sur notre projet, n'hésitez pas à nous contacter. Vous pouvez nous [Envoyer un email](mailto:webcgdd@developpement-durable.gouv.fr?Subject=Plugin/Thème%20DSFR%20CMS%20Wordpress).

### Site de test 

Vous pouvez consulter notre [site de test](https://dsfr.digital-vision-pro.com).