<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_locale_css' ) ):
    function chld_thm_cfg_locale_css( $uri ){
        if ( empty( $uri ) && is_rtl() && file_exists( get_template_directory() . '/rtl.css' ) )
            $uri = get_template_directory_uri() . '/rtl.css';
        return $uri;
    }
endif;
add_filter( 'locale_stylesheet_uri', 'chld_thm_cfg_locale_css' );

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array( 'dsfr-style' ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10 );
         
if ( !function_exists( 'child_theme_configurator_css' ) ):
    function child_theme_configurator_css() {
        if ( !file_exists( trailingslashit( get_stylesheet_directory() ) . 'dsfr/dsfr.css' ) ):
            wp_deregister_style( 'dsfr-style' );
            wp_register_style( 'dsfr-style', trailingslashit( get_template_directory_uri() ) . 'dsfr/dsfr.css' );
        endif;
        wp_enqueue_style( 'chld_thm_cfg_child', trailingslashit( get_stylesheet_directory_uri() ) . 'style.css', array( 'chld_thm_cfg_parent' ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'child_theme_configurator_css', 10 );



function wpdocs_enqueue_custom_admin_style_child() {		
    wp_enqueue_style( 'style-admin', '/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/admin/css/style-admin.css', false, generate_unique_id() ); //1.0.35
    wp_enqueue_script( 'dsfr-wordpress-admin-theme-default-custom-script', '/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/admin/js/admin-theme-default-custom-script.js', array( 'jquery' ), generate_unique_id(), false ); //13.5
    if (is_form_config_page()) {
        wp_enqueue_style( 'custom-admin-style', '/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/admin/css/admin-theme-style.css', false, generate_unique_id() ); //1.0.35
        wp_enqueue_script( 'dsfr-wordpress-admin-theme-script', '/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/admin/js/admin-theme-script.js', array( 'jquery' ), generate_unique_id(), false ); //13.5
    }
}
add_action( 'admin_enqueue_scripts', 'wpdocs_enqueue_custom_admin_style_child' );
// END ENQUEUE PARENT ACTION
