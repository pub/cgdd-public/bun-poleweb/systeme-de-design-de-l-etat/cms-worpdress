<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */
?>
<!DOCTYPE html>
<script>
//determines if the user has a set theme, permet d'éviter le scintillement lors d'une bascule darkmode
    function detectColorScheme(){
        var theme="<?php echo get_option('dsfr_parametre_affichage_defaut'); ?>";    //default to dark

        //local storage is used to override OS theme settings
        if(localStorage.getItem("scheme")){
            if(localStorage.getItem("scheme") == "dark"){
                theme = "dark";
            }
            else{
                theme = "light";
            }
        } else if(!window.matchMedia) {
            //matchMedia method not supported
            theme = "<?php echo get_option('dsfr_parametre_affichage_defaut'); ?>";
            return false;
        } else if(window.matchMedia("(prefers-color-scheme: dark)").matches) {
            //OS theme setting detected as dark
            theme = "<?php echo get_option('dsfr_parametre_affichage_defaut'); ?>";
        }

        //dark theme preferred, set document with a `data-theme` attribute
        
            document.documentElement.setAttribute("data-fr-scheme", theme);
            document.documentElement.setAttribute("data-fr-theme", theme);

    }
    detectColorScheme();
</script>
<?php 
	$darkmode = (((get_option('masquer_parametres_affichage') !== false) AND (get_option('masquer_parametres_affichage') === "non")) ? get_option('dsfr_parametre_affichage_defaut') : 'light'); 
	$mise_en_berne = (((get_option('dsfr_drapeau_en_berne') !== false) AND (get_option('dsfr_drapeau_en_berne') === 'oui')) ? "data-fr-mourning" : "");
?>
<html <?php language_attributes(); ?> dir="ltr" class="ltr js fr" data-fr-scheme="<?php echo $darkmode; ?>" data-fr-theme="<?php echo $darkmode; ?>" data-fr-js="true" <?php echo esc_attr( $mise_en_berne); ?>  >
<head>
	<base href="<?php echo get_option('siteurl');?>" />
	<link rel="icon" href="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/favicon.png" type="image/png">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	
	<?php get_template_part( 'template-parts/header/site-skiplinks' ); ?>
	<?php get_template_part( 'template-parts/header/site-header' ); ?>

	<div id="content" class="site-content">
		<div class="fr-container fr-py-2w fr-px-2w" id="container">	





		





