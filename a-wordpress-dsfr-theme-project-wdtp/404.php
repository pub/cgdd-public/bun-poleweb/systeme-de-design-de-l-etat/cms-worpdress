<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */

get_header(); ?>

	<div  class="content-area">
		<main id="contenu" class="site-main">
			<div class="fr-py-16v" style="padding-top:0 !important;">
				<div class="fr-grid-row ">
					<div class='fr-col-12 fr-col-md-8'> 
						<div>
							<h1 class="fr-h1">Page non trouvée</h1>
							<p class="fr-text--sm">ERREUR 404</p>
							<p>La page que vous recherchez est introuvable. Excusez-nous pour la gêne occasionnée.</p>
							<p class="fr-text--sm">Si vous avez tapé l'adresse web dans le navigateur, vérifiez qu'elle est correcte. La page n'est peut-être plus disponible.<br>Dans ce cas, pour continuer votre visite, vous pouvez consulter notre page d'accueil, ou effectuer une recherche avec notre moteur de recherche en haut de page.<br>Sinon, contactez-nous pour que l'on puisse vous rediriger vers la bonne information.</p>
						</div>
					</div>
					<div class="fr-col-12 fr-col-md-4">
						<img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/error_img.png" width="308" alt="">
					</div>
					<a href="<?php echo home_url(); ?>" class="fr-btn fr-btn--secondary">Accueil <span class="maison fr-icon-home-4-fill"></span></a>&nbsp;<a href="<?php echo get_option('dsfr_form_contact'); ?>" class="fr-btn fr-btn--secondary">Nous contacter<span class="maison fr-icon-mail-line"></span></a>
				</div>
			</div>
		</main>
	</div>
</div>
<?php
get_footer(); ?>
