# A Wordpress DSFR Theme Project WDTP

WDPT (`A Wordpress DSFR Theme Project`) est un thème pour Wordpress basé sur le Système de Design de l'État (DSFR).
Ce document fait la présentation du thème `A Wordpress DSFR Theme Project` et décrit les étapes des évolutions possibles dans cette version.

|historique des versions|date      |commentaire        |
|-----------------------|----------|-------------------|
|1.0.0                  |01/05/2023|1ère version majeur|
|2.0.0                  |15/06/2023|2nde version majeur|

Rédacteur / développeur plugin : Mikaël Folio

Relecteur/testeur : Jean-Philippe Simonnet


Ce projet est un thème Wordpress conçu pour être conforme aux normes du système de Design de l'état Français (DSFR). Il est conçu pour être facile à personnaliser et à utiliser, et il est livré avec des fonctionnalités telles que:

- [x] Prise en charge de la personnalisation du thème via l'interface d'administration de Wordpress
- [x] Intégration avec les outils de développement courants tels que GitLab et Visual Studio Code
- [x] Conforme aux normes d'accessibilité WCAG 2.1 AA
- [x] Prise en charge de la personnalisation du thème via l'interface d'administration de Wordpress
- [x] Intégration avec les outils de développement courants tels que GitLab et Visual Studio Code
- [x] Ajout d'un fil d'ariane

## Installation

L'installation se comporte comme l'installation de n'importe quel autre thème:

1. Clonez le référentiel GitLab sur votre ordinateur
2. Copiez le dossier `a-wordpress-dsfr-theme-project-wdtp` dans le répertoire `wp-content/themes` de votre installation Wordpress.
3. Activez le thème enfant dans l'interface d'administration de Wordpress.
4. Des champs sont créés dans la table options de Wordpress à l'activation et désinstallés de la table si vous désactivez le thème
5. Personnalisez le thème selon vos besoins en utilisant l'interface d'administration de Wordpress ou en modifiant directement les fichiers du thème.

## Contribution

Si vous souhaitez contribuer à ce projet, vous pouvez le faire en créant une demande de fusion sur GitLab. Veuillez vous assurer que votre code est conforme aux normes de codage du projet et qu'il est accompagné de tests appropriés.

## License

Ce projet est sous licence publique générale GNU ( GPL ). Veuillez consulter le fichier LICENSE.md pour plus d'informations.

## Todo
- [ ] Prise en charge de la traduction 
- [ ] Passage à la version supérieur du DSFR (actuel : 1.8.5) 

## Version 
- `2.0` : Fourniture d'une interface IHM permettant la configuration du DSFR. Tous les éléments suivants sont intégrés
    - header DSFR
    - Footer DSFR
    - Menu + configurateur de menu(simple, déroulant, mégamenu)
    - Masquer ou afficher le bandeau d'alerte + configuration du message d'alerte
    - Mise en berne du drapeau
    - Masquer ou afficher le titre et/ou slogan
    - Masquer globalement les commentaires des articles
    - Page d'accueil en H1
    - Masquer ou afficher la recherche
    - Masquer ou afficher second logo
    - Masquer ou afficher Partage sur les réseaux sociaux
    - Masquer ou afficher newsletter
    - Masquer ou afficher la liste d'accès rapide
    - Masquer ou afficher les paramètres d'affichage (mode dark)
    - Masquer ou afficher les cookies
    - Masquer ou afficher les partenaires
    - Masquer ou afficher la liste de liens
    - Masquer ou afficher le bouton Back to top
    - Masquer ou afficher le message de licence + configuration du message à afficher
    - Administration des liens écosystème constitutionnel
    - Administration de liens légaux
    - Masquer ou afficher le bouton des cookies dans le footer
    - Masquer ou afficher le bouton Paramètres d'affichage dans le footer
    - Suppression du fichier qui servait à la configuration en version 1.x
- `1.0` : la configuration de cette version s'effectue à l'aide du fichier d'options. L'automatisation via un formulaire est prévue en version 2
    - header DSFR
    - Footer DSFR
    - Menu + configurateur de menu(simple, déroulant, mégamenu)
    - Configurateur liste de liens
    - Masquer ou afficher le bandeau d'alerte + configuration du message d'alerte
    - Mise en berne du drapeau
    - Masquer ou afficher le titre et/ou slogan
    - Masquer globalement les commentaires des articles
    - Page d'accueil en H1
    - Masquer ou afficher la recherche
    - Masquer ou afficher second logo
    - Masquer ou afficher Partage sur les réseaux sociaux
    - Masquer ou afficher newsletter
    - Masquer ou afficher la liste d'accès rapide
    - Masquer ou afficher les paramètres d'affichage (mode dark)
    - Masquer ou afficher les cookies
    - Masquer ou afficher les partenaires
    - Masquer ou afficher la liste de liens
    - Masquer ou afficher le bouton Back to top
    - Masquer ou afficher le message de licence + configuration du message à afficher
    - Administration des liens écosystème constitutionnel
    - Administration de liens légaux
    - Masquer ou afficher le bouton des cookies dans le footer
    - Masquer ou afficher le bouton Paramètres d'affichage dans le footer
