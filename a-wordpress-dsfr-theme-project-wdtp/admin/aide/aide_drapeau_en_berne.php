<?php
    echo '
    <ul class="drapeau_en_berne_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Mettre le drapeau en berne"</h3></li>
        <li>La mise en berne des drapeaux se décide en France dans des circonstances particulières, comme un deuil national ou une tragédie, qu’il s’agisse d’un événement ayant eu lieu en France ou à l’étranger.</li>
        <li>Le design system de l’état intègre cette fonctionnalité, et vous pouvez mettre en berne la Marianne sur votre site.<br>La mise en berne de la Marianne concerne le header et le footer du site.</li>
        <li>Voici comment se présente cette fonctionnalité:</li>
        <li>Marianne en berne :<br><br><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/marianne_en_berne.jpg" width="250"/></li>
        <li>Marianne normal :<br><br><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/marianne_normale.jpg" width="250"/></li>
    </ul>';
    
?>