<?php
    echo '<div class="aide_onglet_navigateur aide_address_email_responsable aide_dsfr_addresse_email_responsable overlay">
            <ul>
                <li class="paddinghelpli"><h3 class="colorFFF">Aide sur le champ "Adresse email du responsable"</h3></li>
                <li class="colorFFF paddinghelpli textsize15">Ce champ correspond à l\'adresse email de l\'administrateur du site.</li>                
                <li class="colorFFF paddinghelpli textsize15">Ce champ est obligatoire et une adresse email valide est requise.</li> 
            </ul>
        </div>';
?>