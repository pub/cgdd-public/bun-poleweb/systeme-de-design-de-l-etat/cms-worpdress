<?php
    echo '<div class="aide_onglet_navigateur aide_meta_description aide_dsfr_meta_description overlay">
            <ul>
                <li class="paddinghelpli"><h3 class="colorFFF">Aide sur le champ "Meta description"</h3></li>
                <li class="colorFFF paddinghelpli textsize15">Ce champ permet de définir la méta "Description" dans le header. Si une de vos pages n\'a pas de méta description, celle-ci sera utilisé par défaut.</li>                
            </ul>
        </div>';
?>