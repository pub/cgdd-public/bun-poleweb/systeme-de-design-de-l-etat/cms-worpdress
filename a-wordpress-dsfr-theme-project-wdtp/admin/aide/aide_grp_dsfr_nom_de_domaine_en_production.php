<?php
    echo '<div class="aide_onglet_navigateur aide_nom_de_domaine_en_production aide_dsfr_nom_de_domaine_en_production overlay">
            <ul>
                <li class="paddinghelpli"><h3 class="colorFFF">Aide sur le champ "Domaine en production"</h3></li>
                <li class="colorFFF paddinghelpli textsize15">Ce champ est étroitement lié avec la gestion des statistiques. En effet, sans ce champ, vos statistiques si vous disposez de plusieurs instances/serveurs (developpement, preprod et production par exempe) seront comptabilisés sur chacun de ces serveurs. Ce champ permet donc de ne comptabiliser les statistiques uniquement sur le serveur de production</li>
                <li class="colorFFF paddinghelpli textsize15">Pour cela, vous devez saisir pour ce champ, l\'adresse / URL de votre domaine de production (sans https// et sans la barre de fin.</li>
                <li class="colorFFF paddinghelpli textsize15">Exemple: www.notre-environnement.gouv.fr</li>
            </ul>
        </div>';
?>