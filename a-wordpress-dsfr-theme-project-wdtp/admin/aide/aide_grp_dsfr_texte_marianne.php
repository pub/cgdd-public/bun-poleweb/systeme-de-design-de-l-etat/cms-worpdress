<?php
    echo '<div class="aide_onglet_navigateur aide_texte_marianne aide_dsfr_texte_marianne overlay">
            <ul>
                <li class="paddinghelpli"><h3 class="colorFFF">Aide sur le champ "Texte de la Marianne"</h3></li>
                <li class="colorFFF paddinghelpli textsize15">Avec ce champ, vous pouvez définir Le nom de l\'institution qui apparaît sous la Marianne.</li>                
                <li class="colorFFF paddinghelpli textsize15">Exemple:<br><br>
                    <ul>
                        <li class="colorFFF paddinghelpli textsize15">Header: <br><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/logo_header.jpg" width="200"/></li>
                        <li class="colorFFF paddinghelpli textsize15">Footer: <br><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/logo_footer.jpg" width="400"/></li>
                    </ul>
                </li> 
            </ul>
        </div>';
?>