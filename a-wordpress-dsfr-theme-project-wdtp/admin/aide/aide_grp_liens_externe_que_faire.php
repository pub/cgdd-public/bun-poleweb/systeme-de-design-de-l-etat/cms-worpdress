<?php
    echo '<div class="aide_onglet_navigateur aide_liens_externe_que_faire aide_liens_externe_que_faire overlay">
            <ul>
                <li class="paddinghelpli"><h3 class="colorFFF">Aide sur le champ "Qui décide de l\'ouverture des liens externes?"</h3></li>
                <li class="colorFFF paddinghelpli textsize15">L\'utilisation ou le choix du "target=\'_blank\'" fait toujours débat. Laissez le choix ou non à l\'utilisateur, telle est la question.</li>                
                <li class="colorFFF paddinghelpli textsize15">Nous ne sommes pas là pour vous imposer des choix, aussi pour l\'ouverture des liens externes, ca sera votre choix:<br>- Automatique pour ouvrir une nouvelle fenêtre ou onglet.<br>- Utilisateur pour laisser le choix à l\'utilisateur d\'ouvrir ou non une nouvelle fenêtre.</li> 
            </ul>
        </div>';
?>