<?php
    echo '
    <ul class="masquer_commentaires_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Masquer les commentaires"</h3></li>
        <li>Par défaut, à l’installation de Wordpress, les commentaires sont activés pour les articles.</li>
        <li>Si vous ne souhaitez pas permettre les commentaires sur les articles mis en ligne, il vous suffit d’activer cette fonctionnalité.</li>
        <li>Vous pouvez retrouver la personnalisation de cette fonctionnalité en cliquant dans votre menu "<a href="/wp-admin/themes.php">Commentaires</a>".</li>ormale.jpg" width="250"/></li>
    </ul>';
    
?>