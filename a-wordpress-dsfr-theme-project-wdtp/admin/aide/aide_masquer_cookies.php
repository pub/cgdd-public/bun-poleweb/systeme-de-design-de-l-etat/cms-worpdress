<?php
    echo '
    <ul class="masquer_cookies_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Masquer les cookies"</h3></li>
        <li>Les cookies, un véritable enjeu et une obligation pour votre site.</li>
        <li>Avec ce thème, nous utilisons "<a href="https://tarteaucitron.io/fr/" target="_blank" title="Tarteaucitron - Gestionnaire de cookies - nouvelle fenêtre">Tarteaucitron</a>" comme gestionnaire de cookies.</li>
        <li>Dans le formulaire de configuration, vous pouvez ajouter vos <a href="https://tarteaucitron.io/fr/install/" title="Consulter la documentation de Tarteaucitron - nouvelle page" target="_blank">cookies dans la liste</a> et vous pouvez aussi choisir de modifier le texte de la popup d’entrée des cookies (popup apparaissant la première fois que vous arrivez sur le site).</li>
        <li>
        <span class="bold">Si un gestionnaire de cookies est obligatoire pour mon site, alors pourquoi avoir donné la possibilité de le désactiver ?</span><br>Très bonne question. La réponse est simple, il est vrai que c’est une obligation pour nos sites d’avoir un gestionnaire de cookies, mais ce n’est pas de notre ressort d’imposer un applicatif spécifique. Vous restez maître de vos choix.</li>
        <li>Popup :<br><br><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/cookies_consent.jpg" width="250"/><br><br>
        - Panneau de gestion des cookies :<br><br><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/panneau_cookies.jpg" width="250"/>
        </li>        
    </ul>';
    
?>