<?php
    echo '
    <ul class="masquer_liens_legaux_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Masquer la description dans le footer"</h3></li>
        <li>Vous avez la possibilité d\'ajouter la description de votre site dans le footer</li>
        <li>Le champ dans cette commande permet de modifier le contenu de cette description.</li>
    </ul>';
    
?>