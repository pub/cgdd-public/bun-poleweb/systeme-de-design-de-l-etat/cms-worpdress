<?php
    echo '
    <ul class="masquer_haut_de_page_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Masquer Haut de page"</h3></li>
        <li>Le "Back to top" est un lien ou un bouton qui, lorsqu’il est cliqué, ramène l’utilisateur au haut de la page Web.</li>
        <li>C’est particulièrement utile sur des pages longues pour éviter de devoir défiler tout le chemin du bas au haut de la page.</li>
        <li>En activant cette fonctionnalité, vous mettez un bouton "Back to top" sur votre site</li>
        <li><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/backtotop.jpg" width="250"/><br><br></li>        
    </ul>';
    
?>