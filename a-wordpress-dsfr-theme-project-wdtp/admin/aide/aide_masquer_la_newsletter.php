<?php
    echo '
    <ul class="masquer_la_newsletter_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Masquer la newsletter"</h3></li>
        <li>Cette fonctionnalité permet d’intégrer une interface de newsletter à votre site sous la forme d’un formulaire ou d’un lien renvoyant vers votre newsletter externe.</li>
        <li>Toutefois, il faut bien prendre en compte que ce thème ne gère pas du tout le mécanisme de gestion d’une newsletter (si vous utilisez le formulaire au lieu du lien). Il vous incombera donc de mettre en place ce mécanisme (Le formulaire est opérationnel)</li>
        <li>Le bloc "Newsletter" partage à 50% son emplacement avec le bloc "Réseaux sociaux". Si un seul des 2 blocs est activé, l’emplacement de celui qui est activé utilisera les 100% de la largeur.</li>
        <li>Newsletter :<br><br><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/newsletter.jpg" width="250"/></li>
    </ul>';
    
?>