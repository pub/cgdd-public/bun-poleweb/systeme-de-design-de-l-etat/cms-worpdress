<?php
    echo '
    <ul class="masquer_le_titre_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Masquer le titre"</h3></li>
        <li>En activant cette fonctionnalité, vous aurez la possibilité de modifier directement le titre du site.</li>
        <li>Cette fonctionnalité est à l’identique de "Apparence / <a href="/wp-admin/customize.php?return=%2Fwp-admin%2Fadmin.php%3Fpage%3Dform_config">Personnaliser</a>" puis "Identité du site". Nous l’avons intégré car elle fait partie des critières du Design sytem de l’état.</li>
    </ul>';
    
?>