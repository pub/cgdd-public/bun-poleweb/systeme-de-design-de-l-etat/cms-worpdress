<?php
    echo '
    <ul class="masquer_liens_ecosysteme_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Masquer les liens écosystème constitutionnel"</h3></li>
        <li>Vous trouverez ci-dessous, les 4 liens de références de l\'écosystème institutionnel. Il s\'agit de liens obligatoires.</li>
        <li>Vous ne pourrez pas masquer ces liens.</li>
        <li>Seuls la mise à jour des liens est autorisée.</li>
        <li><a href="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/ecosysteme.jpg" target="_blank" title="afficher l\'image - nouvelle fenêtre"><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/ecosysteme.jpg" width="250" /></a><br>
        </li>
    </ul>';
    
?>