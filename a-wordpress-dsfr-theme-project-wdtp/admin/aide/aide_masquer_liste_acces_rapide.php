<?php
    echo '
    <ul class="masquer_liste_acces_rapide_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Liste d’accès rapide"</h3></li>
        <li>Cette fonctionnalité vous permet d’afficher des liens dits "rapides" qui vont permettre à vos visiteurs d’accéder rapidement à vos pages.</li>
        <li>Au total pour ce thème et en conformité avec le système de design de l’état, vous êtes limité à 3 liens rapides, 2 si vous utilisez la fonctionnalité Darkmode.</li>
        <li>
            Votre fonctionnalité apparaît comme ceci si vous n’utilisez pas la fonctionnalité Darkmode<br><br>
            <img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/liens_rapides.jpg" width="250"/>
        </li>
        <li>
            Où comme ceci avec la fonctionnalité Darkmode<br><br>
            <img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/liens_rapides2.jpg" width="250"/>
        </li>

    </ul>';
    
?>