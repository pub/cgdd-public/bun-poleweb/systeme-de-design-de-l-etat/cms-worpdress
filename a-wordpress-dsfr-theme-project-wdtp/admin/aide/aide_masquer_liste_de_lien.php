<?php
    echo '
    <ul class="masquer_liste_de_lien_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Masquer la liste de liens"</h3></li>
        <li>Le footer mis en place par le Système de Design de l’État intègre différents blocs et le bloc "Liste de liens" en fait partie.</li>
        <li>En activant cette fonctionnalité, vous allez pouvoir définir un emplacement pour vos liste de liens (voir image ci-dessous). </li>
        <li>Vous pouvez créér manuellement vos listes ou utiliser notre configurateur spécial d’éléments.</li>
        <li><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/liste_de_liens.jpg" width="250"/><br><br></li>        
    </ul>';
    
?>