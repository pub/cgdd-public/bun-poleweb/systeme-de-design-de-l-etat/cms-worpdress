<?php
    echo '
    <ul class="masquer_logo_secondaire_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Masquer le logo secondaire"</h3></li>
        <li>Le système de design de l’état vous permet d’avoir un logo identaire pour votre site. Ce logo sera placé à la droite de la Marianne.</li>
        <li>Le système de design de l’état impose des dimensions spécifiques pour ce logo, et celles-ci seront automatiquement appliquées par ce thème.</li>
        <li>Il est conseillé d’avoir déjà ce logo aux bonnes dimensions (ce qui vous facillitera la vie).</li>
        <li>Je vous invite à consulter la <a href="https://www.gouvernement.fr/charte/charte-graphique-les-fondamentaux/introduction" target="_blank" ttle="Charte graphique de l\État - nouvelle fenêtre">charte graphique de l’État</a> pour plus d’informations sur le logo identaire.</li>
        <li>Second logo header :<br><br><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/header_second_logo.jpg" width="250"/></li>
        <li>Second logo footer :<br><br><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/footer_second_logo.jpg" width="250"/></li>
        <li>Vous pourrez charger votre logo directement à partir de la médiathèque Wordpress.</li>
    </ul>';
    
?>