<?php
    echo '
    <ul class="masquer_menu_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Masquer le menu"</h3></li>
        <li>Cette fonctionnalité va vous permettre d’ajouter différents types de menus sur votre site.<br>
        <img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/menu.jpg" width="250"/><br></li>
        <li>Voici les différents types de menu disponibles:<br><br>
        - Menu simple<br>
        <img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/menusimple.jpg" /><br><br>
        - Menu déroulant<br>
        <img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/menuderoulant.jpg" width="250"/><br><br>
        - Menu Mégamenu<br>
        <img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/megamenu.jpg" width="250"/><br><br>
        </li>
        <li>Le générateur d’éléments à votre disposition vous permet de générer très facilement un de ces menus</li>
        <li>Bien entendu, pour cette version du thème, nous nous appuyons sur la base CSS du Sytème de Design de l’État. Dans une prochaine version, il sera possible d’intégrer votre propre code et CSS.</li>
    </ul>';
    
?>