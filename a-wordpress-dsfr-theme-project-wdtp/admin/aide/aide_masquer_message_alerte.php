<?php
    echo '
    <ul class="masquer_message_alerte_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Masquer le message d’alerte"</h3></li>
        <li>Le message d’alerte ou plus exactement le bandeau d’information importante permet aux utilisateurs de voir ou d’accéder à une information importante et temporaire. </li>
        <li><span class="bold">Mise en garde du Système de Design de l’État</span> : Le bandeau d’information importante doit être utilisé uniquement pour une information primordiale et temporaire. (Une utilisation excessive ou continue risque de “noyer” le composant). </li>
        <li><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/messagealerte.jpg" width="250"/></li>
    </ul>';
    
?>