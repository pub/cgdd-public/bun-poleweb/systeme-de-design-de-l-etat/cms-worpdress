<?php
    echo '
    <ul class="masquer_page_contact_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Masquer la page de contact"</h3></li>
        <li>Attention, il faut toujours avoir une page de contact quel que soit le site.</li>
        <li>Cette fonctionnalité sert prioritairement à gérér les différents éléments de votre page contact (titre du lien contact, ou le placer ...)</li>
        <li>Dans des rares cas, vous serez peut être amené à cacher provisoirement la page contact, cette fonctionnalité le permet.</li>
    </ul>';
    
?>