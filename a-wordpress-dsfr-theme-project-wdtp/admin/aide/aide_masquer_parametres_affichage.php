<?php
    echo '
    <ul class="masquer_parametres_affichage_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Masquer les paramètres d’affichage"</h3></li>
        <li>En activant cette fonctionnalité, vous ajoutez la fonctionnalité "Darkmode" sur votre site.</li>
        <li>Le bloc "Paramètres d’affichage" partage son emplacement avec la liste d’accès rapide. Donc si vous activez cette fonctionnalité, celle-ci occupera un des slots prévus pour le bloc "Liste d’accès rapide" (3 au total sur ce thème).</li>
        <li>2 modes sont disponibles sur le configurateur DSFR:<br>
        - Mode "Dark" :<br><br><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/darkmode.jpg" width="250"/><br><br>
        - Mode "Light" :<br><br><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/lightmode.jpg" width="250"/>
        </li>
        <li>Pour le configurateur, nous utilisons 2 des modes disponibles (Dark et Light).<br>Le troisième mode (Système) est utilisé dans la popup "Paramètres d’affichage" en frontend.<br>Je vous invite à consulter la <a href="https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/parametre-d-affichage" target="_blank" title="Système de design de l’état - Paramètres d’affichage - nouvelle page">page du Système de design de l’état</a> concernant cette fonctionnalité.</li>
    </ul>';
    
?>