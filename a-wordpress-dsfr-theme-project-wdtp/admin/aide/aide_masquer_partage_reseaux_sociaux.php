<?php
    echo '
    <ul class="masquer_partage_reseaux_sociaux_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Masquer le partage sur les réseaux sociaux"</h3></li>
        <li>Grâce à cette fonctionnalité, vous pouvez choisir d’afficher individuellement les liens vers vos réseaux sociaux (Facebook, Twitter, Linkedin...)</li>
        <li>Le bloc "Réseaux sociaux" partage à 50% son emplacement avec le bloc "Newsletter". Si un seul des 2 blocs est activé, l’emplacement de celui-ci utilise les 100% de la largeur.</li>
        <li>Vous pourrez modifier le nom du bloc (Ex: Suivez nous sur ...).<br>Vous devez indiquer l’url du réseau social à afficher. Laisser vide les champs des réseaux sociaux à ne pas afficher (les champs de ce bloc ne sont pas obligatoires).</li>
        <li>Réseaux sociaux :<br><br><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/reseaux_sociaux.jpg" width="250"/></li>
    </ul>';
    
?>