<?php
    echo '
    <ul class="masquer_partenaires_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Masquer les partenaires"</h3></li>
        <li>Le footer mis en place par le Système de Design de l’État intègre différents blocs et le bloc "Les partenaires" en fait partie.</li>
        <li>En activant cette fonctionnalité, vous allez pouvoir définir un emplacement pour vos partenaires. </li>
        <li>Ces partenaires seront de deux types:<br>
        - Partenaires principaux (2 maximum  possible sur ce thème)<br>
        - Partenaires secondaires (4 maximum possible sur ce thème)
        </li>
        <li><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/partenaires.jpg" width="250"/><br><br></li>        
    </ul>';
    
?>