<?php
    echo '
    <ul class="masquer_recherche_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Masquer la recherche"</h3></li>
        <li>Le bloc de recherche qui se trouve dans le header fait partie du "Tout configurable" de notre théme.</li>
        <li>En activant cette fonctionnalité(bouton en couleur verte), vous pouvez masquer le bloc de recherche </li>
        <li>
            Ce qui donne lorsque vous utilisez la fonctionnalité:<br><br>
            <img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/masquer_recherche_config.jpg" width="250"/><br><br>
            <img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/recherche_masquee.jpg" width="250"/>
        </li>
    </ul>';
    
?>