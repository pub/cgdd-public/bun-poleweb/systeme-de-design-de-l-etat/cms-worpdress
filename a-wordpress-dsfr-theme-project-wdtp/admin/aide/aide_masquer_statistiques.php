<?php
    echo '
    <ul class="masquer_statistiques_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Masquer les statistques"</h3></li>
        <li>Les statistiques, un autre enjeu stratégique de plus pour les sites web.</li>
        <li>Mais quoi plus énervant de voir ses statistiques polluées par des données n’appartenant pas à son site. Comment faire pour ne pas avoir ses statistiques de production polluées par celle du site de préprod? Nous avons la solution.</li>
        <li>Pour cela, nous avons créé un champ "Nom du domaine en production" et le script de statistiques ne sera appelé que si votre nom de domaine actuelle correspond au nom de domaine de production.</li>
    </ul>';
    
?>