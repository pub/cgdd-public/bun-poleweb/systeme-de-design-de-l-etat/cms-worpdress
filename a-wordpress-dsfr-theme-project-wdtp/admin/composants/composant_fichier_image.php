<li class="_masquer_logo_secondaire">
    <label for="dsfr_afficher_selection_type_logo">Type image</label><br>							
    <select name="dsfr_afficher_selection_type_logo" id="dsfr_afficher_selection_type_logo">
        <option value="fichier">fichier</option>
        <option value="code">code</option>
    </select>
</li>		
<li class="_masquer_logo_secondaire image_second_logo suppression-image">
    <label for="select-image-button">Logo secondaire</label><br>
    <input class="loadImage" type="button" id="select-image-button" class="button" value="Sélectionner une image" class="button" value="Sélectionner une image"  data-idimage="image_second_logo" data-urlimage="image_second_logo2" data-imagepreview="selected-image-preview" name="select-image-button" data-imagetype="fichier" required="required"/>
    <input type="hidden" id="image_second_logo" name="image_second_logo" value="">
    <input type="hidden" id="image_second_logo2" name="image_second_logo2" value=""><br>	
    <br><br><img id="selected-image-preview" src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/noimage.png" alt="Image sélectionnée">					         
</li>
<li class="_masquer_logo_secondaire alternative_logo_secondaire suppression-image">
    <label for="alternative_logo_secondaire">Texte alternatif logo secondaire<span class="obligatoire"> (obligatoire)</span>:</label>
    <input class="width100" type="text" name="alternative_logo_secondaire" id="alternative_logo_secondaire" value="" required="required"/>
</li>
<li class="_masquer_logo_secondaire suppression-image">
    <hr class="hr width100">
    <span class="bold">Notes:</span>
</li>
<li class="_masquer_logo_secondaire alternative_logo_secondaire suppression-image">Vous pouvez choisir une image ou la télécharger dans la bibliothèque d'images de Wordpress.</li>
								