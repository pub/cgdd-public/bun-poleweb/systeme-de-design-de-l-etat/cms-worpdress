
<?php
    function generateUuidV4() {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
    echo'
	<style>
	input[type="color"], input[type="date"], input[type="datetime-local"], input[type="datetime"], input[type="email"], input[type="month"], input[type="number"], input[type="password"], input[type="search"], input[type="tel"], input[type="text"], input[type="time"], input[type="url"], input[type="week"], select, textarea {
		box-shadow: 0 0 0 transparent;
		border-radius: 4px;
		border: 1px solid #8c8f94;
		background-color: #fff;
		color: #2c3338;
	  }
.affichage_generateur{
	margin-top:1.5rem;
}
.dontshow{
	display: none !important;
}
input.button-primary {
	background: #2271b1;
	border-color: #2271b1;
	color: #fff;
	text-decoration: none;
	text-shadow: none;
	display: inline-block;
	font-size: 13px;
	line-height: 2.15384615;
	min-height: 30px;
	margin: 0;
	padding: 0 10px;
	cursor: pointer;
	border-width: 1px;
	border-style: solid;
	-webkit-appearance: none;
	border-radius: 3px;
	white-space: nowrap;
	box-sizing: border-box;
	width:80% !important;
}


.button-home {
	padding: 1rem;
	background-color: white;
	color: black;
	display: inline-block;
	text-decoration: none;
}
.button-home:hover {
	background-color: #f6f6f6;
}
a:active, a:hover {
	color: #135e96;
}
.fr-nav__btn{    
	padding: 1rem;
	margin: 0;
	width: auto;
	height: 100%;
	min-height: 3.5rem;
	font-weight: normal;
	border: none;
	max-width: fit-content; 
	background: #fff;
	padding: 0 1rem;
  }
  .fr-nav__btn:hover{  
	background: #f6f6f6;
  }
  .fr-menu__list {
	margin: 0;
	padding: 0.5rem 0 1.5rem;
  }
  .fr-menu__list li a{
	text-decoration: none;
  }
  .fr-menu__list {
	width: 20rem;
	pointer-events: auto;
	padding: 0;
	margin-bottom: 2rem;
	background-color: #fff;
	--idle: transparent;
	--hover: #f6f6f6;
	--active: #ededed;
	box-shadow: inset 0 1px 0 0 #e3e3fd;
}
  button.fr-nav__btn.menu_deroulant_btn::after {
	--icon-size: 1rem;
	flex: 0 0 auto;
	display: inline-block;
	vertical-align: calc((0.75em - var(--icon-size)) * 0.5);
	background-color: currentColor;
	width: var(--icon-size);
	height: var(--icon-size);
	-webkit-mask-size: 100% 100%;
	mask-size: 100% 100%;
	-webkit-mask-image: url(/plugins/auto/z-dsfr/design_system_admin/dsfr/icons/system/arrow-down-s-line.svg);
	mask-image: url(/plugins/auto/z-dsfr/design_system_admin/dsfr/icons/system/arrow-down-s-line.svg);
	--icon-size: 1rem;
	content: "";
	margin-left: 0.5rem;
	margin-right: 0;
	transition: transform 0.3s;
}
.fr-menu {
	margin: -4px -1rem;
	padding: 4px 1rem;
	width: auto;
}
nav.fr-nav{
  position: relative;
  z-index: auto;
}
.fr-menu {
	pointer-events: none;
	position: absolute;
	top: 93%;
	z-index: calc(2000 + 1000);
	filter: drop-shadow(0 2px 6px rgba(0, 0, 18, 0.16));
}
.fr-nav__link, .fr-nav__btn {
	padding: 0.75rem 0;
	font-size: 1rem;
	line-height: 1.5rem;
	text-align: left;
	--hover-tint: #f6f6f6;
	--active-tint: #ededed;
	color: #161616;
	padding: 0 1rem;
}
.fr-nav__link {
	display: block;
}
.fr-nav__link, .fr-nav__btn {
	font-size: 0.875rem;
	line-height: 1.5rem;
}
.fr-menu .fr-nav__link {
	padding: 0.75rem 1rem;
	box-shadow: 0 calc(-1rem - 1px) 0 -1rem #ddd;
}
.fr-menu .fr-nav__link:hover {
	background: #f6f6f6;
}

.fr-nav__btn[aria-expanded=true] {
	color: #000091;
	background-color: #e3e3fd;
	--idle: transparent;
	--hover: #c1c1fb;
	--active: #adadf9;
	padding: 0 1rem;
}
.fr-nav__btn:hover {
	background: #f6f6f6;
	color:#000;
}
#lien_categorie::before{
	content:none;
}
</style>    
	<ul class="affichage_generateur">
		<li>
			<ul class="form_configuration">
				<li class="simple_icone_flag_remove show_menu_type_select affichage_generateur">
					<select name="list-menu-type" id="list-menu-type" data-emplacement="elementFormUl" data-type_generateur="menu_deroulant" data-element_parent="elementFormUl">
						<option value="menu">Lien simple</option>
						<option value="menu_deroulant">Menu déroulant</option>
						<option value="megamenu" selected="selected">Mégamenu</option>
					</select>
				</li>
				<li class="lesresultats">

				</li>
			</ul>
		<li>
	</ul>
	<ul class="form_commandes affichage_generateur">
		<li>
			<input type="hidden" name="type_generateur" id="type_generateur" value="'.$_GET['type_generateur'].'" />
			<input type="hidden" name="element_parent" id="element_parent" value="'.$_GET['element_parent'].'" />
			<label for="menu-megamenu-titre"><span class="redbold">*</span> Nom du menu megamenu:</label><br>
			<input type="text" name="menu-megamenu-titre" id="menu-megamenu-titre" class="width100 text" placeholder="Ex: Mégamenu" required="required"><br><br><label for="contenu-megamenu-titre">Titre du megamenu:</label><br>
			<input type="text" name="contenu-megamenu-titre" id="contenu-megamenu-titre" class="width100 text" placeholder="Ex: Ceci est le tire du contenu de mon mégamenu"><br><br>
			<label for="entete_titre_megamenu">Entête titre Mégamenu ?</label><br>
			<select name="entete_titre_megamenu" id="entete_titre_megamenu" class="text">
				<option value="h1">h1</option>
				<option value="h2">h2</option>
				<option value="h3">h3</option>
				<option value="h4">h4</option>
				<option value="h5">h5</option>
				<option value="h6">h6</option>
				<option value="div">div</option>
				<option value="p">p</option>
			</select>
		</li>
		<li>
			<label for="menu-megamenu-description">Description du megamenu:</label><br>
			<textarea name="menu-megamenu-description" id="menu-megamenu-description" class="width100 text" placeholder="Ma description pour mon megamenu" rows="4"></textarea>
		</li>
		<li>
			<label for="categorie-megamenu-titre">Titre de l\'url de catégorie:</label><br>
			<input type="text" name="categorie-megamenu-titre" id="categorie-megamenu-titre" class="width100 text" placeholder="Ex: Catégorie"><br><br>
			<label for="categorie-megamenu-url">Lien de l\'url de catégorie</label><br>
			<input type="text" name="categorie-megamenu-url" id="categorie-megamenu-url" class="width100 text" placeholder="Ex: https:// ou Lien directe">
		</li>
		<li>
			<label for="menu-megamenu-image">Lien de l\'image:</label><br>
			<input type="text" name="menu-megamenu-image" id="menu-megamenu-image" class="width100 text" placeholder="Lien de l\'image">
		</li>
	</ul>
	<ul class="affichage_generateur">
		<li>
			<ul class="form_special_numeric">
				<li>
					<label for="nombre_de_menus_megamenu">Nombre de blocs de menus ?</label><br>
					<select name="nombre_de_menus_megamenu" id="nombre_de_menus_megamenu" class="text" data-emplacement="megamenuBlock" data-menutype="megamenu">
						<option value="0" selected="selected">0</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
					</select>
				</li>
			</ul>
		</li>
	</ul>
	<ul class="form_commandes gestion_megamenuBlock affichage_generateur">
		
	</ul>

				
				<style>
					#modal .modal-content{width: 70% !important;}
                    .dropdown-menu__image {
                    margin: 0;
                    width: 30%;
                    }
                    .fr-nav__list > *:first-child:nth-last-child(2) ~ *, .fr-nav__list > *:first-child:nth-last-child(3) ~ *, .fr-nav__list > *:first-child:nth-last-child(4) ~ * {
                    margin-left: 1.25rem;
                    }
                    .dropdown-menu__image img, img.arrondi {
                    border-radius: 1rem;
                    max-width: 100%;
                    }
                    input[required]::placeholder {
                    font-weight: bold;
                    }
                    fieldset {
                    border: 1px solid #ccc;
                    padding: 10px;
                    min-height: 72px;
                    margin-top: -80px;
                    }
                    .fr-nav__btn[aria-expanded="true"] {
                    color: #000091;
                    background-color: #e3e3fd;
                    --idle: transparent;
                    --hover: #c1c1fb;
                    --active: #adadf9;
                    padding: 0 1rem;
                    }
                    .fr-nav__link, .fr-nav__btn {
                    font-size: 0.875rem;
                    line-height: 1.5rem;
                    }
                    .fr-nav__link, .fr-nav__btn {
                    padding: 0.75rem 0;
                    font-size: 1rem;
                    line-height: 1.5rem;
                    text-align: left;
                    --hover-tint: #f6f6f6;
                    --active-tint: #ededed;
                    color: #161616;
                    padding: 0 1rem;
                    }
                    .fr-nav__btn {
                    padding: 1rem;
                    margin: 0;
                    width: auto;
                    height: 100%;
                    min-height: 3.5rem;
                    font-weight: normal;
                    border: none;
                    max-width: fit-content;
                    background: #fff;
                    background-color: rgb(255, 255, 255);
                    padding: 0 1rem;
                    }
                    .megamenu_content{
                    position: relative;
                    border: 1px solid #ccc;
                    width: 100%;
                    background: #fff;
                    left:0;
                    }
                    img.arrondi{
                    border:1px solid #ddd;
                    max-height:250px;
                    }
                    .megamenu_html{
                    min-width: 950px;
                    }
                    .megamenu_content{
                    padding-left: 2rem;
                    }
                    h2 {
                    font-size: 2rem;
                    line-height: 2.5rem;
                    }
                    .fr-mb-3v, .fr-my-3v {
                    margin-bottom: 0.75rem !important;
                    margin-top: 0.75rem !important;
                    }
                    .fr-mt-4v, .fr-mt-2w, .fr-my-4v, .fr-my-2w {
                    margin-top: 1rem !important;
                    }
                    .colonnes_menu{
                    margin-top: 2rem;
                    }
                    .colonens_menu h3{
                    font-weight: 600;
                    }
                    .colonnes_menu .fr-mega-menu__list a{
                    padding:0
                    }
                    .colonnes_menu .fr-mega-menu__list{
                    margin-top: .5rem;
                    }
                    .megamenu_close{
                    background: #fff;
                    border: none;
                    }
                    .megamenu_close::after{
                    content: " X";
                    }
                </style>';
?>