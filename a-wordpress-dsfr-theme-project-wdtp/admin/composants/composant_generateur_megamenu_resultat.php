
<?php
//echo '<pre>';print_r($_GET);echo '</pre>';

    echo '<legend>Résultat obtenu</legend>
    <ul id="resultats">
        <li>
            <button class="fr-nav__btn" aria-expanded="true" aria-controls="resultat_menu_megamenu" data-fr-js-collapse-button="true" id="titre_bouton_megamenu" type="button">' .$_GET['megamenuTitreMenu'] . '</button>
            <ul class="megamenu_content">
                <li>
                    <ul class="form_configuration">
                        <li></li>
                        <li style="text-align: end;margin-right: 2rem;"><button aria-expanded="true" aria-controls="megamenu_content" data-fr-js-collapse-button="true" class="megamenu_close">Fermer</button></li>
                    </ul>
                </li>
                <li>
                    <ul class="form_configuration">
                        <li class="megamenu_image">
                            <img loading="lazy" class="arrondi" src="' . $_GET['$megamenuImage'] . '" alt="">
                        </li>
                        <li>
                            <ul class="megamenu_html">
                                <li class="megamenu_titre_comtent">
                                    <' . $_GET['megamenuEntete'] . ' id="titre_megamenu">' . $_GET['megamenuTitreAccueil'] . '</' . $_GET['megamenuEntete'] . '>
                                    <div class="fr-my-3v" id="description_de_megamenu">' . $_GET['megamenuDescription'] . '</div>
                                    <a id="lien_categorie" class="fr-link fr-fi-arrow-right-line fr-link--icon-right" href="' . $_GET['megamenuUrlCategorie'] . '">' . $_GET['megamenuTitreCategorie'] . '</a>
                                </li>
                                <li>
                                    <ul class="form_commandes colonnes_menu">';
                                    if ($_GET['nombreBlocMenu'] > 0) {
                                        for ($i = 1; $i <= $_GET['nombreBlocMenu']; $i++) {
                                            echo '
                                            <li class="blocmenu' . $i . '">
                                                <h3 class="fr-mega-menu__category">' . $_GET['titrebloc'.$i] . '</h3>
                                                <ul class="fr-mega-menu__list" id="sousmenus_' . $i .'">';
                                                
                                                for ($j = 1; $j <= $_GET['nombreDeSousMenus'.$i]; $j++) {
                                                    echo '<li><a class="fr-nav__link" href="' . $_GET['url'.$j.'menu'.$i] . '" target="_self">' . $_GET['titre'.$j.'menu'.$i] . '</a></li>';
                                                }                                                
                                            echo '
                                                </ul>
                                            </li>';
                                        }
                                    }
                                    echo'    
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
    </ul>';
    ?>