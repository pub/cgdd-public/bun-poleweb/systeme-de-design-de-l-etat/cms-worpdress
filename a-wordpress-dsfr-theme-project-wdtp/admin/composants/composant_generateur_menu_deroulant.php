
<?php
    function generateUuidV4() {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
    echo'
    <ul class="affichage_generateur">
        <li>
            <ul class="form_configuration">
                <li class="simple_icone_flag_remove show_menu_type_select affichage_generateur">
                    <select name="list-menu-type" id="list-menu-type" data-emplacement="elementFormUl" data-type_generateur="menu_deroulant" data-element_parent="'.$_GET['element_parent'].'">
                        <option value="menu">Lien simple</option>
                        <option value="menu_deroulant" selected="selected">Menu déroulant</option>
                        <option value="megamenu">Mégamenu</option>
                    </select>
                </li>
                <li class="les-resultats">

                </li>
            </ul>
        </li>
    </ul>
    <ul class="affichage_generateur">
        <li id="simple_menu_nav">
            <input type="hidden" name="type_generateur" id="type_generateur" value="'.$_GET['type_generateur'].'" />
            <input type="hidden" name="element_parent" id="element_parent" value="'.$_GET['element_parent'].'" />
            <style>
            input[type="color"], input[type="date"], input[type="datetime-local"], input[type="datetime"], input[type="email"], input[type="month"], input[type="number"], input[type="password"], input[type="search"], input[type="tel"], input[type="text"], input[type="time"], input[type="url"], input[type="week"], select, textarea {
                box-shadow: 0 0 0 transparent;
                border-radius: 4px;
                border: 1px solid #8c8f94;
                background-color: #fff;
                color: #2c3338;
              }

            .affichage_generateur{
                margin-top:1.5rem;
            }
            .dontshow{
                display: none !important;
            }
            input.button-primary {
                background: #2271b1;
                border-color: #2271b1;
                color: #fff;
                text-decoration: none;
                text-shadow: none;
                display: inline-block;
                font-size: 13px;
                line-height: 2.15384615;
                min-height: 30px;
                margin: 0;
                padding: 0 10px;
                cursor: pointer;
                border-width: 1px;
                border-style: solid;
                -webkit-appearance: none;
                border-radius: 3px;
                white-space: nowrap;
                box-sizing: border-box;
                width:80% !important;
            }

            
            .button-home {
                padding: 1rem;
                background-color: white;
                color: black;
                display: inline-block;
                text-decoration: none;
            }
            .button-home:hover {
                background-color: #f6f6f6;
            }
            a:active, a:hover {
                color: #135e96;
            }
            </style>
            <ul>
                <li>
                    <label for="menu_deroulant_titre"><span class="redbold">*</span> Titre du menu ?</label><br>
                    <input type="text" name="menu_deroulant_titre" id="menu_deroulant_titre" placeholder="Ex : Menu 1" class="width100 text"  required="required">
                </li>
                <li>
                    <ul class="form_commandes">
                        <li>
                            <label for="nombre_de_sousmenus_deroulant">Nombre de sous-menus ?</label><br>
                            <select name="nombre_de_sousmenus_deroulant" id="nombre_de_sousmenus_deroulant" data-elementname="menuderoulant_titre"  data-emplacement="ajoutSousMenus">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                            </select>
                        </li>
                        <li></li>
                        <li>
                            <label for="menu_deroulant_icon">Icône du menu:</label><br>
                            <input type="text" name="menu_deroulant_icon" id="menu_deroulant_icon" class="width100 text" placeholder="Voir définition icones du DSFR">
                        </li>
                    </ul>
                </li>
                <li class="gestion_ajoutSousMenus" style="display:grid;">
                    <ul class="form_configuration ajoutSousMenus_1" style="order:1;">
                        <li>
                            <label for="menuderoulant_titre_1"><span class="redbold">*</span> Titre sousmenu 1:</label><br>
                            <input type="text" name="menuderoulant_titre_1" id="menuderoulant_titre_1" placeholder="Ex : SousMenu 1" class="width100 text"  required="required">
                        </li>
                        <li>
                            <label for="menuderoulant_url_1"><span class="redbold">*</span> Url sous-menu 1:</label><br>
                            <input type="text" name="menuderoulant_url_1" id="menuderoulant_url_1" placeholder="Ex : https://lien_ssmenu1 ou /ssmenu1" class="width100 text" required="required">
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
    </ul>';
?>