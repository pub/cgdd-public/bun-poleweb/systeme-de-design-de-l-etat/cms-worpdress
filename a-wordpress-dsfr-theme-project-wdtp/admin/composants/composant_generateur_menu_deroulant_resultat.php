
<?php
    
    echo'
    <ul class="affichage_generateur">
        <li id="simple_menu_nav">
            <style>
                input.text, textarea, select {
                position: relative;
                z-index: 2;
                padding: var(--spip-form-input-padding-y) var(--spip-form-input-padding-x);
                margin: 0;
                width: 100%;
                border: 1px solid var(--spip-form-input-border-color);
                border-radius: var(--spip-form-input-border-radius);
                background-color: var(--spip-color-white);
                transition: box-shadow 0.1s;
            }
            .affichage_generateur{
                margin-top:1.5rem;
            }
            .dontshow{
                display: none !important;
            }
            input.button-primary {
                background: #2271b1;
                border-color: #2271b1;
                color: #fff;
                text-decoration: none;
                text-shadow: none;
                display: inline-block;
                font-size: 13px;
                line-height: 2.15384615;
                min-height: 30px;
                margin: 0;
                padding: 0 10px;
                cursor: pointer;
                border-width: 1px;
                border-style: solid;
                -webkit-appearance: none;
                border-radius: 3px;
                white-space: nowrap;
                box-sizing: border-box;
                width:80% !important;
            }

            
            .button-home {
                padding: 1rem;
                background-color: white;
                color: black;
                display: inline-block;
                text-decoration: none;
            }
            .button-home:hover {
                background-color: #f6f6f6;
            }
            a:active, a:hover {
                color: #135e96;
            }
            .fr-nav__btn{    
                padding: 1rem;
                margin: 0;
                width: auto;
                height: 100%;
                min-height: 3.5rem;
                font-weight: normal;
                border: none;
                max-width: fit-content; 
                background: #fff;
                padding: 0 1rem;
              }
              .fr-nav__btn:hover{  
                background: #f6f6f6;
              }
              .fr-menu__list {
                margin: 0;
                padding: 0.5rem 0 1.5rem;
              }
              .fr-menu__list li a{
                text-decoration: none;
              }
              .fr-menu__list {
                width: 20rem;
                pointer-events: auto;
                padding: 0;
                margin-bottom: 2rem;
                background-color: #fff;
                --idle: transparent;
                --hover: #f6f6f6;
                --active: #ededed;
                box-shadow: inset 0 1px 0 0 #e3e3fd;
            }
              button.fr-nav__btn.menu_deroulant_btn::after {
                --icon-size: 1rem;
                flex: 0 0 auto;
                display: inline-block;
                vertical-align: calc((0.75em - var(--icon-size)) * 0.5);
                background-color: currentColor;
                width: var(--icon-size);
                height: var(--icon-size);
                -webkit-mask-size: 100% 100%;
                mask-size: 100% 100%;
                -webkit-mask-image: url(/plugins/auto/z-dsfr/design_system_admin/dsfr/icons/system/arrow-down-s-line.svg);
                mask-image: url(/plugins/auto/z-dsfr/design_system_admin/dsfr/icons/system/arrow-down-s-line.svg);
                --icon-size: 1rem;
                content: "";
                margin-left: 0.5rem;
                margin-right: 0;
                transition: transform 0.3s;
            }
            .fr-menu {
                margin: -4px -1rem;
                padding: 4px 1rem;
                width: auto;
            }
            nav.fr-nav{
              position: relative;
              z-index: auto;
            }
            .fr-menu {
                pointer-events: none;
                position: absolute;
                top: 93%;
                z-index: calc(2000 + 1000);
                filter: drop-shadow(0 2px 6px rgba(0, 0, 18, 0.16));
            }
            .fr-nav__link, .fr-nav__btn {
                padding: 0.75rem 0;
                font-size: 1rem;
                line-height: 1.5rem;
                text-align: left;
                --hover-tint: #f6f6f6;
                --active-tint: #ededed;
                color: #161616;
                padding: 0 1rem;
            }
            .fr-nav__link {
                display: block;
            }
            .fr-nav__link, .fr-nav__btn {
                font-size: 0.875rem;
                line-height: 1.5rem;
            }
            .fr-menu .fr-nav__link {
                padding: 0.75rem 1rem;
                box-shadow: 0 calc(-1rem - 1px) 0 -1rem #ddd;
            }
            .fr-menu .fr-nav__link:hover {
                background: #f6f6f6;
            }
            
            .fr-nav__btn[aria-expanded=true] {
                color: #000091;
                background-color: #e3e3fd;
                --idle: transparent;
                --hover: #c1c1fb;
                --active: #adadf9;
                padding: 0 1rem;
            }
            .fr-nav__btn:hover {
                background: #f6f6f6;
                color:#000;
            }
            .fr-nav__btn:hover {
                background: #f6f6f6;
              }
              .fr-nav__btn {
                padding: 1rem;
                margin: 0;
                width: auto;
                height: 100%;
                min-height: 3.5rem;
                font-weight: normal;
                border: none;
                max-width: fit-content;
                background: #fff;
                background-color: rgb(255, 255, 255);
                padding: 0 1rem;
              }
            </style>
            <nav class="fr-nav" id="resultats" role="navigation" aria-label="Menu principal" data-fr-js-navigation="true">
                <ul class="fr-nav__list">
                    <li class="fr-nav__item" data-fr-js-navigation-item="true">
                        <button class="fr-nav__btn menu_deroulant_btn" aria-expanded="false" aria-controls="menu-646d00ee27a64" data-fr-js-collapse-button="true">'. $_GET['menuDeroulantTitre'] .'</button>
                        <div class="fr-collapse fr-menu" id="menu-646d00ee27a64" data-fr-js-collapse="true" style="display:none;">
                            <ul class="fr-menu__list">';                            
                            for ($i = 1; $i <= $_GET['nombreMenu']; $i++) {
                                echo' <li><a class="fr-nav__link" href="'. $_GET['menuderoulant_url_'.$i] .'">'. $_GET['menuderoulant_titre_'.$i] .'</a></li>';
                            }                              
                            echo'    
                            </ul>
                        </div>
                    </li>
                </ul>
            </nav>
        </li>
    </ul>';

?>