<?php
    echo '
        <nav class="fr-nav" id="resultats" role="navigation" aria-label="Menu principal" data-fr-js-navigation="true">
            <ul class="fr-nav__list">
                <li class="fr-nav__item" data-fr-js-navigation-item="true">
                    <a href="' .  $_GET['menuSimpleUrl']. '" class="button-home">' .((strlen($_GET['menuSimpleIcon']) > 0) ? '<img src="'.$_GET['menuSimpleIcon'].'" width="200" alt="'.$_GET['menuSimpleTitre'].'" />' : $_GET['menuSimpleTitre']). '</a>
                </li>
            </ul>
        </nav>
    ';
?>