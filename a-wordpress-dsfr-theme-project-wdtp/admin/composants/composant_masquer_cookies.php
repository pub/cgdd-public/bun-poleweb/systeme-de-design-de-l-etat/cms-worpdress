<?php
echo '
    <li class="_masquer_cookies">
        <label for="afficher_bouton_cookies">Afficher le lien des cookies dans le footer</label><br>
        <select name="afficher_bouton_cookies" id="afficher_bouton_cookies">
            <option value="non">non</option>
            <option value="oui" selected="selected">oui</option>        
        </select>
    </li>
    <li class="_masquer_cookies">
        <div class="editer editer_liste_des_cookies obligatoire saisie_textarea editer_even">
            <label class="editer-label" for="liste_des_cookies">Liste des cookies?</span></label>
            <textarea class="width100" name="liste_des_cookies" id="liste_des_cookies" rows="4" cols="33" data-parent="masquer_cookies"></textarea>
        </div>
    </li>
    <li class="_masquer_cookies">
        <div class="editer editer_modifier_texte_cookies saisie_selection editer_odd">
            <label class="editer-label" for="modifier_texte_cookies">Modifier le texte des cookies?</label>
            <select class="width100" name="modifier_texte_cookies" id="modifier_texte_cookies" data-information="select2options" data-remove="non" data-parent="masquer_cookies" required="required"  data-link="'.$_GET['datalink'].'">
                <option value="">Sélectionnez une option</option>
                <option value="oui" selected="selected">oui</option>
                <option value="non">Non</option>
            </select>
        </div>
    </li>
    <li class="_masquer_cookies gestion_masquer_cookies_select2options">
        <div class="editer editer_titre_texte_cookies obligatoire saisie_input editer_even">
            <label class="editer-label" for="titre_texte_cookies">Titre du texte des cookies<span class="obligatoire"> (obligatoire)</span></label>
            <input type="text" name="titre_texte_cookies" class="text width100" id="titre_texte_cookies" value="" required="required"  data-parent="masquer_cookies">
        </div>
    </li>
    <li class="_masquer_cookies gestion_masquer_cookies_select2options">
        <div class="editer editer_texte_cookies obligatoire saisie_textarea editer_odd">
            <label class="editer-label" for="texte_cookies">Texte des cookies<span class="obligatoire"> (obligatoire)</span></label>
            <textarea class="width100" name="texte_cookies" id="texte_cookies" rows="6" cols="33" required="required"  data-parent="masquer_cookies"></textarea>
        </div>
    </li>';  
?>