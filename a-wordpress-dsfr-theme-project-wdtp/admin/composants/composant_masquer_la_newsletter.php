<?php
echo '
    <li class="_masquer_la_newsletter">
        <div class="editer editer_titre_form_newsletter saisie_input editer_odd">
            <label class="editer-label" for="champ_titre_form_newsletter">Titre newsletter:</label>            
            <input type="text" name="titre_form_newsletter" class="text width100" id="champ_titre_form_newsletter" value="" placeholder="Proposer un titre">
        </div>
    </li>
    <li class="_masquer_la_newsletter">
        <div class="editer editer_slogan_form_newsletter saisie_input editer_even">
            <label class="editer-label" for="champ_slogan_form_newsletter">Slogan de la newsletter</label>
            <input type="text" name="slogan_form_newsletter" class="text width100" id="champ_slogan_form_newsletter" placeholder="Proposer un slogan" value="">
        </div>
    </li>
    <li class="_masquer_la_newsletter">
        <div class="editer editer_description_newsletter saisie_textarea editer_odd">
            <label class="editer-label" for="champ_description_newsletter">Description de la newsletter</label>
            <textarea name="description_newsletter" id="champ_description_newsletter" rows="6" cols="33" placeholder="Proposer une description" ></textarea>
        </div>
    </li>
    <li class="_masquer_la_newsletter">
        <div class="editer editer_type_formulaire_newsletter saisie_selection editer_even">
            <label class="editer-label" for="champ_type_formulaire_newsletter">Type de formulaire</label>
            <select name="type_formulaire" id="type_formulaire" data-information="select2options" data-remove="lien" data-parent="masquer_la_newsletter" data-link="'.$_GET['datalink'].'">
                <option value="">Configurer le type de formulaire à utiliser</option>
                <option value="formulaire" selected="selected">formulaire</option>
                <option value="lien">lien</option>
            </select>
        </div>
    </li>
    <li class="_masquer_la_newsletter">
        <div class="editer editer_url_formulaire_newsletter obligatoire saisie_input editer_odd">
            <label class="editer-label" for="champ_url_formulaire_newsletter">URL du lien ou du formulaire<span class="obligatoire"> (obligatoire)</span></label>
            <input type="text" name="url_formulaire_newsletter" class="text width100" id="champ_url_formulaire_newsletter" value="" placeholder="Indiquer le lien ou l\'url du formulaire" required="required" data-parent="masquer_la_newsletter">
        </div>
    </li>
    <li class="_masquer_la_newsletter">
        <div class="editer editer_titre_bouton_sabonner obligatoire saisie_input editer_even">
            <label class="editer-label" for="champ_titre_bouton_sabonner">Titre du bouton<span class="obligatoire"> (obligatoire)</span></label>
            <input type="text" name="titre_bouton_sabonner" class="text width100" id="champ_titre_bouton_sabonner" value="S\'abonner" placeholder="Proposer un titre pour la bouton"  required="required" data-parent="masquer_la_newsletter">
        </div>
    </li>
    <li class="_masquer_la_newsletter gestion_masquer_la_newsletter_select2options">
        <div class="editer editer_nom_champ_input_form obligatoire saisie_input editer_odd">
            <label class="editer-label" for="champ_nom_champ_input_form">Nom du champ input<span class="obligatoire"> (obligatoire)</span></label>
            <input type="text" name="nom_champ_input_form" class="text width100" id="champ_nom_champ_input_form" value="" placeholder="Indiquer un nom pour le champ du formulaire" required="required" data-parent="masquer_la_newsletter">
        </div>
    </li>
    </ul>';  
?>