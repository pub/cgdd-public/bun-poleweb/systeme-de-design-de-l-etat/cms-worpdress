<?php
echo '
<li class="_masquer_liens_ecosysteme">
    <ul class="gestion_lien_eco ecosystem_links" id="ecosysteme" style="display:grid;">
        <li class="nombre_de_liens_ecosysteme ecosysteme">
            <label for="nombre_de_liens_ecosysteme">Nombre de liens écosystème</label><br>
            <select name="nombre_de_liens_ecosysteme" id="nombre_de_liens_ecosysteme" data-elementname="lien_eco" data-parent="nombre_de_liens_ecosysteme" data-select="numeric" data-select-0="1">
                <option value="1" selected="selected">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
            </select>
        </li>
        <li class="ecosysteme">
            <ul class="l8020">
                <li class="ecosysteme"> 
                    <label for="lien_eco_1">Lien écosystème 1:</label>
                    <input class="width100" type="text" name="lien_eco_1" id="lien_eco_1" value="direct;data.gouv.fr;https://data.gouv.fr;">
                </li>
                <li>
                    <button class="configurer_element" type="button" title="Configurer cet élément" data-parent="ecosystem_links" data-fille="lien_eco_1" data-type-generateur="simple-icone"><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/settings.svg" width="12" height="12"></button>
                </li>
            </ul>
        </li>
        <li class="ecosysteme">
            <ul class="l8020">
                <li class="ecosysteme"> 
                    <label for="lien_eco_2">Lien écosystème 2:</label>
                    <input class="width100" type="text" name="lien_eco_2" id="lien_eco_2" value="direct;service-public.fr;https://service-public.fr/;">
                </li>
                <li>
                    <button class="configurer_element" type="button" title="Configurer cet élément" data-parent="ecosystem_links" data-fille="lien_eco_22" data-type-generateur="simple-icone"><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/settings.svg" width="12" height="12"></button>
                </li>
            </ul>
        </li>
        <li class="ecosysteme">
            <ul class="l8020">
                <li class="ecosysteme"> 
                    <label for="lien_eco_3">Lien écosystème 3:</label>
                    <input class="width100" type="text" name="lien_eco_3" id="lien_eco_3" value="direct;gouvernement.fr;https://gouvernement.fr/;">
                </li>
                <li>
                    <button class="configurer_element" type="button" title="Configurer cet élément" data-parent="ecosystem_links" data-fille="lien_eco_3" data-type-generateur="simple-icone"><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/settings.svg" width="12" height="12"></button>
                </li>
            </ul>
        </li>
        <li class="ecosysteme">
            <ul class="l8020">
                <li class="ecosysteme"> 
                    <label for="lien_eco_4">Lien écosystème 4:</label>
                    <input class="width100" type="text" name="lien_eco_4" id="lien_eco_4" value="direct;legifrance.gouv.fr;https://legifrance.gouv.fr/;">
                </li>
                <li>
                    <button class="configurer_element" type="button" title="Configurer cet élément" data-parent="ecosystem_links" data-fille="lien_eco_4" data-type-generateur="simple-icone"><img src="/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/settings.svg" width="12" height="12"></button>
                </li>
            </ul>
        </li>
    </ul>
</li>';
    
?>