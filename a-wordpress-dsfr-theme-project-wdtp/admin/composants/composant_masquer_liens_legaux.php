<?php
echo '
<li class="_masquer_liens_legaux">
    <ul class="legallinks gestion_lien_legal" style="display:grid;">
        <li class="nombre_de_liens_obligations_legales legallinks">
            <label for="nombre_de_liens_obligations_legales">Nombre de liens obligations légales</label><br>
            <select name="nombre_de_liens_obligations_legales" id="nombre_de_liens_obligations_legales" data-elementname="lien_legal" data-parent="nombre_de_liens_obligations_legales" data-select="numeric" data-select-0="1">
                <option value="1" selected="selected">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">7</option>
                <option value="7">4</option>
                <option value="7">8</option>
                <option value="9">9</option>
            </select>
        </li>
        <li class="legallinks nombre_de_liens_obligations_legales_1 ">  
            <ul class="l8020"> 
                <li>  
                    <label for="lien_legal_1">Lien 1:</label>
                    <input class="width100" type="text" name="lien_legal_1" id="lien_legal_1" placeholder="Cliquer sur le bouton pour configurer cet élément" required="required"/>
                </li>
                <li>
                    <button class="configurer_element" type="button" title="Configurer cet élément" data-parent="legallinks" data-fille="lien_legal_1" data-type-generateur="simple-icone"><img src="'.$_GET['datalink'].'/assets/images/settings.svg" width="12" height="12"></button>
                </li> 
            </ul>
        </li>
    </ul>
</li>
<li class="_masquer_liens_legaux">
    <hr class="hr width100">
    <span class="bold">Notes DSFR:</span>
</li>
<li class="_masquer_liens_legaux">
    Il s\'agitde la liste de liens qui doit être définie en fonction du site<br><br>
    Toutefois, à la configuration de ce thème, le responsable devra porter attention aux liens & contenus suivants qui sont obligatoires : <br>- “accessibilité : non/partiellement/totalement conforme”,<br>- mentions légales,<br>- données personnelles et gestion des cookies<br><br>

</li>';
    
?>