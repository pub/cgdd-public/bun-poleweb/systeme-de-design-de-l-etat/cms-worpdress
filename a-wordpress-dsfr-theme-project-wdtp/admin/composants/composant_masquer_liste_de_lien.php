<?php
echo '
    <li class="_masquer_liste_de_lien">
        <ul class="gestion_liste_de_liens" style="display:grid;">
            <li class="_masquer_liste_de_lien nbr_liste">
                <div class="editer editer_nombre_de_liste_de_liens saisie_selection editer_odd">
                    <label class="editer-label" for="nombre_de_liste_de_liens">Nombre de liste de liens</label><br>
                    <select name="nombre_de_liste_de_liens" id="nombre_de_liste_de_liens" data-elementname="liste_de_liens" data-select="numeric" data-parent="masquer_liste_de_lien" data-select-0="1">
                    <option value="1" selected="selected">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                    </select>
                </div>
            </li>
            <li class="_masquer_liste_de_lien liste_externe  masquer_liste_de_lien_1" style="order:1;">
                <ul class="l8020">
                    <li class="_masquer_liste_de_lien liste_externe">
                        <label for="liste_de_liens_1">Liste de liens 1:<span class="obligatoire"> (obligatoire)</span></label>
                        <input required="required" class="width100" type="text" name="liste_de_liens_1" id="liste_de_liens_1" value="" placeholder="Cliquer sur le bouton pour configurer cet élément"/>
                    </li>                    
                    <li class="_masquer_liste_de_lien _masquer_liste_de_lien_1">
                        <button class="configurer_element" type="button" title="Configurer cet élément" data-parent="masquer_liste_de_lien_primaire" data-fille="liste_de_liens_1" data-type-generateur="liste_de_liens"><img src="'.$_GET['datalink'].'/assets/images/settings.svg" width="12" height="12"></button>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
';  
?>
