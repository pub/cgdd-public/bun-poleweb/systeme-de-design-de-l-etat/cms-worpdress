<?php
echo '
    <li class="_masquer_menu">
        <div class="editer editer_nombre_de_menu saisie_selection editer_odd">
            <label class="editer-label" for="nombre_de_menu">Nombre de menus</label><br>
            <select name="nombre_de_menu" id="nombre_de_menu" data-select="numeric" data-parent="masquer_menu" data-elementname="menu">
                <option value="1" selected="selected">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
            </select>
        </div>
    </li>
    <li class="_masquer_menu" style="order:1;">
        <ul class="l8020">
            <li>
                <div class="editer editer_menu_1 obligatoire saisie_input editer_even">
                    <label class="editer-label" for="menu_1">Menu 1<span class="obligatoire"> (obligatoire)</span></label>
                    <input type="text" name="menu_1" class="text width100 altkey" id="menu_1" value="" required="required" aria-describedby="explication_menu_1" data-parent="masquer_menu">
                </div>
            </li>  
            <li class="_masquer_menu masquer_menu_1">
                <button class="configurer_element" type="button" title="Configurer cet élément" data-parent="menumembre" data-fille="menu_1" data-type-generateur="menu"><img src="'.$_GET['datalink'].'/assets/images/settings.svg" width="12" height="12"></button>
            </li>     
        </ul>
    </li>';
    
?>