<?php
echo '
    <li class="_masquer_message_alerte">
        <div class="editer editer_message_alerte obligatoire saisie_textarea editer_odd">
            <label class="editer-label" for="message_alerte">Message d\'alerte<span class="obligatoire"> (obligatoire)</span></label>
            <textarea name="message_alerte" class="width100" id="message_alerte" rows="4" cols="33"  data-parent="masquer_message_alerte" required="required"></textarea>
        </div>
    </li>
    <li class="_masquer_message_alerte">
        <hr class="hr width100">
        <span class="bold">Notes DSFR:</span>
    </li>
    <li class="_masquer_message_alerte">Le bandeau d’information importante doit être utilisé uniquement pour une information primordiale et temporaire. (Une utilisation excessive ou continue risque de “noyer” le composant).<br><br>
        Le bandeau doit être visible sur toutes les pages du site, quelque soit l’appareil utilisé.<br><br>
        Pour une information vitale comme une alerte enlèvement, il est possible d’utiliser une bannière spécifique.
    </li>';
?>