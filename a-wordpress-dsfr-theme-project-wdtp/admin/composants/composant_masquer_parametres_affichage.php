<?php
echo '
	<li class="_masquer_parametres_affichage">
		<div class="editer editer_parametre_affichage_defaut saisie_selection editer_even">
			<label class="editer-label" for="champ_parametre_affichage_defaut">Paramètres d\'affichage par défaut</label>
			<select name="parametre_affichage_defaut" id="champ_parametre_affichage_defaut">
				<option value="">Sélectionnez une option</option>
				<option value="dark">dark</option>
				<option value="light" selected="selected">light</option>
			</select>
		</div>
	</li>
	<li class="_masquer_parametres_affichage">
		<div class="editer editer_pad_dans_footer saisie_selection editer_odd">
			<label class="editer-label" for="champ_pad_dans_footer">Paramètres d\'affichage dans footer?</label>
			<select name="pad_dans_footer" id="champ_pad_dans_footer">
				<option value="">Sélectionnez une option</option>
				<option value="non">non</option>
				<option value="oui" selected="selected">oui</option>
			</select>
		</div>
	</li>
	<li class="_masquer_parametres_affichage">
		<hr class="hr width100">
		<span class="bold">Notes:</span>
	</li>
	<li class="_masquer_parametres_affichage">
		Les paramètres d\'affichage prennent la place du lien d\'accès rapide 3<br><br>
		Les paramètres d\'affichage sont dépendants des liens d\'accès -&gt; Liens d\'accès automatiquement activés si vous activez « <strong>paramètres d\'affichage</strong> ».
	</li>';  
?>