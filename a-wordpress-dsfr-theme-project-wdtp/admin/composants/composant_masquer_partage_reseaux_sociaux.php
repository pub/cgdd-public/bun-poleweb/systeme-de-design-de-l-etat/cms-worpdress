<?php
echo '
    <li class="_masquer_partage_reseaux_sociaux">
        <div class="editer editer_titre_partage_reseaux_sociaux saisie_input editer_even">
            <label class="editer-label" for="titre_partage_reseaux_sociaux">Titre du bloc suivi</label>
            <input type="text" name="titre_partage_reseaux_sociaux" class="text width100" id="titre_partage_reseaux_sociaux" placeholder="Nom à donner au bloc de suivi">
        </div>
    </li>
    <li class="_masquer_partage_reseaux_sociaux">
        <div class="editer editer_url_facebook saisie_input editer_odd">
            <label class="editer-label" for="url_facebook">Url Facebook</label>
            <input type="text" name="url_facebook" class="text width100" id="url_facebook" placeholder="Lien vers votre page Facebook">
        </div>
    </li>
    <li class="_masquer_partage_reseaux_sociaux">
        <div class="editer editer_url_twitter saisie_input editer_even">
            <label class="editer-label" for="url_twitter">Url Twitter</label>
            <input type="text" name="url_twitter" class="text width100" id="url_twitter" placeholder="Lien vers votre page Twitter">
        </div>
    </li>
    <li class="_masquer_partage_reseaux_sociaux">
        <div class="editer editer_url_youtube saisie_input editer_odd">
            <label class="editer-label" for="url_youtube">Url Youtube</label>
            <input type="text" name="url_youtube" class="text width100" id="url_youtube" placeholder="Lien vers votre page Youtube">
        </div>
    </li>
    <li class="_masquer_partage_reseaux_sociaux">
        <div class="editer editer_url_instagram saisie_input editer_even">
            <label class="editer-label" for="url_instagram">Url Instagram</label>
            <input type="text" name="url_instagram" class="text width100" id="url_instagram" placeholder="Lien vers votre page Instagram">
        </div>
    </li>
    <li class="_masquer_partage_reseaux_sociaux">
        <div class="editer editer_url_linkedin saisie_input editer_odd">
            <label class="editer-label" for="url_linkedin">Url Linkedin</label>
            <input type="text" name="url_linkedin" class="text width100" id="url_linkedin" placeholder="Lien vers votre page Linkedin">
        </div>
    </li>
    <li class="_masquer_partage_reseaux_sociaux">
        <div class="editer editer_url_rss saisie_input editer_even">
            <label class="editer-label" for="url_rss">Url Flux RSS</label>
            <input type="text" name="url_rss" class="text width100" id="url_rss" placeholder="Lien vers votre Flux RSS">
        </div>
    </li>';
?>