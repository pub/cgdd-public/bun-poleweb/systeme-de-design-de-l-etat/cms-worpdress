<?php
echo '
<li class="_masquer_partenaires">
    <h3>Les partenaires principaux</h3>
    <ul class="gestion_partenaires_principaux" style="display:grid;">
        <li class="nombre_de_partenaires_principaux">
            <label for="nombre_de_partenaires_principaux">Nombre de partenaires principaux</label><br>
            <select name="nombre_de_partenaires_principaux" id="nombre_de_partenaires_principaux" data-elementname="partenaires_principaux" data-parent="partenaires_principaux" data-select="numeric" data-select-0="0">
                <option value="0">0</option>
                <option value="1" selected="selected">1</option>
                <option value="2">2</option>
            </select>
        </li>
        <li class="partenaires_principaux_1" style="order:1;">
            <ul class="l8020">
                <li class="partenaires_principaux">
                    <label for="partenaires_principaux_1">Partenaire principal 1:</label>
                    <input class="width100" type="text" name="partenaires_principaux_1" id="partenaires_principaux_1" placeholder="Nom partenaire;url partenaire;url image partenaire" value="" required="required">
                </li>
                <li class="partenaires_principaux">
                    <button class="configurer_element" type="button" title="Configurer cet élément" data-parent="masquer_partenaires" data-fille="partenaires_principaux_1" data-type-generateur="simple-image"><img src="'.$_GET['datalink'].'/assets/images/settings.svg" width="12" height="12"></button>
                </li>
            </ul>
        </li>
    </ul>
    <h3>Les partenaires secondaires</h3>
    <ul class="gestion_partenaires_secondaires" style="display:grid;">
        <li class="nombre_de_partenaires_secondaires">
            <label for="nombre_de_partenaires_secondaires">Nombre de partenaires secondaires</label><br>
            <select name="nombre_de_partenaires_secondaires" id="nombre_de_partenaires_secondaires" data-elementname="partenaires_secondaires" data-parent="partenaires_secondaires" data-select="numeric" data-select-0="0">
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2" selected="selected">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
            </select>
        </li>
        <li class="partenaires_secondaires_1" style="order:1;">
            <ul class="l8020">
                <li class="partenaires_secondaires">
                    <label for="partenaires_secondaires_1">Partenaire secondaire 1:</label>
                    <input class="width100" type="text" name="partenaires_secondaires_1" id="partenaires_secondaires_1" placeholder="Nom partenaire;url partenaire;url image partenaire" value="" required="required">
                </li>
                <li class="partenaires_secondaires_1">
                    <button class="configurer_element" type="button" title="Configurer cet élément" data-parent="masquer_partenaires" data-fille="partenaires_secondaires_1" data-type-generateur="simple-image"><img src="'.$_GET['datalink'].'/assets/images/settings.svg" width="12" height="12"></button>
                </li>
            </ul>
        </li>    
        <li class="partenaires_secondaires_2" style="order:2;">
            <ul class="l8020">
                <li class="partenaires_secondaires">
                    <label for="partenaires_secondaires_2">Partenaire secondaire 2:</label>
                    <input placeholder="Nom partenaire;url partenaire;url image partenaire" class="width100" type="text" name="partenaires_secondaires_2" id="partenaires_secondaires_2" value="" required="required">
                </li>
                <li class="partenaires__secondaires_2">
                    <button class="configurer_element" type="button" title="Configurer cet élément" data-parent="masquer_partenaires" data-fille="partenaires_secondaires_2" data-type-generateur="simple-image"><img src="'.$_GET['datalink'].'/assets/images/settings.svg" width="12" height="12"></button>
                </li>
            </ul>
        </li>	
    </ul>
</li>';  
?>


