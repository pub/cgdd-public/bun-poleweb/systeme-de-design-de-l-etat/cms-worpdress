<?php
echo '
    <li class="_masquer_statistiques">
        <div class="editer editer_script_statistiques obligatoire saisie_textarea editer_odd">
            <label class="editer-label" for="script_statistiques">Script statistiques<span class="obligatoire"> (obligatoire)</span></label>
            <textarea name="script_statistiques" class="width100" id="script_statistiques" rows="4" cols="33" aria-describedby="explication_script_statistiques" data-parent="masquer_statistiques" required="required"></textarea>
        </div>
    </li>
    <li class="_masquer_statistiques">
        <hr class="hr width100">
        <span class="bold">Notes:</span>
    </li>
    <li class="_masquer_statistiques">Le script de statistiques ne sera appliqué que sur le domaine défini.<br><br>
        Pour modifier le domaine sur lequel s\'applique le script de statistiques, il faudra modifier le champ "Nom du domaine en production" ou <a href="#dsfr_nom_de_domaine_en_production">Cliquer ici</a>
    </li>';
?>