jQuery(document).ready(function($) {
  
  // Fonction pour vérifier si de nouveaux liens doivent être ajoutés
  function doitAjouterNouveauxLiens(name) {
    // Si l'élément avec ce nom n'existe pas, renvoie vrai
    return $(`[name="${name}"]`).length === 0;
  }

  // Assure que l'attribut 'data' est présent
  function assurerAttributData(typeData, nom, valeur) {
    const elementParent = $('[name="' + nom + '"]');
    if (!elementParent.attr(typeData)) {
        elementParent.attr(typeData, valeur); 
    }
  }
  function configurerLesElements() {
    assurerAttributData('data-gestion', 'masquer_le_titre', 'true');
    assurerAttributData('data-gestion', 'masquer_le_slogan', 'true');
    assurerAttributData('data-gestion', 'masquer_message_alerte', 'true');
    assurerAttributData('data-gestion', 'masquer_statistiques', 'true');
    assurerAttributData('data-gestion', 'masquer_partage_reseaux_sociaux', 'true');
    assurerAttributData('data-gestion', 'masquer_la_newsletter', 'true');
    assurerAttributData('data-gestion', 'masquer_parametres_affichage', 'true');
    assurerAttributData('data-gestion', 'masquer_cookies', 'true');
    assurerAttributData('data-gestion', 'masquer_liste_acces_rapide', 'true');
    assurerAttributData('data-gestion', 'masquer_logo_secondaire', 'true');
    assurerAttributData('data-gestion', 'masquer_partenaires', 'true');
    assurerAttributData('data-gestion', 'masquer_menu', 'true');
    assurerAttributData('data-gestion', 'masquer_liste_de_lien', 'true');
    assurerAttributData('data-gestion', 'masquer_liens_ecosysteme', 'true');
    assurerAttributData('data-gestion', 'masquer_liens_legaux', 'true');
    assurerAttributData('data-gestion', 'masquer_description_footer', 'true');
    assurerAttributData('data-gestion', 'masquer_page_contact', 'true');
    

    assurerAttributData('data-required', 'lien_dacces_rapide1', 'non');
    assurerAttributData('data-required', 'lien_dacces_rapide2', 'non');
    assurerAttributData('data-required', 'lien_dacces_rapide3', 'non');

    assurerAttributData('data-elementname', 'nombre_de_liste_de_liens', 'liste_de_liens');        
    assurerAttributData('data-elementname', 'nombre_de_menu', 'menu');
    assurerAttributData('data-elementname', 'nombre_de_partenaires_principaux', 'partenaires_principaux');
    assurerAttributData('data-elementname', 'nombre_de_partenaires_secondaires', 'partenaires_secondaires');
    assurerAttributData('data-elementname', 'nombre_de_liens_ecosysteme', 'lien_eco');
    assurerAttributData('data-elementname', 'nombre_de_liens_obligations_legales', 'lien_legal');
    
    assurerAttributData('data-information', 'type_formulaire_newsletter', 'select2options');
    assurerAttributData('data-information', 'modifier_texte_cookies', 'select2options');
    
    assurerAttributData('data-remove', 'type_formulaire_newsletter', 'lien');
    assurerAttributData('data-remove', 'modifier_texte_cookies', 'non');

    assurerAttributData('data-parent', 'type_formulaire_newsletter', 'masquer_la_newsletter');
    assurerAttributData('data-parent', 'message_alerte', 'masquer_message_alerte');
    assurerAttributData('data-parent', 'script_statistiques', 'masquer_statistiques');
    assurerAttributData('data-parent', 'liste_des_cookies', 'masquer_cookies');
    assurerAttributData('data-parent', 'texte_cookies', 'masquer_cookies');
    assurerAttributData('data-parent', 'modifier_texte_cookies', 'masquer_cookies');
    assurerAttributData('data-parent', 'nombre_de_liste_de_liens', 'masquer_liste_de_lien');
    assurerAttributData('data-parent', 'nombre_de_menu', 'masquer_menu');
    assurerAttributData('data-parent', 'nombre_de_partenaires_principaux', 'partenaires_principaux');
    assurerAttributData('data-parent', 'nombre_de_partenaires_secondaires', 'partenaires_secondaires');
    assurerAttributData('data-parent', 'nombre_de_liens_ecosysteme', 'nombre_de_liens_ecosysteme');
    assurerAttributData('data-parent', 'nombre_de_liens_obligations_legales', 'nombre_de_liens_obligations_legales');


    assurerAttributData('data-select', 'nombre_de_liste_de_liens', 'numeric');        
    assurerAttributData('data-select-0', 'nombre_de_liste_de_liens', '1');        
    assurerAttributData('data-select', 'nombre_de_menu', 'numeric');
    assurerAttributData('data-select-0', 'nombre_de_menu', '1');
    assurerAttributData('data-select', 'nombre_de_partenaires_principaux', 'numeric');
    assurerAttributData('data-select', 'nombre_de_partenaires_secondaires', 'numeric');
    assurerAttributData('data-select', 'nombre_de_liens_ecosysteme', 'numeric');
    assurerAttributData('data-select', 'nombre_de_liens_obligations_legales', 'numeric');
    assurerAttributData('data-select-0', 'nombre_de_partenaires_principaux', '0');
    assurerAttributData('data-select-0', 'nombre_de_partenaires_secondaires', '0');
    assurerAttributData('data-select-0', 'nombre_de_liens_ecosysteme', '1');
    assurerAttributData('data-select-0', 'nombre_de_liens_obligations_legales', '1');
    assurerAttributData('data-select-0', 'masquer_liens_ecosysteme', '1');
    assurerAttributData('data-select-0', 'masquer_liens_legaux', '0');

    $('[name="masquer_liens_ecosysteme"]').removeAttr('checked').attr("disabled","disabled");
    $('[name="masquer_licence"]').removeAttr('checked').attr("disabled","disabled");
  }
  // debug
  function meslogs(data) {
    console.log(data + $(data).attr('data-information'));
  }
  function miseAJourDesElementsDesCommandes() {
    ;
  }
function surveillerSelectNumeric() {
    // Attachement de l'événement 'change' à l'élément avec l'attribut 'data-select' égal à 'numeric'
    $(document).on('change', '[data-select="numeric"]', function() {        
        var $this = $(this);
        var nombreElements = $this.val();

        const nomElements = $this.attr('data-elementname');
        const masqueElement = $this.attr('data-parent');
        const urlLink = $('[name="masquer_partenaires"]').data('link');
        const filenameElement = `${urlLink}/admin/composants/composant_${nomElements}_numeric.php`;
        // Parcours et ajout des nouveaux éléments si nécessaire
        var files = []
        for(let i = 1; i <= nombreElements; i++){
            const nouvelElement = doitAjouterNouveauxLiens(`${nomElements}_${i}`); 
            if(nouvelElement){
              var dataLink = {autreparametre: i,
                              datalink: urlLink
                            };
              const emplacement = `.gestion_${nomElements}`;
              files.push({ filename: filenameElement, data: dataLink, emplacement: emplacement });
              //createAjaxRequest(filenameElement, dataLink, emplacement);
            }
        }
        processFilesSequentially(files);

        // Suppression des éléments excédentaires
        const selectCourant = $this.attr('name');
        const nombreOptionsSelect = $(`[name="${selectCourant}"] option`).length;
        const dataSelect0 = $this.data('select-0');
        for(let j = parseInt(nombreElements) + 1; j <= nombreOptionsSelect; j++){
            const suppressionElement = `${masqueElement}_${j}`;
            $(`.${suppressionElement}`).remove();
        }
    });
}
  // Cette fonction surveille les changements d'état des éléments select ayant 2 options
function surveillerSelect2Options() {
  $(document).on('change', '[data-information="select2options"]', function() {
      // Extraction des attributs nécessaires de l'élément select
      //const nomSelect = $(this).attr('name'); 
      const parentElement = $(this).data('parent');
      const emplacement = `.gestion_${parentElement}`;
      const nomComposant = `${parentElement}_select2options`;
      const triggerRemove = $(this).data('remove');
      const urlLink = $('[name="' + parentElement + '"]').data('link');
      const filename = `${urlLink}/admin/composants/composant_${nomComposant}.php`;
      const elementValeur = $(this).val();
      const datalink = { datalink: $(this).data('link')};
      //const elementCache = $(`.${parentElement}_select2options`);

      // Suppression du composant si la valeur de l'élément est égale à triggerRemove
      if(elementValeur === triggerRemove) {
          $(`.gestion_${nomComposant}`).remove();
      } 
      // Construction des champs du formulaire dans le cas contraire
      else {
        createAjaxRequest(filename, datalink, emplacement);
      }
  });
}
function createAjaxRequest(filename, data, emplacement) {
  return new Promise(function(resolve, reject) {
    $.ajax({
      url: filename,
      type: 'GET',
      data: data,
      success: function(response) {
        $(emplacement).append(response);
        resolve(); // Résoudre la promesse une fois que la requête AJAX est terminée
      },
      error: function() {
        console.error('Une erreur s\'est produite lors de l\'ajout de la div.');
        reject(); // Rejeter la promesse en cas d'erreur
      }
    });
  });
}

function processFilesSequentially(fileList) {
  var sequence = Promise.resolve();

  fileList.forEach(function(file) {
    sequence = sequence.then(function() {
      return createAjaxRequest(file.filename, file.data, file.emplacement);
    });
  });
  return sequence;
}



/*var files = [
  { filename: 'file1.txt', data: { key: 'value1' }, emplacement: '#div1' },
  { filename: 'file2.txt', data: { key: 'value2' }, emplacement: '#div2' },
  { filename: 'file3.txt', data: { key: 'value3' }, emplacement: '#div3' }
];

processFilesSequentially(files)
  .then(function() {
    console.log('Toutes les requêtes AJAX sont terminées avec succès.');
  })
  .catch(function() {
    console.error('Une ou plusieurs requêtes AJAX ont échoué.');
  });
*/
// Cette fonction est utilisée pour construire les champs du formulaire
function construireLesChampsDuFormulaire(elementCache, data={}, nomCheckbox, linkTheme) {
  let fichierURL = `${linkTheme}/admin/composants/composant_${nomCheckbox}.php`;
  let emplacementInsertion = `.gestion_${nomCheckbox}`;
  let requestData = data;
  createAjaxRequest(fichierURL, requestData, emplacementInsertion);
}
// Cette fonction ajuste les paramètres d'affichage quand la valeur de l'élément caché est 'non'
function ajusterParametresAffichageNon(emplacement, nomCheckbox) {
  const fichierURL = '/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/admin/composants/composant_masquer_liste_acces_rapide.php';
  const requestData = {masquer_parametres_affichage:'non',
    masquer_liste_acces_rapide:'non'};
  const emplacementInsertion = '.gestion_masquer_liste_acces_rapide';
  $('[name="masquer_liste_acces_rapide"]').val('non').removeAttr('checked').attr("disabled","disabled");
  $('[name="_masquer_liste_acces_rapide"]').val('non');
  $(`.remove_width_${nomCheckbox}`).remove();
  doitAjouterNouveauxLiens('lien_dacces_rapide1') && createAjaxRequest(fichierURL, requestData, emplacementInsertion);
}

// Cette fonction ajuste les paramètres d'affichage quand la valeur de l'élément caché est 'oui'
function ajusterParametresAffichageOui(emplacement) {
  emplacement = '.liens_acces_rapide';
  $('[name="masquer_liste_acces_rapide"]').removeAttr("disabled");
  const elementDelete = $(".remove_width_masquer_parametres_affichage");  
  const masquer_parametres_affichage = $('[name="_masquer_parametres_affichage"]').val();
  const masquer_liste_acces_rapide = $('[name="_masquer_liste_acces_rapide"]').val();
  const linkTheme = $('[name="masquer_liste_acces_rapide"]').data('link');

  
  const data = {masquer_parametres_affichage: masquer_parametres_affichage,
                masquer_liste_acces_rapide: masquer_liste_acces_rapide,
                datalink: linkTheme
              };

  if ($('[name="masquer_liste_acces_rapide"]').val() === 'non') {
    var ajoutLienAccesRapide = "build_mlar_width_masquer_parametres_affichage";
  } else {
    var ajoutLienAccesRapide = "masquer_liste_acces_rapide";
  }
  const filename = `${linkTheme}/admin/composants/composant_${ajoutLienAccesRapide}.php`;
  createAjaxRequest( filename, data, emplacement);
}

  // Cette fonction modifie la visibilité d'un élément en fonction de l'état d'une checkbox
  function mettreAJourVisibiliteElement(checkbox, elementCache, data={}, nomCheckbox, linkTheme, onChange = false) {
    // Récupère l'état de la checkbox
    const estCoche = checkbox.is(':checked');
    const masquer_parametres_affichage = $('[name="_masquer_parametres_affichage"]').val();
    const masquer_liste_acces_rapide = $('[name="_masquer_liste_acces_rapide"]').val();
    // Modifie la valeur de l'élément caché et de la checkbox en fonction de l'état de la checkbox
    elementCache.val(estCoche ? 'oui' : 'non');
    checkbox.val(estCoche ? 'oui' : 'non');
    
    // Récupère tous les éléments qui ont pour parent la checkbox et modifie leur attribut 'required' en fonction de l'état de la checkbox
    const elementsAssocies = $(`[data-parent="${checkbox.attr('name')}"]`);
    estCoche ? elementsAssocies.removeAttr('required') : elementsAssocies.attr('required', 'required');
    
    if ((nomCheckbox === 'masquer_liste_acces_rapide') && ($('[name="masquer_liste_acces_rapide"]').val() === 'non')) {
      data = {masquer_parametres_affichage: masquer_parametres_affichage,
        masquer_liste_acces_rapide: masquer_liste_acces_rapide
      };
    }
    
    // Si l'élément caché a pour valeur 'non' et que le paramètre onChange est vrai, alors appelle la fonction construireLesChampsDuFormulaire
    if(elementCache.val() === 'non' && onChange){
        construireLesChampsDuFormulaire(nomCheckbox, data, nomCheckbox, linkTheme);
    }
    // Si le nom de la checkbox est 'masquer_parametres_affichage', alors on ajuste les paramètres d'affichage
    if(nomCheckbox === 'masquer_parametres_affichage') {
        const value = elementCache.val();
        let emplacement = '.liens_acces_rapide';
        value === 'non' ? ajusterParametresAffichageNon(emplacement, nomCheckbox) : ajusterParametresAffichageOui(emplacement);
    }
    console.log(estCoche);

  }

  // configuration des éléments spécifiques
  configurerLesElements();
  miseAJourDesElementsDesCommandes();
  surveillerSelect2Options();
  surveillerSelectNumeric();
  surveillerDemandeGenerateurs();
 

  $(document).on('click', '[data-informations="commandes"]', function() {
    $this = $(this);
    const linkTheme = $this.data('link');
    const titleSite = $this.data('title');
    const datalink = $this.data('link');
    const nomCheckbox = $this.attr('name');
    const commandeValue = $this.attr('value');
    const gestion = $this.attr('data-gestion');    
    const elementCache = $(`[name="_${nomCheckbox}"]`);
    const elementMasque = $(`._${nomCheckbox}`);
    const data = {titleSite : titleSite,
                  datalink : datalink
                };
    elementMasque.remove();
    
    mettreAJourVisibiliteElement($this, elementCache, data, nomCheckbox, linkTheme, gestion);


    console.log(nomCheckbox + '::' + commandeValue + ':: ' + gestion);

  });

  function resetMenuGeneratorForm() {
    $('.affichage_generateur').remove();
    $('[name="submit_generateurs_menu"]').addClass('dontshow');
    $('[name="type-menu-type"]').val('');
    $('.les-resultats').addClass('dontshow');
    $('.les-resultats nav').remove();
    $('#modal').addClass('dontshow');
    $('[name="submit_generateurs_menu"]').attr("value", "Afficher les résultats");
    $('button.autoriser_reinit_form').addClass('dontshow');        
    $('#resultats').remove();
    $('submit_generateurs_menu').addClass('dontshow');
    $('.simple_icone_flag_remove').remove();
  }
  function processMenuSimple(typeMenuType, submitButton, elementParent, emplacement, filename) {
    var menuSimpleTitre = $('[name="menu-simple-titre"]').val();
    var menuSimpleUrl = $('[name="menu-simple-url"]').val();
    var menuSimpleIcon = $('[name="menu-simple-icon"]').val();
    
    if ((menuSimpleTitre === '')  || (menuSimpleUrl === '')) {
      alert('Veuillez remplir tous les champs obligatoires (marqués par un astérisque)');
      return;
    }

    if(submitButton.attr("value") === "Valider le résultat") {
        var resultat = 'direct;' + menuSimpleTitre + ';' + menuSimpleUrl + ';' + menuSimpleIcon;
        $('[name="' + elementParent + '"]').val(resultat);
        resetMenuGeneratorForm();
    } else {
        createAjaxRequest(filename, {
            menuSimpleTitre: menuSimpleTitre,
            menuSimpleUrl: menuSimpleUrl,
            menuSimpleIcon: menuSimpleIcon
        }, emplacement);

        submitButton.attr("value", "Valider le résultat");
        $('button.autoriser_reinit_form').removeClass('dontshow');
        $('.les-resultats').removeClass('dontshow');
        $('.resultat_menu_simple').removeClass('dontshow');
    }
}

  function surveillerDemandeGenerateurs(){
    $(document).on('click', '.configurer_element', function(e) {
      e.preventDefault();
      $this = $(this);
      resetMenuGeneratorForm();
      const componentFille = $this.data('fille');
      const typeGenerateur = $this.data('type-generateur');
      const cheminElementFichier = $('[name="plugin_root"]').val();
      const fichierElement = `${cheminElementFichier}/template-parts/excerpt/excerpt-generateurs-${typeGenerateur}.php`;
      const emplacementElement = '.elementFormUl';
      const datalink = {typeGenerateur: typeGenerateur,
                        componentFille: componentFille,
                      };
      $('#modal').attr('data-parent', componentFille);
      $('#modal').removeClass('dontshow');
      $('#menu_generator input[type="submit"]').removeClass('dontshow');
      $(emplacementElement).append('<li class="simple_icone_flag_remove"><label>Type de menu: <span class="type-menu-type">' + typeGenerateur + '</span></label></li>');
      createAjaxRequest(fichierElement, datalink, emplacementElement);

    });

    $(document).on('click', '#modal .close', function(e) {
      e.preventDefault();
      $('#modal').addClass('dontshow');
      $('.simple_icone_flag_remove').remove();
      $('#modal').removeAttr('data-parent');


    });
  $(document).on('change', '[name="dsfr_afficher_selection_type_logo"]', function(e) {
    const $this = $(this);
    const valeur = $this.val();
    const imagechargee = $('#image_second_logo2').val();
    const datalink = {};
    const filename = '/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/admin/composants/composant_' + valeur +'_image.php';
    const emplacement = '.gestion_masquer_logo_secondaire';
    if (valeur === 'code'){
      $('.suppression-image').remove();
    }
    if (valeur === 'fichier'){
      $('.suppression-code').remove();  
      $('._masquer_logo_secondaire ').remove();          
    }
    createAjaxRequest(filename, datalink, emplacement);

  });

    $(document).on('click', '[name="submit_generateurs_menu"]', function(e) {
      var form = document.getElementById('menu_generator');
      if (!form.checkValidity()) {
        return;
      }
      e.preventDefault();
      $this = $(this);
      const submitButton = $('[name="submit_generateurs_menu"]');
      const typeGenerateur = $('[name="type_generateur"]').val();
      const elementParent = $('[name="element_parent"]').val();
      const emplacement = $('.resultat_menu_simple fieldset');

      $('.resultat_menu_simple fieldset nav').remove();
      var uniqueRandomVar = Math.random().toString(36).substr(2) + Date.now().toString(36);
      var filename = `/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/admin/composants/composant_generateur_${typeGenerateur}_resultat.php?${uniqueRandomVar}`;
      switch (typeGenerateur) {
        case 'simple-icone':
            processMenuSimple(typeGenerateur, submitButton, elementParent, emplacement, filename);
        break;        
        case 'simple-image':
            processMenuSimple(typeGenerateur, submitButton, elementParent, emplacement, filename);
        break;        
        case 'menu':
          const list_menu_type = $('[name="list-menu-type"]').val()
            if (list_menu_type === 'menu_deroulant') {
              processMenuDeroulant(typeGenerateur, submitButton, elementParent, '.les-resultats', 'composant_generateur_menu_deroulant_resultat');
            } else if (list_menu_type === 'megamenu') {
              processMegaMenu(typeGenerateur, submitButton, elementParent, emplacement, filename);
              } else {
                processMenuSimple(typeGenerateur, submitButton, elementParent, emplacement, filename);
              }
        break;        
        case 'liste_de_liens':
            processListeDeLiens(typeGenerateur, submitButton, elementParent, emplacement, filename);
        break;     
        default:
        break;
      }
    });
  }
  function processMegaMenu(typeMenuType, submitButton, elementParent, emplacement, filename) {        
    var nombreDeBlocDeMenus = $('[name="nombre_de_menus_megamenu"]').val();
    if(submitButton.attr("value") !== "Valider le résultat"){
        submitButton.attr("value", "Valider le résultat");
        $('button.autoriser_reinit_form').removeClass('dontshow');
        var mesdatas = {
            nombreBlocMenu: nombreDeBlocDeMenus,
            megamenuTitreMenu: $('[name="menu-megamenu-titre"]').val(),
            megamenuTitreAccueil: $('[name="contenu-megamenu-titre"]').val(),
            megamenuEntete: $('[name="entete_titre_megamenu"]').val(),
            megamenuDescription: $('[name="menu-megamenu-description"]').val(),
            megamenuTitreCategorie: $('[name="categorie-megamenu-titre"]').val(),
            megamenuUrlCategorie: $('[name="categorie-megamenu-url"]').val(),
            $megamenuImage: $('[name="menu-megamenu-image"]').val(),
        };
        for(let i = 1; i <= nombreDeBlocDeMenus; i++){
            var nombreDeSousMenus = $('[name="nombre_de_menus_megamenu_bloc' + i + '"]').val();
            mesdatas['titrebloc' + i] = $('[name="menu-megamenu-titre-menu-bloc' + i + '"]').val();
            mesdatas['nombreDeSousMenus' + i] = nombreDeSousMenus;
            for(let j = 1; j <= nombreDeSousMenus; j++){
                mesdatas['titre' + j + 'menu' + i] = $('[name="titre' + j + 'menu' + i + '"]').val();
                mesdatas['url' + j + 'menu' + i] = $('[name="url' + j + 'menu' + i + '"]').val();
            }                
        }
        const emplacement = '.lesresultats';
        filename = '/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/admin/composants/composant_generateur_megamenu_resultat.php';
        createAjaxRequest(filename, mesdatas, emplacement);
        
        $('.les-resultats').removeClass('dontshow');
    } else{
        //megamenu;3;Mega;/IMG/theme.svg;titre;h2;desc;cat;urlcat;menu1b1;11;11;21;21;menu2b2;12;12;menu3b3;13;13;23;23;33;33;
        const entete = $('[name="entete_titre_megamenu"]').val();;
        var resultat = 'megamenu;' + nombreDeBlocDeMenus + ';' + $('[name="menu-megamenu-titre"]').val() + ';' + $('[name="menu-megamenu-image"]').val() + ';' + $('[name="contenu-megamenu-titre"]').val() + ';' + entete + ';' + $('[name="menu-megamenu-description"]').val() + ';' + $('[name="categorie-megamenu-titre"]').val() + ';' + $('[name="categorie-megamenu-url"]').val() + ';'
        switch (entete) {
          case 'h1':
            var entete_bloc = 'h2';
          break;        
          case 'h2':
            var entete_bloc = 'h3';
          break;        
          case 'h3':
            var entete_bloc = 'h4';
          break;        
          case 'h4':
            var entete_bloc = 'h5';
          break;        
          case 'h5':
            var entete_bloc = 'h6';
          break;    
          default:
            var entete_bloc = 'p';
          break;
        }
        for(var i =1; i <= nombreDeBlocDeMenus; i++ ){
            resultat += 'menu' + i + ';' + $('[name="menu-megamenu-titre-menu-bloc' + i + '"]').val() + ';' + entete_bloc + ';';
            var nombreDeSousMenus = $('[name="nombre_de_menus_megamenu_bloc' + i + '"]').val();
            for(var j = 1; j <= nombreDeSousMenus; j++){
                resultat += $('[name="titre' + j + 'menu' + i + '"]').val() + ';' + $('[name="url' + j + 'menu' + i + '"]').val();
                if (j <= nombreDeSousMenus){
                    resultat += ';';
                }
            }                
        }
        $('[name="' + elementParent + '"]').val(resultat);
        resetMenuGeneratorForm();
        submitButton.attr("value", "Valider le résultat");
        $('button.autoriser_reinit_form').removeClass('dontshow');
        $('.les-resultats').removeClass('dontshow');
        
    }
}
$(document).on('change', '[name="nombre_de_menus_megamenu"]', function() {
    const $this = $(this);
    const nombreBlocMega = $this.val();
    const menuType = $this.data('menutype');
    const emplacementBloc = $this.data('emplacement');
    const filenameComponent = `/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/admin/composants/composant_generateur_${menuType}_${emplacementBloc}.php`;

    var files = [];
    for(let i = 1; i <= nombreBlocMega; i++){
      const nouvelElement = doitAjouterNouveauxLiens(`menu-megamenu-titre-menu-bloc${i}`); 
      if(nouvelElement){
        var dataLink = {autreparametre: i
                      };
        const emplacement = `.gestion_${emplacementBloc}`;
        files.push({ filename: filenameComponent, data: dataLink, emplacement: emplacement });
        //createAjaxRequest(filenameElement, dataLink, emplacement);
      }
   }
   processFilesSequentially(files);
    

    const selectCourant = $this.attr('name');
    const nombreOptionsSelect = $(`[name="${selectCourant}"] option`).length;

    for(let j = parseInt(nombreBlocMega) + 1; j <= nombreOptionsSelect - 1; j++) {
        $(`.colonne${j}`).remove();
    }
});

$(document).on('change', '[data-commonname="megamenu_bloc"]', function() {
  const $this = $(this);
  const nombreBlocMega = $this.val();
  const menuType = $this.data('menutype');
  const emplacementBloc = $this.data('emplacement');
  const emplacementGenerateurs = $(`.${emplacementBloc}`);
  const filenameComponent = `/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/admin/composants//composant_generateur_${menuType}_block_menu.php`;
  const menuNumber = $this.data('menunumber');


  var files = [];
  for(let i = 1; i <= nombreBlocMega; i++){
    const nouvelElement = doitAjouterNouveauxLiens(`titre${i}menu${menuNumber}`); 
    if(nouvelElement){
      var dataLink = {autreparametre: i,
        menuNumber: menuNumber
                    };
      const emplacement = $(`.${emplacementBloc}`);;
      files.push({ filename: filenameComponent, data: dataLink, emplacement: emplacement });
      //createAjaxRequest(filenameElement, dataLink, emplacement);
    }
 }
 processFilesSequentially(files);

  const selectCourant = $this.attr('name');
  const nombreOptionsSelect = $(`[name="${selectCourant}"] option`).length;

  for(let j = parseInt(nombreBlocMega) + 1; j <= nombreOptionsSelect; j++) {
      const aEffacer = `.menu_${menuNumber}_${j}`;
      $(aEffacer).remove();
  }
});

  // Événement déclenché lorsqu'un changement est apporté à l'élément avec le nom 'nombre_de_sousmenus_deroulant'
$(document).on('change', '[name="nombre_de_sousmenus_deroulant"]', function() {
  const nombre_de_sousmenus_deroulant = $(this).val();
  const nomElements = $(this).attr('data-elementname');
  const emplacementAjout = $(this).attr('data-emplacement');
  const fileNameElement = `/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/admin/composants/composant_generateur_menu_deroulant_${nomElements}.php`; 
  const typeGenerateur = $('[name="type_generateur"]').val();
  const elementParent = $('[name="element_parent"]').val();
  // Parcours du nombre de sous-menus déroulants
  for(let i = 1; i <= nombre_de_sousmenus_deroulant; i++){
      const nouvelElement = doitAjouterNouveauxLiens(`${nomElements}_${i}`); 
      if(nouvelElement){
        // Si un nouvel élément doit être ajouté, construit le champ de formulaire et l'ajoute
        // Requête AJAX pour charger le composant
        createAjaxRequest(fileNameElement, {
          autreparametre: i,
          type_generateur: typeGenerateur,
          element_parent: elementParent
        }, '.gestion_' + emplacementAjout);
      }
  }

  const selectCourant = $(this).attr('name');
  const nombreOptionsSelect = $(`[name="${selectCourant}"] option`).length;
  
  // Parcours du nombre d'options dans le select courant
  for(let j = parseInt(nombre_de_sousmenus_deroulant) + 1; j <= nombreOptionsSelect; j++){
      // Supprime l'élément
      const suppressionElement = `${emplacementAjout}_${j}`;
      $(`.${suppressionElement}`).remove();
  }
});
 // Événement déclenché lorsqu'un changement est apporté à l'élément avec le nom 'nombre_de_liste_de_liens'
 $(document).on('change', '[name="nombre_de_liens_dans_liste"]', function() {
  const nombre_de_liens_dans_liste = $(this).val();
  const nomElements = $(this).attr('data-elementname');
  const emplacementAjout = $(this).attr('data-emplacement');
  const fileNameElement = `/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/admin/composants/composant_generateur_${nomElements}.php`; 
  const typeGenerateur = $('[name="type_generateur"]').val();
  const elementParent = $('[name="element_parent"]').val();

  var files = []
  for(let i = 1; i <= nombre_de_liens_dans_liste; i++){
      const nouvelElement = doitAjouterNouveauxLiens(`${nomElements}_titre_${i}`); 
      if(nouvelElement){
        var dataLink = {autreparametre: i,
          nombre_de_liens_dans_liste: nombre_de_liens_dans_liste
                      };
        const emplacement = `.gestion_${emplacementAjout}`;
        files.push({ filename: fileNameElement, data: dataLink, emplacement: emplacement });
        //createAjaxRequest(filenameElement, dataLink, emplacement);
      }
  }
  processFilesSequentially(files);


  const selectCourant = $(this).attr('name');
  const nombreOptionsSelect = $(`[name="${selectCourant}"] option`).length;
  
  // Parcours du nombre d'options dans le select courant
  for(let j = parseInt(nombre_de_liens_dans_liste) + 1; j <= nombreOptionsSelect; j++){
      // Supprime l'élément
      const suppressionElement = `${emplacementAjout}_${j}`;
      $(`.${suppressionElement}`).remove();
  }
});


 // Événement déclenché lorsqu'un changement est apporté à l'élément avec le nom 'nombre_de_liste_de_liens'
 $(document).on('change', '[name="nombre_de_liste_de_liens"]', function() {
  const nombre_de_liste_de_liens = $(this).val();
  const nomElements = $(this).attr('data-elementname');
  const emplacementAjout = $(this).attr('data-emplacement');
  const fileNameElement = `/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/admin/composants/composant_generateur_${nomElements}.php`; 
  const typeGenerateur = $('[name="type_generateur"]').val();
  const elementParent = $('[name="element_parent"]').val();

  var files = []
  for(let i = 1; i <= nombre_de_liste_de_liens; i++){
      const nouvelElement = doitAjouterNouveauxLiens(`${nomElements}_titre_${i}`); 
      if(nouvelElement){
        var dataLink = {autreparametre: i,
                        nombre_de_liste_de_liens: nombre_de_liste_de_liens
                      };
        const emplacement = `.gestion_${emplacementAjout}`;
        files.push({ filename: fileNameElement, data: dataLink, emplacement: emplacement });
        //createAjaxRequest(filenameElement, dataLink, emplacement);
      }
  }
  processFilesSequentially(files);


  const selectCourant = $(this).attr('name');
  const nombreOptionsSelect = $(`[name="${selectCourant}"] option`).length;
  
  // Parcours du nombre d'options dans le select courant
  for(let j = parseInt(nombre_de_liste_de_liens) + 1; j <= nombreOptionsSelect; j++){
      // Supprime l'élément
      const suppressionElement = `${emplacementAjout}_${j}`;
      $(`.${suppressionElement}`).remove();
  }
});

function createMenuDeroulantResult(nombreDeSousMenus) {
  var resultat = 'deroulant;' + $('[name="menu_deroulant_titre"]').val() + ';' + nombreDeSousMenus + ';' + $('[name="menu_deroulant_icon"]').val() + ';';
  for(let i = 1; i <= nombreDeSousMenus; i++){
      resultat += $('[name="menuderoulant_titre_' + i + '"]').val() + ';' + $('[name="menuderoulant_url_' + i + '"]').val();
      if(i < nombreDeSousMenus){
          resultat += ';';
      }
  }
  return resultat;
}
function createListeDeLienResult(nombreDeSousMenus) {
  var resultat = $('[name="liste_de_liens_titre"]').val() + ';' + $('[name="liste_de_liens_entete"]').val() + ';';
  for(let i = 1; i <= nombreDeSousMenus; i++){
      resultat += $('[name="liste_de_liens_titre_' + i + '"]').val() + ';' + $('[name="liste_de_liens_url_' + i + '"]').val();
      if(i < nombreDeSousMenus){
          resultat += ';';
      }
  }
  return resultat;
}
function processListeDeLiens(typeMenuType, submitButton, elementParent, emplacement, filename) {     
  var nombre_de_liens_dans_liste = $('[name="nombre_de_liens_dans_liste"]').val();
  emplacement = '.resultat_liste_de_lien li nav';
  if(submitButton.attr("value") !== "Valider le résultat"){
    var mesdatas = {
      nombre_de_liens_dans_liste: nombre_de_liens_dans_liste,
        liste_de_liens_titre: $('[name="liste_de_liens_titre"]').val(),
        liste_de_liens_entete: $('[name="liste_de_liens_entete"]').val(),
        liste_de_liens_icon: $('[name="liste_de_liens_icon"]').val()
    };

    for(let i = 1; i <= nombre_de_liens_dans_liste; i++){
        mesdatas['liste_de_liens_titre_' + i] = $('[name="liste_de_liens_titre_' + i + '"]').val();
        mesdatas['liste_de_liens_url_' + i] = $('[name="liste_de_liens_url_' + i + '"]').val();
    }
    createAjaxRequest(filename, mesdatas, emplacement);
    submitButton.attr("value", "Valider le résultat");
    $('.les-resultats').removeClass('dontshow');
  } else {
      var resultat = createListeDeLienResult(nombre_de_liens_dans_liste);
      $('[name="' + elementParent + '"]').val(resultat);
      resetMenuGeneratorForm();
      submitButton.attr("value", "Valider le résultat");
      $('button.autoriser_reinit_form').removeClass('dontshow');
      $('.les-resultats').removeClass('dontshow');
  }
}

function processMenuDeroulant(typeMenuType, submitButton, elementParent, emplacement, filename) {
  var nombreDeSousMenus = $('[name="nombre_de_sousmenus_deroulant"]').val();
  const nameFile = `/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/admin/composants/${filename}.php`;
  if(submitButton.attr("value") !== "Valider le résultat"){
      var mesdatas = {
          nombreMenu: nombreDeSousMenus,
          menuDeroulantTitre: $('[name="menu_deroulant_titre"]').val(),
          menu_deroulant_icon: $('[name="menu_deroulant_icon"]').val()
      };

      for(let i = 1; i <= nombreDeSousMenus; i++){
          mesdatas['menuderoulant_titre_' + i] = $('[name="menuderoulant_titre_' + i + '"]').val();
          mesdatas['menuderoulant_url_' + i] = $('[name="menuderoulant_url_' + i + '"]').val();
      }
      createAjaxRequest(nameFile, mesdatas, emplacement);
      submitButton.attr("value", "Valider le résultat");
      $('.les-resultats').removeClass('dontshow');
  } else {
      var resultat = createMenuDeroulantResult(nombreDeSousMenus);
      $('[name="' + elementParent + '"]').val(resultat);
      resetMenuGeneratorForm();
      submitButton.attr("value", "Valider le résultat");
      $('button.autoriser_reinit_form').removeClass('dontshow');
      $('.les-resultats').removeClass('dontshow');
  }
}

  $(document).on('change', '[name="list-menu-type"]', function() {
    // Suppression de l'élément ayant la classe 'affichage_generateur'
    $('.affichage_generateur').remove();
    $this = $(this);
    const type_generateur = $this.data('type_generateur');
    const element_parent = $this.data('element_parent');
    // Gestion de la visibilité du bouton 'submit_generateurs_menu'
    if($(this).val() === '') {
        $('[name="submit_generateurs_menu"]').addClass('dontshow');
    } else {
        $('[name="submit_generateurs_menu"]').removeClass('dontshow');
    }

    // Génération d'une variable aléatoire unique
    const uniqueRandomVar = Math.random().toString(36).substr(2) + Date.now().toString(36);

    // Récupération des informations du select
    const OptionUtilisee = $(this).val();
    const selectedText = $(this).find("option:selected").text();

    // Localisation de l'emplacement où le générateur sera ajouté
    let emplacementGenerateurs = '.' + $(this).attr('data-emplacement');
 

    // Construction de l'URL du composant à charger
    const filenameComponent = `/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/admin/composants/composant_generateur_${OptionUtilisee}.php?${uniqueRandomVar}`;
    
    // Requête AJAX pour charger le composant
    createAjaxRequest(filenameComponent, {
        autreParametre : selectedText,
        type_generateur: type_generateur,
        element_parent: element_parent
    }, emplacementGenerateurs);
    

    // Affichage du bouton de soumission
    $('.submit_generateurs_menu').removeClass('dontshow');       
});


  $(document).on('click', '.loadImage', function(e) {
    e.preventDefault();
    $this = $(this);
    const idImage = $this.data('idimage');
    const urlImage = $this.data('urlimage');
    const previewImage = $this.data('imagepreview');

    // Ouvrir le Media Manager
    var mediaUploader = wp.media({
      title: 'Sélectionner une image',
      button: {
        text: 'Utiliser cette image'
      },
      multiple: false // Permet de sélectionner une seule image
    }).on('select', function() {
      var attachment = mediaUploader.state().get('selection').first().toJSON();

      // Afficher l'image sélectionnée et enregistrer l'ID dans un champ caché
      $(`#${previewImage}`).attr('src', attachment.url);
      $(`[name="${idImage}"]`).val(attachment.id);
      $(`[name="${urlImage}"]`).val(attachment.url);
     
    }).open();
  });
  
  $('button.questionmark').on('click', function(eevent) {
    eevent.preventDefault();
    $this = $(this);
    const buttonValue = $this.text();
    const parentForm = $this.data('parent');
    const formComponents = `${parentForm}_primaire`;
    const formHelp = `${parentForm}_secondaire`;
    const emplacement = `aide_${parentForm}`;
    const filename = `/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/admin/aide/${emplacement}.php`;
    const datalink = {};
    if (buttonValue === 'X'){
      $(`.${formHelp}`).remove();
      $this.text('?');
      $(`.${emplacement}`).removeClass('overlay');
      $(`.${formComponents}`).removeClass('dontshow');
    } else{
      $(`.${emplacement}`).addClass('overlay');
      $(`.${formComponents}`).addClass('dontshow');
      $this.text('X');
      createAjaxRequest(filename, datalink, `.${emplacement}`);
    }
  });

  $('button.questionmark_header').on('click', function(eevent) {
    eevent.preventDefault();
    $this = $(this);
    const buttonValue = $this.find('span').text();
    const parent = $this.data('parent');
    const fille =  $this.data('fille');
    const datavalue = $this.data('value');
    const emplacement = `aide_${parent}`;
    const filename_help = `/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/admin/aide/${emplacement}.php`;
    const filename_principal = `/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/admin/aide/${parent}.php`;
    const datalink = {dataname: datavalue};
    if (buttonValue === 'X'){
        $(`.${fille}`).remove();
        $this.find('span').text('?');
        createAjaxRequest(filename_principal, datalink, `.${parent}`);
        $(this).removeAttr('title').attr('title','Afficher la fenêtre d\'aide');
    } else {
        $this.find('span').text('X');
        $(`.${fille}`).remove();
        createAjaxRequest(filename_help, datalink, `.${parent}`);
        $(this).removeAttr('title').attr('title','Fermer la fenêtre d\'aide');
    }
  });


  $(document).on('keydown', function(e) {
    if (e.keyCode === 13 && e.ctrlKey) {
      $('.submitFormulaire input[name="submit"]').click();
    }
  });

});
  
