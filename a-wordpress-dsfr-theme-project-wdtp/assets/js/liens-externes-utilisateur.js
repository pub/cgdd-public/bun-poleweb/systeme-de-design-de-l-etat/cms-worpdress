
    jQuery(document).ready(function($) {
        var currentDomain = window.location.hostname;

        $('a').each(function() {
            var linkDomain = this.hostname;
            var $this = $(this);
            var containsImgTag = $this.find('img').length > 0;
            if (!containsImgTag) {
                
                var hrefValue = $this.attr('href');
                if (hrefValue && hrefValue.startsWith('/')){
                    hrefValue = 'https://' + currentDomain + hrefValue;
                }
                if (hrefValue && hrefValue.startsWith('http') && linkDomain !== currentDomain) {
                $(this).attr('title', $this.text() + ' - Lien externe');
                $(this).attr('rel', 'external');
                }
            }
        });
    });