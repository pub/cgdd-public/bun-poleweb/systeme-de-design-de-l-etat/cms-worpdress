<?php 
/**
 * The template for displaying the test category archive.
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */

get_header(); ?>



<!--h1 class="entry-title"><?php the_category(); ?></h1-->
<h1 class="entry-title">
    <?php
    $categories = get_the_category();
    $separator = ', ';
    $output = '';
    if ( ! empty( $categories ) ) {
        foreach( $categories as $category ) {
            $output .= esc_html( $category->name ) . $separator;
        }
        echo trim( $output, $separator );
    }
    ?>
</h1>

<?php



		
		if ( have_posts() ) : /*?>

			<header class="page-header">
				<?php
					// Hide the category title and description
				?>
			</header><!-- .page-header -->

			<?php */
			// Start the Loop
			while ( have_posts() ) : the_post();

				/*
				* Include the Post-Format-specific template for the content.
				* If you want to override this in a child theme, then include a file
				* called content-___.php (where ___ is the Post Format name) and that will be used instead.
				*/
				get_template_part( 'template-parts/content/content', 'actualites');

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>



<?php
get_sidebar();
get_footer();
/*

<div class="fr-grid-row fr-grid-row--gutters fr-mb-1w">
	<div class="fr-col-12 fr-col-md-4">
		<style>.fr-card.fr-enlarge-link:not(.fr-card--no-icon) .fr-card__content {padding-bottom: 2rem;}</style>
		<div class="fr-card fr-enlarge-link 
			fr-card--shadow
			">
			<div class="fr-card__body">
				<div class="fr-card__content fr-background-alt--green-bourgeon">
					<p class="fr-card__title"><a href="la-sndi/article/sndi"> SNDI</a></p>
					<div class="fr-card__desc">
						<p>La Stratégie Nationale de lutte contre la Déforestation Importée a été adoptée le 14 novembre 2018 pour mettre fin à l’importation de produits forestiers ou agricoles non durables contribuant à la déforestation.</p>
					</div>
				</div>
			</div>
			<div class="fr-card__header">
				<div class="fr-card__img"> 
					<img src="local/cache-gd2/02/9f365dc7ee90a81ab96a406eb42f36.jpg?1680500970" width="390" height="220" class="fr-responsive-img" alt="Image représentant une forêt vue du ciel" data-fr-js-ratio="true">
				</div>
			</div>
		</div>
	</div>
	<div class="fr-col-12 fr-col-md-4">
		<style>.fr-card.fr-enlarge-link:not(.fr-card--no-icon) .fr-card__content {padding-bottom: 2rem;}</style>
		<div class="fr-card fr-enlarge-link 
			fr-card--shadow
			">
			<div class="fr-card__body">
				<div class="fr-card__content fr-background-alt--green-bourgeon">
					<p class="fr-card__title"><a href="le-reglement-europeen/article/le-reglement-europeen"> Le règlement européen</a></p>
					<div class="fr-card__desc">
						<p>La Commission européenne a présenté le 17 novembre 2021 un projet de règlement pour s’assurer que les produits importés dans l’UE et exportés depuis l’UE n’ont pas engendré de déforestation ou de dégradation des forêts.</p>
					</div>
				</div>
			</div>
			<div class="fr-card__header">
				<div class="fr-card__img">
					<img src="local/cache-gd2/5c/fa495f4bdbf1a57021922d8de57114.jpg?1680500971" width="390" height="220" class="fr-responsive-img" alt="" data-fr-js-ratio="true">
				</div>
			</div>
		</div>
	</div>
	<div class="fr-col-12 fr-col-md-4">
		<style>.fr-card.fr-enlarge-link:not(.fr-card--no-icon) .fr-card__content {padding-bottom: 2rem;}</style>
		<div class="fr-card fr-enlarge-link 
			fr-card--shadow
			">
			<div class="fr-card__body">
				<div class="fr-card__content fr-background-alt--green-bourgeon">
					<p class="fr-card__title"><a href="l-evaluation-des-risques/article/tableau-de-bord-d-evaluation-des-risques-de-deforestation-lies-aux-importations"> L’évaluation des risques</a></p>
					<div class="fr-card__desc">
						<p>Tableau de bord d’évaluation des risques de déforestation liés aux importations françaises de soja</p>
					</div>
				</div>
			</div>
			<div class="fr-card__header">
				<div class="fr-card__img">
					<img src="local/cache-gd2/c2/28ae8934015001c3850b3c98846c90.jpg?1680500971" width="390" height="220" class="fr-responsive-img" alt="Image of a soybean plantation " data-fr-js-ratio="true">
				</div>
			</div>
		</div>
	</div>
</div>*/