<?php
/**
 * The template for displaying comments.
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback to wordpress_dsfr_project_comment() which is
 * located in the inc/template-tags.php file.
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */

// If the current post is protected by a password and the visitor has not yet
// entered the password, return early without loading the comments.
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) :
		?>
		<h2 class="comments-title">
			<?php
			$comments_number = get_comments_number();
			if ( '1' === $comments_number ) {
				printf(
					/* translators: %s: Post title. */
					esc_html__( 'One thought on &ldquo;%s&rdquo;', 'wordpress-dsfr-project' ),
					'<span>' . get_the_title() . '</span>'
				);
			} else {
				printf( // WPCS: XSS OK.
					/* translators: 1: Number of comments, 2: Post title. */
					esc_html( _nx( '%1$s thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', $comments_number, 'comments title', 'wordpress-dsfr-project' ) ),
					number_format_i18n( $comments_number ),
					'<span>' . get_the_title() . '</span>'
				);
			}
			?>
		</h2><!-- .comments-title -->

		<?php the_comments_navigation(); ?>

		<ol class="comment-list">
			<?php
			wp_list_comments(
				array(
					'style'      => 'ol',
					'short_ping' => true,
					'avatar_size' => 50,
				)
			);
			?>
		</ol><!-- .comment-list -->

		<?php the_comments_navigation(); ?>

	<?php endif; // Check for have_comments(). ?>

	<?php
	// If comments are closed and there are comments, let's leave a little note, shall we?
	if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
		?>
		<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'wordpress-dsfr-project' ); ?></p>
	<?php endif; ?>

	<?php
	comment_form(
		array(
			'title_reply_before' => '<h2 id="reply-title" class="comment-reply-title">',
			'title_reply_after'  => '</h2>',
		)
	);
	?>

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments -->

</div><!-- #comments