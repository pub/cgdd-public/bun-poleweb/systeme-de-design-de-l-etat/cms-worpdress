<?php
/**
 * The template for displaying error pages .
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */

get_header(); ?>

	<div  class="content-area">
		<main id="contenu" class="site-main">
			<div class="fr-py-16v" style="padding-top:0 !important;">
				<div class="fr-grid-row ">
					<div class='fr-col-12 fr-col-md-8'> 
						<div>
							<h1 class="fr-h1">Erreur d'analyse</h1>
							<p class="fr-text--sm">PARSE ERROR </p>
							<p>Une erreur s'est produite lors de l'analyse du script. </p>
							<p class="fr-text--sm">Essayez de rafraîchir la page ou essayez plus tard.<br>Si vous avez besoin d'une aide, merci de nous contacter.</p>
						</div>
					</div>
					<div class="fr-col-12 fr-col-md-4">
						<img src="<?php echo get_theme_file_uri();?>/assets/images/error_img.png" width="308" alt="">
					</div>
					<a href="<?php echo home_url(); ?>" class="fr-btn fr-btn--secondary">Accueil <span class="maison fr-icon-home-4-fill"></span></a>&nbsp;<a href="<?php echo get_option('dsfr_form_contact'); ?>" class="fr-btn fr-btn--secondary">Nous contacter<span class="maison fr-icon-mail-line"></span></a>
                    <?php if (WP_DEBUG) {
                        echo '<p><strong>Error Details:</strong><br>' . esc_html(debug_backtrace()[1]['function']) . '() on line ' . esc_html(debug_backtrace()[0]['line']) . ' in file ' . esc_html(debug_backtrace()[0]['file']) . '</p>';
                    } ?>
				</div>
			</div>
		</main>
	</div>
</div>
<?php
get_footer(); ?>
