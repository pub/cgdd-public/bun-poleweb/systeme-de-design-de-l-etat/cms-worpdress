<?php
/**
 * The footer for our theme.
 *
 * Displays the footer section of the site.
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */
	?> </div> <?php
			if (((get_option('dsfr_masquer_la_newsletter') !== false) AND (get_option('dsfr_masquer_la_newsletter') === 'non')) OR ((get_option('dsfr_masquer_partage_reseaux_sociaux') !== false) AND (get_option('dsfr_masquer_partage_reseaux_sociaux') === 'non'))) { ?>
				<div class="fr-follow">
					<div class="fr-container">
						<div class="fr-grid-row">
							<?php 
								// newsletter
								get_template_part( 'template-parts/footer/footer', 'newsletter' );
								get_template_part( 'template-parts/footer/footer', 'partage-reseaux-sociaux' );
							?>					
							
						</div>
					</div>
				</div>
			<?php } 
				if ((get_option('dsfr_masquer_haut_de_page') !== false) AND (get_option('dsfr_masquer_haut_de_page') === 'non')) {
					get_template_part( 'template-parts/content/content', 'backtotop' );
				}
			
			?>
			<footer class="fr-footer" role="contentinfo" id="footer">
			<?php
					if (get_option('dsfr_masquer_liste_de_lien') === 'non') {
						get_template_part( 'template-parts/footer/footer', 'liste-de-liens' );
					}

			?>
				<div class="fr-container">
					<?php $marianne_value = str_replace( "\r\n", '<br>', get_option( 'dsfr_texte_marianne' )); ?>
					<div class="fr-footer__body">
						<div class="fr-footer__brand fr-enlarge-link">
							<a href="/" title="Retour à l’accueil du site">
								<p class="fr-logo">
									<?php echo wp_kses_post( $marianne_value );?>
								</p>
							</a>
							<?php 
								if ((get_option('dsfr_masquer_logo_secondaire') !== false) AND (get_option('dsfr_masquer_logo_secondaire') === 'non')) {
									
									// Define the image size query variable before calling get_template_part
									set_query_var( 'image_size', '200px' );
									get_template_part( 'template-parts/footer/footer', 'second-logo' );
								}							
							?>
						</div>
						<div class="fr-footer__content">
							<?php get_template_part( 'template-parts/footer/footer', 'ecosysteme' ); ?>							
						</div>						
					</div>					
					<?php 
					// partenaires
						if ((get_option('dsfr_masquer_partenaires') !== false) AND (get_option('dsfr_masquer_partenaires') === 'non')) {
							get_template_part( 'template-parts/footer/footer', 'partenaires' );
						}									
					?>
					<div class="fr-footer__bottom">
						<ul class="fr-footer__bottom-list">
							<?php get_template_part( 'template-parts/footer/footer', 'obligations-legales' ); ?>
							<?php 					
								if (get_option('dsfr_masquer_page_contact')	 === 'non'){		
									$contact_in = array("1", "3", "4", "7");
									if (in_array(get_option('dsfr_emplacement_contact'), $contact_in)) {			
										echo '<li class="fr-footer__bottom-item"><a class="fr-footer__bottom-link" role="button" tabindex="0" href='.get_option('dsfr_form_contact').'  target="_self">'.get_option('dsfr_titre_url_contact').'</a></li>';
									}
								}
							?>
							<?php if ((get_option('dsfr_afficher_bouton_cookies') === 'oui') && (get_option('dsfr_masquer_cookies') === "non")){ ?>
								<li class="fr-footer__bottom-item">
									<a class="fr-footer__bottom-link" role="button" tabindex="0" href="<?php echo home_url( $_SERVER['REQUEST_URI'] );?>#consentement">Gestion des cookies</a>
								</li>
							<?php } ?>
							<?php if (get_option('dsfr_masquer_parametres_affichage') === 'non') { ?>
								<li class="fr-footer__bottom-item">
                                    <button class="fr-btn--display fr-footer__bottom-link" aria-controls="fr-theme-modal" data-fr-opened="false" title="Paramètres d'affichage" data-fr-js-modal-button="true">
                                        Paramètres d'affichage
                                    </button>
                                </li>
							<?php } ?>
						</ul>
						<?php if (get_option('dsfr_licence') !== false) { ?>
							<div class="fr-footer__bottom-copy">
								<p><?php echo get_option('dsfr_licence'); ?></p>
							</div>
						<?php } ?>
					</div>
				</div>
			</footer><!-- #footer -->
		</div><!-- #container -->
	</div><!-- #content -->
</div><!-- #page -->

<?php 
 
	if (get_option('dsfr_masquer_cookies') === 'non') {
		get_template_part( 'template-parts/content/content', 'cookies-consent' );
	}

						
	 wp_footer(); 

	$stylesheet_icons_dsfr = get_theme_file_uri( 'dsfr/utility/icons/icons.css' );
	$stylesheet_theme_tac_dsfr = get_theme_file_uri( 'dsfr/dsfr-theme-tac.css' );
	$stylesheet_theme_custom_dsfr = get_theme_file_uri( 'style.css' );
    printf( '<link rel="stylesheet" href="%s" id="dsfr-icons">', esc_url( $stylesheet_icons_dsfr ) );
	printf( '<link rel="stylesheet" href="%s" id="dsfr-tac">', esc_url( $stylesheet_theme_tac_dsfr ) );
	printf( '<link rel="stylesheet" href="%s?version=%s" id="custom-style-dsfr">', esc_url( $stylesheet_theme_custom_dsfr ), uniqid(uniqid()) );

	//wp_enqueue_style( 'custom-admin-style', $stylesheet_theme_custom_dsfr, false, '1.0.3' );
	$js_custom_js = get_theme_file_uri( 'assets/js/custom-theme.js' );
	$js_module_dsfr = get_theme_file_uri( 'dsfr/dsfr.module.js' );
	$js_nomodule_dsfr = get_theme_file_uri( 'dsfr/dsfr.nomodule.js' );
	printf( '<script src="%s?%s" id="dsfr-custom-js"></script>', esc_url( $js_custom_js ), uniqid(uniqid()) );
	printf( '<script module src="%s" id="dsfr-module" module></script>', esc_url( $js_module_dsfr ) );
	printf( '<script nomodule src="%s" id="dsfr-nomodule" nomodule></script>', esc_url( $js_nomodule_dsfr ) );


	get_template_part( 'template-parts/excerpt/excerpt', 'modal-parametres-affichage' ); 
	if(get_option('masquer_statistiques') === 'non') {
		get_template_part( 'template-parts/excerpt/excerpt', 'statistiques' ); 
	}
	// traitement des liens externes
	if (get_option('dsfr_liens_externe_que_faire') === 'automatique') {
		$liens_externes_automatique = get_theme_file_uri( 'assets/js/liens-externes-automatique.js' );
		printf( '<script src="%s?%s" id="dsfr-custom-liens-externes-automatiques"></script>', esc_url( $liens_externes_automatique ), '1' );
	} else{
		$liens_externes_utilisateur = get_theme_file_uri( 'assets/js/liens-externes-utilisateur.js' );
		printf( '<script src="%s?%s" id="dsfr-custom-liens-externes-utilisateur"></script>', esc_url( $liens_externes_utilisateur ), uniqid(uniqid()) );
	}
?>
</body>
</html>
