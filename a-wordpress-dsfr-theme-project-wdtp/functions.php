<?php
	/**
	 * Wordpress DSFR Project functions and definitions
	 *
	 * @link https://developer.wordpress.org/themes/basics/theme-functions/
	 *
	 * @package WordPress
	 * @subpackage Wordpress_DSFR_Project
	 * @since Wordpress_DSFR_Project 1.0
	 */
	
	/**
	 * Enqueue scripts and styles.
	 */
	function wordpress_dsfr_project_scripts() {
		// Enqueue main stylesheet.	
		wp_enqueue_style('dsfr-style', get_theme_file_uri().'/dsfr/dsfr.css');
		
	}
	add_action( 'wp_enqueue_scripts', 'wordpress_dsfr_project_scripts' );	
	/**
	 * Register navigation menus.
	 */
	function wordpress_dsfr_project_register_menus() {
		register_nav_menus(
			array(
				'primary' => __( 'Primary Menu', 'wordpress-dsfr-project' ),
				'footer'  => __( 'Footer Menu', 'wordpress-dsfr-project' ),
			)
		);
	}
	add_action( 'init', 'wordpress_dsfr_project_register_menus' );	
	// Lauch JQuery
	function jquery_scripts() {
		wp_enqueue_script('jquery');
	}
	add_action('wp_enqueue_scripts', 'jquery_scripts');	  
	/**
	 * Add theme support for various features.
	 */
	function wordpress_dsfr_project_setup() {
		// Add support for automatic feed links.
		add_theme_support( 'automatic-feed-links' );
	
		// Add support for post thumbnails.
		add_theme_support( 'post-thumbnails' );
	
		// Add support for title tag.
		add_theme_support( 'title-tag' );
	
		// Add support for HTML5 markup.
		add_theme_support(
			'html5',
			array(
				'comment-list',
				'comment-form',
				'search-form',
				'gallery',
				'caption',
			)
		);	
		// Add support for editor styles.
		add_editor_style( 'style.css' );
	}
	add_action( 'after_setup_theme', 'wordpress_dsfr_project_setup' );	
	/**
	 * Customize excerpt length.
	 */
	function wordpress_dsfr_project_custom_excerpt_length( $length ) {
		return 20;
	}
	add_filter( 'excerpt_length', 'wordpress_dsfr_project_custom_excerpt_length', 999 );
	
	/**
	 * Customize excerpt more text.
	 */
	function wordpress_dsfr_project_custom_excerpt_more( $more ) {
		return '...';
	}
	add_filter( 'excerpt_more', 'wordpress_dsfr_project_custom_excerpt_more' );
	
	/**
	 * Customize search form.
	 */
	function wordpress_dsfr_project_custom_search_form( $form ) {
		$form = '
		<form role="search" method="get" class="search-form" action="' . esc_url( home_url( '/' ) ) . '">
			<label>
				<span class="screen-reader-text">' . _x( 'Search for:', 'label', 'wordpress-dsfr-project' ) . '</span>
				<input type="search" class="search-field" placeholder="' . esc_attr_x( 'Search &hellip;', 'placeholder', 'wordpress-dsfr-project' ) . '" value="' . get_search_query() . '" name="s" />
			</label>
			<button type="submit" class="search-submit">' . _x( 'Search', 'submit button', 'wordpress-dsfr-project' ) . '</button>
		</form>';	
		return $form;
	}
	add_filter( 'get_search_form', 'wordpress_dsfr_project_custom_search_form' );
	// Hooks
add_filter( 'upload_mimes', 'capitaine_mime_types' );
add_filter( 'wp_check_filetype_and_ext', 'capitaine_file_types', 10, 4 );

// Autoriser l'import des fichiers SVG et WEBP
function capitaine_mime_types( $mimes ){
    $mimes['svg'] = 'image/svg+xml';
    $mimes['webp'] = 'image/webp';
    return $mimes;
}

// Contrôle de l'import d'un WEBP
function capitaine_file_types( $types, $file, $filename, $mimes ) {
	if ( false !== strpos( $filename, '.webp' ) ) {
    	$types['ext'] = 'webp';
   		$types['type'] = 'image/webp';
	}
	return $types;
}
	/**
	 * Customize comment form.
	 */
	function wordpress_dsfr_project_custom_comment_form() {
		$commenter = wp_get_current_commenter();
		$req       = get_option( 'require_name_email' );
		$aria_req  = ( $req ? " aria-required='true'" : '' );
		$fields    = array(
			'author' => '<p class="comment-form-author"><label for="author">' . __( 'Name', 'wordpress-dsfr-project' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
						'<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>',
			'email'  => '<p class="comment-form-email"><label for="email">' . __( 'Email', 'wordpress-dsfr-project' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
						'<input id="email" name="email" type="email" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p>',
			'url'    => '<p class="comment-form-url"><label for="url">' . __( 'Website', 'wordpress-dsfr-project' ) . '</label> ' .
						'<input id="url" name="url" type="url" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></p>',
		);
		$required_text = sprintf( ' ' . __( 'Required fields are marked %s', 'wordpress-dsfr-project' ), '<span class="required">*</span>' );
	
		comment_form(
			array(
				'fields'               => $fields,
				'comment_field'        => '<p class="comment-form-comment"><label for="comment">' . __( 'Comment', 'wordpress-dsfr-project' ) . '</label> <textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>',
				'title_reply'          => __( 'Leave a Reply', 'wordpress-dsfr-project' ),
				'title_reply_to'       => __( 'Leave a Reply to %s', 'wordpress-dsfr-project' ),
				'cancel_reply_link'    => __( 'Cancel reply', 'wordpress-dsfr-project' ),
				'label_submit'         => __( 'Post Comment', 'wordpress-dsfr-project' ),
				'comment_notes_before' => '<p class="comment-notes">' . __( 'Your email address will not be published.', 'wordpress-dsfr-project' ) . ( $req ? $required_text : '' ) . '</p>',
				'comment_notes_after'  => '',
			)
		);
	}
	add_action( 'comment_form_before', 'wordpress_dsfr_project_custom_comment_form' );
	
	// Fonction d'activation du thème
	function design_system_admin_theme_activation() {
	    // Ajouter les options personnalisées dans la table wp_options
		add_option('plugin_dsfr_installe','non');
		add_option('dsfr_masquer_commentaires', 'oui');
	    add_option('dsfr_max_liste_de_liens', '6');
	    add_option('dsfr_max_menus', '9');
	    add_option('dsfr_max_liens_menu_deroulant', '9');	
		add_option('dsfr_max_partenaires_principaux', '2');	
		add_option('dsfr_max_partenaires_secondaires', '4');	
		add_option('dsfr_max_ecosysteme', '4');	
		add_option('dsfr_max_legal', '8');	
	    add_option('dsfr_nom_de_domaine_en_production', 'mettre-ici-votre-url-de-domaine-de-production.com');
	    add_option('dsfr_titre_onglet_navigateur', 'DSFR Theme 2.0 - Wordpress');
	    add_option('dsfr_meta_description', 'Ceci est la méta decription du site qui apparaît dans la balise meta description de head');
	    add_option('dsfr_liens_externe_que_faire', 'utilisateur');
		add_option('dsfr_elements_recherche', '10');
	    add_option('dsfr_addresse_email_responsable', 'mikael.folio@developpement-durable.gouv.fr');
	    add_option('dsfr_form_contact', '/dsfr_form_contact');
	    add_option('dsfr_texte_marianne', 'Gouvernement');
	    add_option('dsfr_masquer_le_titre', 'oui');
	    add_option('dsfr_titre_du_site', get_option('blogname'));
	    add_option('dsfr_masquer_le_slogan', 'oui');
	    add_option('dsfr_slogan_du_site', get_option('blogdescription'));
	    add_option('dsfr_drapeau_en_berne', 'non');
	    add_option('dsfr_masquer_haut_de_page', 'oui');
	    add_option('dsfr_home_en_h1', 'non');
	    add_option('dsfr_masquer_recherche', 'oui');
	    add_option('dsfr_masquer_message_alerte', 'oui');
	    add_option('dsfr_message_alerte', 'Label titre bandeau d\'information importante, avec un texte assez long, et un <a href="#" target="_blank">lien au fil du texte</a> lorem ipsum dolor sit lorem ipsum dolor sit.');
	    add_option('dsfr_masquer_statistiques', 'oui');
	    add_option('dsfr_script_statistiques', '');
	    add_option('dsfr_masquer_liste_de_lien', 'oui');
	    //add_option('type_de_liste_de_liens', 'interne');
	    add_option('dsfr_nombre_de_liste_de_liens', '1');
		for ($i = 1; $i <= get_option('dsfr_max_liste_de_liens'); $i++) {
			$nom_option = 'dsfr_liste_de_liens_' . $i;
			if ($i <= get_option('dsfr_nombre_de_liste_de_liens')) {
				add_option($nom_option, 'Navigation;Lien 1;/;lien2;/;Lien 3;/;Lien 4;/');
			} else {
				add_option($nom_option, '');
			}		
		}
	    add_option('dsfr_masquer_menu', 'oui');
	    //add_option('type_de_menu', '');
	    add_option('dsfr_nombre_de_menu', '1');
		for ($i = 1; $i <= get_option('dsfr_max_menus'); $i++) {
			$nom_option = 'dsfr_menu_' . $i;
			if ($i <= get_option('dsfr_nombre_de_menu')) {
				add_option($nom_option, 'direct;Accueil;/;fr-icon-question-fill');
			} else {
				add_option($nom_option, '');
			}		
		}
	    add_option('dsfr_masquer_partenaires', 'oui');
	    add_option('dsfr_nombre_de_partenaires_principaux', '1');
		for ($i = 1; $i <= get_option('dsfr_max_partenaires_principaux'); $i++) {
			$nom_option = 'partenaires_principaux_' . $i;
			$bdd_partenaires = 'dsfr_'.$nom_option;
			if ($i <= get_option('dsfr_nombre_de_partenaires_principaux')) {
				add_option($bdd_partenaires, 'direct;Nom partenaire principal 1;https://www.gouvernement.fr/;/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/gouvernement.jpg');
			} else {
				add_option($bdd_partenaires, '');
			}		
		}
		add_option('dsfr_nombre_de_partenaires_secondaires', '1');
		for ($i = 1; $i <= get_option('dsfr_max_partenaires_secondaires'); $i++) {
			$nom_option = 'partenaires_secondaires_' . $i;
			$bdd_ps = 'dsfr_'.$nom_option;
			if ($i <= get_option('dsfr_nombre_de_partenaires_secondaires')) {
				add_option($bdd_ps, 'direct;Nom secondaire principal 1;https://www.ecologie.gouv.fr/;/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/ministere-sport-logo.jpg');
			} else {
				add_option($bdd_ps, '');
			}		
		}
	    add_option('dsfr_masquer_cookies', 'oui');		
	    add_option('dsfr_liste_des_cookies', '(tarteaucitron.job = tarteaucitron.job || []).push(\'matomo\');(tarteaucitron.job = tarteaucitron.job || []).push(\'youtube\');(tarteaucitron.job = tarteaucitron.job || []).push(\'dailymotion\');');
	    add_option('dsfr_modifier_texte_cookies', 'non');
	    add_option('dsfr_titre_texte_cookies', 'À propos des cookies sur ce site');
	    add_option('dsfr_texte_cookies', 'Un cookie est un petit fichier informatique, un traceur, déposé et lu lors de la consultation d’un site internet. Il permet de conserver des données utilisateur afin de faciliter la navigation et de permettre certaines fonctionnalités.');
	    add_option('dsfr_masquer_parametres_affichage', 'oui');
	    add_option('dsfr_parametre_affichage_defaut', 'dark');
	    add_option('dsfr_pad_dans_footer', 'non');
	    add_option('dsfr_masquer_liste_acces_rapide', 'oui');
	    add_option('dsfr_lien_dacces_rapide1', '');
	    add_option('dsfr_lien_dacces_rapide2', '');
	    add_option('dsfr_lien_dacces_rapide3', '');
	    add_option('dsfr_masquer_la_newsletter', 'oui');
	    add_option('dsfr_titre_form_newsletter', 'Ma newsletter');
	    add_option('dsfr_slogan_form_newsletter', 'La meilleure des newsletters');
	    add_option('dsfr_description_newsletter', 'Ma description de newsletter');
	    add_option('dsfr_type_formulaire_newsletter', 'formulaire');
	    add_option('dsfr_url_formulaire_newsletter', '/');
	    add_option('dsfr_titre_bouton_sabonner', 'S\'abonner');
	    add_option('dsfr_nom_champ_input_form', 'mail');
	    add_option('dsfr_masquer_partage_reseaux_sociaux', 'oui');
	    add_option('dsfr_titre_partage_reseaux_sociaux', 'Nous suivre sur');
	    add_option('dsfr_url_facebook', '');
	    add_option('dsfr_url_twitter', '');
	    add_option('dsfr_url_youtube', '');
	    add_option('dsfr_url_instagram', '');
	    add_option('dsfr_url_linkedin', '');
	    add_option('dsfr_url_rss', '');
	    add_option('dsfr_masquer_logo_secondaire', 'oui');
	    add_option('dsfr_image_second_logo', '');
		add_option('dsfr_image_second_logo2', '');
	    add_option('dsfr_afficher_selection_type_logo', '');
	    add_option('dsfr_code_second_logo', '');
	    add_option('dsfr_alternative_logo_secondaire', '');	
	    add_option('dsfr_nombre_de_liens_ecosysteme', '4');
		add_option('dsfr_masquer_liens_ecosysteme', 'non');
		add_option('dsfr_lien_eco_1', 'direct;data.gouv.fr;https://data.gouv.fr/;');
		add_option('dsfr_lien_eco_2', 'direct;service-public.fr;https://www.service-public.fr/;');
		add_option('dsfr_lien_eco_3', 'direct;legifrance.gouv.fr;https://www.legifrance.gouv.fr/;');
		add_option('dsfr_lien_eco_4', 'direct;gouvernement.fr;https://www.gouvernement.fr/;');
		add_option('dsfr_masquer_liens_legaux', 'non');
		add_option('dsfr_nombre_de_liens_obligations_legales', '4');
		add_option('dsfr_lien_legal_1', 'direct;accessibilité : non/partiellement/totalement conforme;/;');
		add_option('dsfr_lien_legal_2', 'direct;Mentions légales;/;');
		add_option('dsfr_lien_legal_3', 'direct;données personnelles;/;');
		add_option('dsfr_lien_legal_4', 'direct;Gestion des cookies;/;');
		add_option('dsfr_afficher_bouton_cookies', 'non');
		add_option('dsfr_masquer_licence', 'non');
		add_option('dsfr_licence', 'Sauf mention contraire, tous les contenus de ce site sont sous licence etalab-2.0');
		add_option('dsfr_masquer_description_footer', 'oui');
		add_option('dsfr_description_footer', '');
		
	}
	add_action('after_switch_theme', 'design_system_admin_theme_activation');
	
	// Fonction de désinstallation du thème
	function design_system_admin_theme_uninstall() {
	    // Supprimer les options de la table wp_options
		delete_option('plugin_dsfr_installe');
		delete_option('dsfr_masquer_commentaires');
		delete_option('dsfr_masquer_cookies');
		delete_option('dsfr_nom_de_domaine_en_production');
		delete_option('dsfr_masquer_le_titre');
	    delete_option('dsfr_titre_onglet_navigateur');
	    delete_option('dsfr_meta_description');
	    delete_option('dsfr_liens_externe_que_faire');	
		delete_option('dsfr_elements_recherche');	
	    delete_option('dsfr_addresse_email_responsable');
	    delete_option('dsfr_form_contact');
	    delete_option('dsfr_texte_marianne');
	    delete_option('dsfr_titre_du_site');
	    delete_option('dsfr_masquer_le_slogan');
	    delete_option('dsfr_slogan_du_site');
	    delete_option('dsfr_drapeau_en_berne');
	    delete_option('dsfr_masquer_haut_de_page');
	    delete_option('dsfr_home_en_h1');
	    delete_option('dsfr_masquer_recherche');
	    delete_option('dsfr_masquer_message_alerte');
	    delete_option('dsfr_message_alerte');
	    delete_option('dsfr_masquer_statistiques');
	    delete_option('dsfr_script_statistiques');
	    delete_option('dsfr_masquer_liste_de_lien');
	    //delete_option('type_de_liste_de_liens');
	    delete_option('dsfr_nombre_de_liste_de_liens');
		for ($i = 1; $i <= get_option('dsfr_max_liste_de_liens'); $i++) {
			$nom_option = 'dsfr_liste_de_liens_' . $i;
			delete_option($nom_option);
		}
	    delete_option('dsfr_masquer_menu');
	    delete_option('type_de_menu');
	    delete_option('dsfr_nombre_de_menu');
		for ($i = 1; $i <= get_option('dsfr_max_menus'); $i++) {
			$nom_option = 'dsfr_menu_' . $i;
			delete_option($nom_option);
		}
	    delete_option('dsfr_masquer_partenaires');
	    delete_option('dsfr_nombre_de_partenaires_principaux');
		for ($i = 1; $i <= get_option('dsfr_max_partenaires_principaux'); $i++) {
			$nom_option = 'dsfr_partenaires_principaux_' . $i;
			delete_option($nom_option);
		}
		delete_option('dsfr_nombre_de_partenaires_secondaires');
		for ($i = 1; $i <= get_option('dsfr_max_partenaires_secondaires'); $i++) {
			$nom_option = 'dsfr_partenaires_secondaires_' . $i;
			delete_option($nom_option);		
		}
	    delete_option('dsfr_liste_des_cookies');
	    delete_option('dsfr_modifier_texte_cookies');
	    delete_option('dsfr_titre_texte_cookies');
	    delete_option('dsfr_texte_cookies');
	    delete_option('dsfr_masquer_parametres_affichage');
	    delete_option('dsfr_parametre_affichage_defaut');
	    delete_option('dsfr_pad_dans_footer');
	    delete_option('dsfr_masquer_liste_acces_rapide');
	    delete_option('dsfr_lien_dacces_rapide1');
	    delete_option('dsfr_lien_dacces_rapide2');
	    delete_option('dsfr_lien_dacces_rapide3');
	    delete_option('dsfr_masquer_la_newsletter');
	    delete_option('dsfr_titre_form_newsletter');
	    delete_option('dsfr_slogan_form_newsletter');
	    delete_option('dsfr_description_newsletter');
	    delete_option('dsfr_type_formulaire_newsletter');
	    delete_option('dsfr_url_formulaire_newsletter');
	    delete_option('dsfr_titre_bouton_sabonner');
	    delete_option('dsfr_nom_champ_input_form');
	    delete_option('dsfr_masquer_partage_reseaux_sociaux');
	    delete_option('dsfr_titre_partage_reseaux_sociaux');
	    delete_option('dsfr_url_facebook');
	    delete_option('dsfr_url_twitter');
	    delete_option('dsfr_url_youtube');
	    delete_option('dsfr_url_instagram');
	    delete_option('dsfr_url_linkedin');
	    delete_option('dsfr_url_rss');
		delete_option('dsfr_masquer_logo_secondaire');
		delete_option('dsfr_code_second_logo');
		delete_option('dsfr_image_second_logo');
		delete_option('dsfr_image_second_logo2');
		delete_option('dsfr_afficher_selection_type_logo');
	    delete_option('dsfr_alternative_logo_secondaire');	
		delete_option('dsfr_masquer_liens_ecosysteme');
	    delete_option('dsfr_nombre_de_liens_ecosysteme');
		for ($i = 1; $i <= get_option('dsfr_max_ecosysteme'); $i++) {
			$nom_option = 'dsfr_lien_eco_' . $i;		
			delete_option($nom_option);		
		}
		delete_option('dsfr_nombre_de_liens_obligations_legales');
		for ($i = 1; $i <= get_option('dsfr_max_legal'); $i++) {
			$nom_option = 'dsfr_lien_legal_' . $i;
			delete_option($nom_option);		
		}
		delete_option('dsfr_afficher_bouton_cookies');
		delete_option('dsfr_masquer_licence');
		delete_option('dsfr_licence');
	    delete_option('dsfr_max_liste_de_liens');
	    delete_option('dsfr_max_menus');
	    delete_option('dsfr_max_liens_menu_deroulant');	
		delete_option('dsfr_max_partenaires_principaux');	
		delete_option('dsfr_max_partenaires_secondaires');	
		delete_option('dsfr_max_ecosysteme');	
		delete_option('dsfr_max_legal');	
		delete_option('dsfr_masquer_description_footer');
		delete_option('dsfr_description_footer');
	    delete_option('dsfr_masquer_liens_legaux');

	
	}
	add_action('switch_theme', 'design_system_admin_theme_uninstall');
	
	/**
	* Customize comment output.
	*/
	function wordpress_dsfr_project_custom_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment;
		?>
		<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
			<article id="div-comment-<?php comment_ID(); ?>" class="comment-body">
				<footer class="comment-meta">
					<div class="comment-author vcard">
						<?php
							$avatar_size = 64;
							if ( '0' != $comment->comment_parent ) {
								$avatar_size = 48;
							}
							
							echo get_avatar( $comment, $avatar_size );
							
							/* translators: %s: comment author link */
							printf(
								/* translators: 1: comment author link, 2: date and time */
								__( '%1$s on %2$s <span class="says">said:</span>', 'wordpress-dsfr-project' ),
								sprintf( '<span class="fn">%s</span>', get_comment_author_link() ),
								sprintf(
									'<a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
									esc_url( get_comment_link( $comment->comment_ID ) ),
									get_comment_time( 'c' ),
									/* translators: 1: date, 2: time */
									sprintf( __( '%1$s at %2$s', 'wordpress-dsfr-project' ), get_comment_date(), get_comment_time() )
								)
							);
							?>
						<?php edit_comment_link( __( 'Edit', 'wordpress-dsfr-project' ), '<span class="edit-link">', '</span>' ); ?>
					</div>
					<!-- .comment-author -->
					<?php if ( '0' == $comment->comment_approved ) : ?>
					<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'wordpress-dsfr-project' ); ?></p>
					<?php endif; ?>
				</footer>
				<!-- .comment-meta -->
				<div class="comment-content">
					<?php comment_text(); ?>
				</div>
				<!-- .comment-content -->
				<div class="reply">
					<?php
						comment_reply_link(
							array_merge(
								$args,
								array(
									'add_below' => 'div-comment',
									'depth'     => $depth,
									'max_depth' => $args['max_depth'],
								)
							)
						);
						?>
				</div>
				<!-- .reply -->
			</article>
		</li>
		<!-- .comment-body -->
	<?php
	}
	add_action( 'wp_enqueue_scripts', 'wordpress_dsfr_project_scripts' );
		
		// Génération d'un fil d'ariane 
	function mon_fil_dariane() {
		global $post;
		echo '
		<div class="fr-container">
			<nav role="navigation" class="fr-breadcrumb" aria-label="vous êtes ici :" data-fr-js-breadcrumb="true">
				<button class="fr-breadcrumb__button" aria-expanded="false" aria-controls="breadcrumb-1" data-fr-js-collapse-button="true">Voir le fil d’Ariane</button>
				<div class="fr-collapse" id="breadcrumb-1" data-fr-js-collapse="true">
					<ol class="fr-breadcrumb__list">';
						if ( !is_home() && !is_front_page() ) {
							echo '<li><a href="' . get_home_url() . '">Accueil</a></li>';
							if ( is_category() || is_single() ) {
								$category = get_the_category();
								if ( $category ) {
									$category_values = array_values( $category );
									$last_category = end( $category_values );
									$cat_parents = rtrim( get_category_parents( $last_category->term_id, true, ',' ), ',' );
									$cat_parents = explode( ',', $cat_parents );
									foreach ( $cat_parents as $parent ) {
										echo '<li>' . $parent . '</li>';
									}
								}
							} 
							if (is_search()) {
								echo '<li>Recherche</li>';echo '<li>' . $_GET['s'] . '</li>';
							} 
							if ( is_page() && !is_front_page() ) {
								$ancestors = get_post_ancestors( $post->ID );
								if ( $ancestors ) {
									$ancestors = array_reverse( $ancestors );
									foreach ( $ancestors as $ancestor ) {
										echo '<li><a href="' . get_permalink( $ancestor ) . '">' . get_the_title( $ancestor ) . '</a></li>';
									}
								}
								echo '<li>' . get_the_title() . '</li>';
							} 
							if ( is_single() ) {
								echo '<li>' . get_the_title() . '</li>';
							} 
							if ( !is_category() && !is_page() && !is_single() && !is_front_page() ) {
								echo '<li>' . get_the_title() . '</li>';
							}
							
						} else {
							echo '<li>' . get_the_title() . '</li>';
						}
						echo ' 
					</ol>
				</div>
			</nav>
		</div>';
	}
		
	// Ajout des classes DSFR au body  
	function ajouter_classe_body( $classes ) {
		$classes[] = 'front path-frontpage page-node-type-page';
		return $classes;
	}
	add_filter( 'body_class', 'ajouter_classe_body' );
	
	// Activation / Désactivation commentaires articles. Attention choix glogale
	function masquer_commentaires( $open, $post_id ) {
		$masquer = get_option( 'dsfr_masquer_commentaires' );
		if ( $masquer == 'oui' ) {
			return false;
		}
		return $open;
	}
	add_filter( 'comments_open', 'masquer_commentaires', 10, 2 );
		
	// Gestion activation / désactivation des différents options de l page de configuration
	function va_et_vient($id_checkbox, $checked, $activation = '') {
		$getoption = (((get_option($id_checkbox) !== false ) AND (!empty(get_option($id_checkbox)))) ? get_option($id_checkbox) : $checked);
		$desactivation = '';
		if (($id_checkbox === 'masquer_liste_acces_rapide') AND (get_option('dsfr_masquer_parametres_affichage') === 'non')){
			$desactivation = ' disabled="disabled"';
		}
		if (!empty($activation)) {
			$getoption = $activation;
		}
		$switch = $getoption;
		echo '
		<div class="ic-Super-toggle--on-off">
			<input' . $desactivation. ' type="checkbox" id="'.$id_checkbox.'" class="ic-Super-toggle__input"'. (($switch === 'oui') ? ' checked' : '' ).' value="'.$switch.'" name="'.$id_checkbox.'" data-informations="commandes" data-link="'.get_template_directory_uri().'">
			<label class="ic-Super-toggle__label" for="'.$id_checkbox.'">
				<div class="ic-Super-toggle__screenreader">Enable flux capacitor</div>
				<div class="ic-Super-toggle__disabled-msg" data-checked="On" data-unchecked="Bouton désactivé" aria-hidden="true"></div>
				<div class="ic-Super-toggle-switch" aria-hidden="true">
					<div class="ic-Super-toggle-option-LEFT" aria-hidden="true">
						<svg class="ic-Super-toggle__svg" xmlns="http://www.w3.org/2000/svg" version="1.1" x="0" y="0" width="548.9" height="548.9" viewBox="0 0 548.9 548.9" xml:space="preserve">
						<polygon points="449.3 48 195.5 301.8 99.5 205.9 0 305.4 95.9 401.4 195.5 500.9 295 401.4 548.9 147.5 " fill="#fff" />
						</svg>
					</div>
					<div class="ic-Super-toggle-option-RIGHT" aria-hidden="true">
						<svg class="ic-Super-toggle__svg" xmlns="http://www.w3.org/2000/svg" version="1.1" x="0" y="0" width="28" height="28" viewBox="0 0 28 28" xml:space="preserve">
						<polygon points="28 22.4 19.6 14 28 5.6 22.4 0 14 8.4 5.6 0 0 5.6 8.4 14 0 22.4 5.6 28 14 19.6 22.4 28 " fill="#fff" />
						</svg>
					</div>
				</div>
			</label>
		</div>
		<input type="hidden" name="_'.$id_checkbox.'" value="'.$switch.'" />';
		?>	
		<script>
			jQuery(document).ready(function($) {
			// Sélectionner l'élément input de type checkbox
			var checkbox = $('input[type="checkbox"]#<?php echo $id_checkbox; ?>');
			var hidden = $('input[type="hidden"][name="_<?php echo $id_checkbox; ?>"]');
			var masque = $('#_<?php echo $id_checkbox; ?>');
			if (checkbox.is(':checked')) {
				hidden.val('oui');
				checkbox.val('oui');
				masque.hide();
			} else {
				checkbox.removeAttr('checked');
				hidden.val('non');
				checkbox.val('non');
				masque.show();
			}
			// Cacher le bouton en cas de changement de valeur de la case à cocher
			checkbox.change(function() {
				if (checkbox.is(':checked')) {
					hidden.val('oui');
					masque.hide();
					checkbox.val('oui');
				} else {
					checkbox.removeAttr('checked');
					hidden.val('non');
					masque.show();
					checkbox.val('non');
				}
			}); 
				});
		</script>
		<?php  
	}
		
	// Personnaliser le texte de la popup de Tarteaucitron
	function custom_tarteaucitron_popup_text( $texts ) {
		$texts['alertBig'] = 'Votre message personnalisé';
		return $texts;
	}
	add_filter( 'tarteaucitron_banner_text', 'custom_tarteaucitron_popup_text' );
		
	function page_de_configuration(){
		//pre($_POST);
		if(isset($_POST['plugin_dsfr_installe'])) {
			update_option('plugin_dsfr_installe', sanitize_text_field($_POST['plugin_dsfr_installe']));			
			update_option('dsfr_nom_de_domaine_en_production', sanitize_text_field(stripslashes($_POST['dsfr_nom_de_domaine_en_production'])));
			update_option('dsfr_titre_onglet_navigateur', sanitize_text_field(stripslashes($_POST['dsfr_titre_onglet_navigateur'])));
			update_option('dsfr_masquer_commentaires', sanitize_text_field($_POST['_masquer_commentaires']));
			update_option('dsfr_liens_externe_que_faire', sanitize_text_field($_POST['liens_externe_que_faire']));
			update_option('dsfr_elements_recherche', sanitize_text_field($_POST['dsfr_elements_recherche']));
			update_option('dsfr_home_en_h1', sanitize_text_field($_POST['_home_en_h1']));
			$dsfr_meta_description = stripslashes( $_POST['dsfr_meta_description'] );
			update_option('dsfr_meta_description', sanitize_text_field($dsfr_meta_description));
			update_option('dsfr_addresse_email_responsable', sanitize_email(stripslashes($_POST['dsfr_addresse_email_responsable'])));
			
			//$dsfr_texte_marianne = stripslashes( $_POST['dsfr_texte_marianne'] );
			$allowed_tags = array(
				'br' => array(),
			);
			update_option( 'dsfr_texte_marianne', wp_kses( stripslashes($_POST['dsfr_texte_marianne']), $allowed_tags ) );
			
			// masquer le titre du site
			update_option('dsfr_masquer_le_titre', sanitize_text_field($_POST['_masquer_le_titre']));
			if ($_POST['_masquer_le_titre'] === 'non'){
				$titre_du_site = stripslashes( $_POST['titre_du_site'] );
				update_option('dsfr_titre_du_site', sanitize_text_field($titre_du_site));
				update_option('blogname', sanitize_text_field($titre_du_site));
			} 
			// Masquer slogan
			if (isset($_POST['_masquer_le_slogan'])){
				update_option('dsfr_masquer_le_slogan', sanitize_text_field($_POST['_masquer_le_slogan']));
				if ($_POST['_masquer_le_slogan'] === 'non'){
					$slogan_du_site = stripslashes( $_POST['slogan_du_site'] );
					update_option('dsfr_slogan_du_site', sanitize_text_field($slogan_du_site));
					update_option('blogdescription', sanitize_text_field($slogan_du_site));
				} 
			}
			// Masquer la page contact
			update_option('dsfr_masquer_page_contact', sanitize_text_field($_POST['_masquer_page_contact']));

			if ($_POST['_masquer_page_contact'] === 'non'){
				update_option('dsfr_form_contact', sanitize_text_field(stripslashes($_POST['dsfr_form_contact'])));
				update_option('dsfr_emplacement_contact', sanitize_text_field($_POST['dsfr_emplacement_contact']));
				update_option('dsfr_titre_url_contact', sanitize_text_field($_POST['dsfr_titre_url_contact']));
			} 
			
			// Masquer logo secondaire
			update_option('dsfr_masquer_logo_secondaire', sanitize_text_field($_POST['_masquer_logo_secondaire']));
			if (get_option('dsfr_masquer_logo_secondaire') === 'non'){
				update_option('dsfr_afficher_selection_type_logo', sanitize_text_field($_POST['dsfr_afficher_selection_type_logo']));
				update_option('dsfr_alternative_logo_secondaire', sanitize_text_field($_POST['alternative_logo_secondaire']));
				if (get_option('dsfr_afficher_selection_type_logo') === 'fichier'){
					update_option('dsfr_image_second_logo', sanitize_text_field($_POST['image_second_logo']));
					update_option('dsfr_image_second_logo2', sanitize_text_field($_POST['image_second_logo2']));
				}
				if (get_option('dsfr_afficher_selection_type_logo') === 'code'){
					update_option('dsfr_code_second_logo', sanitize_textarea_field($_POST['dsfr_code_second_logo']));
				}
			}
			// Drapeau en berne 
			if (isset($_POST['drapeau_en_berne'])){  
				update_option('dsfr_drapeau_en_berne', sanitize_text_field($_POST['drapeau_en_berne']));
			} else {
				update_option('dsfr_drapeau_en_berne', 'non');
			}
		
			// Masquer le partage des réseaux sociaux
			update_option('dsfr_masquer_partage_reseaux_sociaux', sanitize_text_field($_POST['_masquer_partage_reseaux_sociaux']));
			if (get_option('dsfr_masquer_partage_reseaux_sociaux') === 'non'){   
				$titre_partage_reseaux_sociaux  = stripslashes( $_POST['titre_partage_reseaux_sociaux'] );
				update_option('dsfr_titre_partage_reseaux_sociaux', sanitize_text_field($titre_partage_reseaux_sociaux));
				$url_facebook = stripslashes( $_POST['url_facebook'] );
				update_option('dsfr_url_facebook', sanitize_url($url_facebook));
				$url_twitter = stripslashes( $_POST['url_twitter'] );
				update_option('dsfr_url_twitter', sanitize_url($url_twitter));
				$url_instagram = stripslashes( $_POST['url_instagram'] );
				update_option('dsfr_url_instagram', sanitize_url($url_instagram));
				$url_linkedin = stripslashes( $_POST['url_linkedin'] );
				update_option('dsfr_url_linkedin', sanitize_url($url_linkedin));
				$url_youtube = stripslashes( $_POST['url_youtube'] );
				update_option('dsfr_url_youtube', sanitize_url($_POST['url_youtube']));
				$url_rss = stripslashes( $_POST['url_rss'] );
				update_option('dsfr_url_rss', sanitize_text_field($url_rss));
			} 
			// Masquer la newsletter
			update_option('dsfr_masquer_la_newsletter', sanitize_text_field($_POST['_masquer_la_newsletter']));

			if (get_option('dsfr_masquer_la_newsletter') === 'non'){    
				update_option('dsfr_titre_form_newsletter', sanitize_text_field(stripslashes($_POST['titre_form_newsletter'])));
				update_option('dsfr_slogan_form_newsletter', sanitize_text_field(stripslashes($_POST['slogan_form_newsletter'])));
				update_option('dsfr_type_formulaire_newsletter', sanitize_text_field($_POST['type_formulaire']));
				update_option('dsfr_url_formulaire_newsletter', sanitize_url($_POST['url_formulaire']));		


				$titre_bouton_sabonner = stripslashes( $_POST['titre_bouton_sabonner'] );
				update_option('dsfr_titre_bouton_sabonner', sanitize_text_field($titre_bouton_sabonner));

				if (get_option('dsfr_type_formulaire_newsletter') === 'formulaire') {
					update_option('dsfr_nom_champ_input_form', sanitize_text_field($_POST['nom_champ_input_form']));
				} 
				//update_option('titre_bouton_submit', sanitize_text_field($_POST['titre_bouton_submit']));
				$description_newsletter = stripslashes( $_POST['description_newsletter'] );
				update_option('dsfr_description_newsletter', sanitize_textarea_field($description_newsletter));
				//update_option('url_form_newsletter', sanitize_url($_POST['url_form_newsletter']));
			} 
			// Masquer la recherche
			update_option('dsfr_masquer_recherche', sanitize_text_field($_POST['_masquer_recherche']));
			// Masquer la liste d'accès rapide
			update_option('dsfr_masquer_liste_acces_rapide', sanitize_text_field($_POST['_masquer_liste_acces_rapide']));
			if (get_option('dsfr_masquer_liste_acces_rapide') === 'non'){
				//update_option('type_de_liste_ar', sanitize_text_field($_POST['type_de_liste_a_r_value']));
				//update_option('type_de_liste_a_r_value', sanitize_text_field($_POST['type_de_liste_a_r_value']));
				update_option('dsfr_lien_dacces_rapide1', sanitize_text_field(stripslashes($_POST['lien_dacces_rapide1'])));
				update_option('dsfr_lien_dacces_rapide2', sanitize_text_field(stripslashes($_POST['lien_dacces_rapide2'])));
				if ($_POST['_masquer_parametres_affichage'] === 'oui'){        
					update_option('dsfr_lien_dacces_rapide3', sanitize_text_field(stripslashes($_POST['lien_dacces_rapide3'])));
				}
			} 
			// Masquer les paramètres d'affichage
			update_option('dsfr_masquer_parametres_affichage', sanitize_text_field($_POST['_masquer_parametres_affichage']));
			if (get_option('dsfr_masquer_parametres_affichage') === 'non'){
				update_option('dsfr_parametre_affichage_defaut', sanitize_text_field($_POST['parametre_affichage_defaut']));
				update_option('dsfr_pad_dans_footer', sanitize_text_field($_POST['pad_dans_footer']));	
			} 
			// Masquer les cookies
			update_option('dsfr_masquer_cookies', sanitize_text_field($_POST['_masquer_cookies']));
			if (get_option('dsfr_masquer_cookies') === 'non'){			
				$liste_des_cookies = stripslashes( $_POST['liste_des_cookies'] );
				update_option('dsfr_liste_des_cookies', sanitize_textarea_field($liste_des_cookies));
			}
			// Masquer Haut de page
			update_option('dsfr_masquer_haut_de_page', sanitize_text_field($_POST['_masquer_haut_de_page']));
		
			// Masquer le menu
			update_option('dsfr_masquer_menu', sanitize_text_field($_POST['_masquer_menu']));
			if ($_POST['_masquer_menu'] === 'non'){      
				//update_option('type_de_menu', sanitize_text_field($_POST['type_de_menu']));   					
				update_option('dsfr_nombre_de_menu', sanitize_text_field($_POST['nombre_de_menu']));					
				for ($i=1; $i <= $_POST['nombre_de_menu']; $i++){
					$menu = 'menu_'.$i;
					$bdd_menu = 'dsfr_'.$menu;
					$menu_n = stripslashes( $_POST[$menu] );
	
					$allowed_tags = array(
						'a' => array(
							'href' => array(),
							'target' => array()
						)
					);
					$clean_menu = wp_kses( $menu_n, $allowed_tags );
					// enregistrer le message nettoyé dans les options WordPress
					update_option( $bdd_menu, $clean_menu );
				}
			}		  
			update_option('dsfr_masquer_liste_de_lien', sanitize_text_field($_POST['_masquer_liste_de_lien']));
			if ($_POST['_masquer_liste_de_lien'] === 'non')
			{
				//update_option('type_de_liste_de_liens', sanitize_text_field($_POST['type_de_liste_de_liens']));
				update_option('dsfr_nombre_de_liste_de_liens', sanitize_text_field($_POST['nombre_de_liste_de_liens']));
				for ($i=1; $i <= $_POST['nombre_de_liste_de_liens']; $i++){
					$liste_de_lien = 'liste_de_liens_'.$i;
					$bdd_liste_de_liens = 'dsfr_'.$liste_de_lien;
					//update_option($menu, sanitize_text_field($_POST[$liste_de_lien]));
					$lien_n = stripslashes( $_POST[$liste_de_lien] );
					$allowed_tags = array(
						'a' => array(
							'href' => array(),
							'target' => array()
						)
					);
					$clean_lien = wp_kses( $lien_n, $allowed_tags );
					// enregistrer le message nettoyé dans les options WordPress
					update_option( $bdd_liste_de_liens, $clean_lien );
				}			  					
			}
			
			update_option('dsfr_masquer_partenaires', sanitize_text_field($_POST['_masquer_partenaires']));
			if($_POST['_masquer_partenaires'] === 'non'){
				update_option('dsfr_nombre_de_partenaires_principaux', sanitize_text_field($_POST['nombre_de_partenaires_principaux']));
				update_option('dsfr_nombre_de_partenaires_secondaires', sanitize_text_field($_POST['nombre_de_partenaires_secondaires']));
				update_option('dsfr_titre_bloc_partenaires', sanitize_text_field($_POST['dsfr_titre_bloc_partenaires']));
				if ($_POST['_masquer_partenaires'] === 'non'){
					for ($i=1; $i <= intval($_POST['nombre_de_partenaires_principaux']); $i++){					
						$pp = 'partenaires_principaux_'.$i;
						$bdd_pp = 'dsfr_'.$pp;
						//$ppartenaire = stripslashes( $_POST[$partenaire] );				  
						update_option($bdd_pp, sanitize_text_field(stripslashes($_POST[$pp])));
					}				
					for ($i=1; $i <= intval($_POST['nombre_de_partenaires_secondaires']); $i++){
						$ps = 'partenaires_secondaires_'.$i;
						$bdd_ps = 'dsfr_'.$ps;
						//$spartenaire = stripslashes( $_POST[$menu] );
						update_option($bdd_ps, sanitize_text_field(stripslashes($_POST[$ps])));
					}
				}
			}
			update_option('dsfr_masquer_liens_ecosysteme', sanitize_textarea_field($_POST['_masquer_liens_ecosysteme']));
			if (get_option('dsfr_masquer_liens_ecosysteme') === 'non'){
				if (isset($_POST['nombre_de_liens_ecosysteme'])) { 
					update_option('dsfr_nombre_de_liens_ecosysteme', sanitize_textarea_field($_POST['nombre_de_liens_ecosysteme'])); 
				} else { 
					update_option('dsfr_nombre_de_liens_ecosysteme', 4);
				}
				for ($i=1; $i <= get_option('dsfr_nombre_de_liens_ecosysteme'); $i++){
					$lien = 'lien_eco_'.$i;
					$bdd_lien = 'dsfr_'.$lien;
					update_option($bdd_lien, sanitize_text_field(stripslashes($_POST[$lien])));
				}
			}
			update_option('dsfr_masquer_liens_legaux', sanitize_textarea_field($_POST['_masquer_liens_legaux']));
			if (get_option('dsfr_masquer_liens_legaux') === 'non') {				
				update_option('dsfr_nombre_de_liens_obligations_legales', sanitize_textarea_field($_POST['nombre_de_liens_obligations_legales']));
				for ($i=1; $i <= $_POST['nombre_de_liens_obligations_legales']; $i++){
					$lien = 'lien_legal_'.$i;
					$bdd_lien = 'dsfr_'.$lien;
					update_option($bdd_lien, sanitize_text_field(stripslashes($_POST[$lien])));
				}
			}

			update_option('dsfr_masquer_licence', sanitize_text_field(stripslashes($_POST['_masquer_licence'])));
			if (get_option('dsfr_masquer_licence') === 'non'){
				$dsfr_licence = stripslashes( $_POST['dsfr_licence'] );		
				// définir les balises HTML autorisées
				$allowed_tags_licence = array(
					'a' => array(
							'href' => array(),
							'target' => array(),
							'rel' => array(),
							'class' => array(),
							'title' => array(),
							'id' => array()
					)
				);
				$clean_dsfr_licence = wp_kses( $dsfr_licence, $allowed_tags_licence );
				// enregistrer le message nettoyé dans les options WordPress
				update_option( 'dsfr_licence', $clean_dsfr_licence );
			}
			update_option('dsfr_masquer_description_footer', sanitize_text_field(stripslashes($_POST['_masquer_description_footer'])));
			if(get_option('dsfr_masquer_description_footer') === 'non'){
				update_option('dsfr_description_footer', sanitize_textarea_field(stripslashes($_POST['dsfr_description_footer'])));
			}
	
			update_option('dsfr_masquer_statistiques', sanitize_text_field($_POST['_masquer_statistiques']));
			if ((get_option('dsfr_masquer_statistiques') !== false) AND (get_option('dsfr_masquer_statistiques') === 'non')){
				$message = stripslashes( $_POST['script_statistiques'] );
				$allowed_tags = array(
					'a' => array(
						'href' => array(),
						'target' => array()
					),
					'script' => array(
						'type' => array(),
						'src' => array()
					),
					'noscript' => array(),
					'p' => array(),
					'img' => array(
						'src' => array(),
						'alt' => array(),
						'style' => array()
					)

				);
				$clean_message = wp_kses( $message, $allowed_tags );
				// enregistrer le message nettoyé dans les options WordPress
				update_option( 'dsfr_script_statistiques', $clean_message );
		
				//update_option('script_statistiques', sanitize_textarea_field($_POST['script_statistiques']));
			}
			update_option('dsfr_masquer_message_alerte', sanitize_text_field($_POST['_masquer_message_alerte']));
			if ((get_option('dsfr_masquer_message_alerte') !== false) AND (get_option('dsfr_masquer_message_alerte') === 'non')){
				// définir les balises HTML autorisées
				
				$message = stripslashes( $_POST['message_alerte'] );
				$allowed_tags = array(
					'a' => array(
						'href' => array(),
						'target' => array()
					)
				);
				$clean_message = wp_kses( $message, $allowed_tags );
				// enregistrer le message nettoyé dans les options WordPress
				update_option( 'dsfr_message_alerte', $clean_message );
			}
			if (get_option('dsfr_masquer_cookies') === 'non'){
				update_option('dsfr_afficher_bouton_cookies', sanitize_textarea_field(stripslashes($_POST['afficher_bouton_cookies'])));
				update_option('dsfr_modifier_texte_cookies', sanitize_text_field(stripslashes($_POST['modifier_texte_cookies'])));
				
				if($_POST['texte_cookies']){
					update_option('dsfr_titre_texte_cookies', sanitize_text_field(stripslashes($_POST['titre_texte_cookies'])));
					// définir les balises HTML autorisées
					$texte_cookies = stripslashes( $_POST['texte_cookies'] );

			
					$allowed_tags_licence = array(
						'a' => array(
							'href' => array(),
							'target' => array(),
							'rel' => array(),
							'class' => array(),
							'title' => array(),
							'id' => array()
						)
					);
					$clean_texte_cookies = wp_kses( $texte_cookies, $allowed_tags_licence );
			
					// enregistrer le message nettoyé dans les options WordPress
					update_option( 'dsfr_texte_cookies', $clean_texte_cookies );
				}
			}
		}
	?>
	<div id="modal" class="modal dontshow">
		<div class="modal-content">
			<span class="close">&times;</span> 
			<form method="post" id="menu_generator">
				<ul class="form_configuration">
					<li>
						<h2><strong>Générateur d'éléments</strong></h2>
					</li>
					<li><input type="submit" name="submit_generateurs_menu" class="button button-primary" value="Visualiser le résultat"/></li>
				</ul>
				<ul class="elementFormUl">
					<li class="erreur dontshow">Erreur : Tous les champs avec des astérisques (couleur rouge) doivent être remplis.</li>
				</ul>

			</form>
		</div>		
	</div>
	<div class="bo_content">
		<h1>DSFR Formulaire de configuration</h1>
		<?php if (isset($_POST['plugin_dsfr_installe'])){
			echo '
				<div class="message_validation">
					<div class="elementor-widget-container">
						<blockquote class="avertissement_ok encadre">
							<strong>Les nouvelles données ont bien été enregistrées</strong>.<br>Elles seront prises en compte immédiatement sur les pages publiques<br>Si ce n\'est pas le cas, il faut penser à vider le cache serveur.</blockquote>							
					</div>
				</div>';
		}?>
		<div class="mise-en-page-lien">
			<div class="bloc-mise-en-page bloc-lien">
				<h2>Légende</h2>
				<ul class="form_configuration">
					<li>	
						<ul class="form_configuration">
							<li><span class="bold">Oui</span> <?php va_et_vient('oui', 'oui' );?></li>   
							<li><?php va_et_vient('non', 'non' );?><span class="bold">Non</span></li>   
						</ul>
					</li>
					<li>
						<ul style="display:inline-flex;">
							<li><img src="<?php echo get_template_directory_uri().'/assets/images/ctrl.jpg'; ?>" height="37"/></li>
							<li><img src="<?php echo get_template_directory_uri().'/assets/images/plus.jpg'; ?>"  height="37"/></li>
							<li><img src="<?php echo get_template_directory_uri().'/assets/images/enter.jpg'; ?>"  height="37"/></li>
							<li><img src="<?php echo get_template_directory_uri().'/assets/images/egal.jpg'; ?>"  height="37"/></li>
							<li style="padding-top:0.5rem;"><span class="bold">Soumettre immédiatement le formulaire</span></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<br>
		<form method="post" action="" enctype="multipart/form-data" id="configurateur_dsfr_wordpress">
			<input type="hidden" name="plugin_dsfr_installe" value="oui" />
			<input type="hidden" name="plugin_root" value="<?php echo get_template_directory_uri(); ?>" />
			<hr class="hr" />
			<h2>Informations générales Header</h2>
			<ul class="form_configuration">
				<li><label for="dsfr_nom_de_domaine_en_production">Nom du domaine en production :</label></li>
				<li class="flex ">
					<div class="width100 grp_dsfr_nom_de_domaine_en_production"><input type="text" name="dsfr_nom_de_domaine_en_production" id="dsfr_nom_de_domaine_en_production" value="<?php echo esc_attr(get_option('dsfr_nom_de_domaine_en_production')); ?>" class="aide_dsfr_nom_de_domaine_en_production"/></div>
					<button class="liste_de_lien questionmark_header" type="button" title="Afficher la fenêtre d'aide" data-value="<?php echo esc_attr(get_option('dsfr_nom_de_domaine_en_production')); ?>"  data-parent="grp_dsfr_nom_de_domaine_en_production" data-fille="aide_dsfr_nom_de_domaine_en_production"><span class="bold colorblack">?</span></button>
				</li>
				<li><label for="dsfr_titre_onglet_navigateur">Titre apparaissant dans l'onglet du navigateur :</label></li>
				<li class="flex">
					<div class="width100 grp_dsfr_titre_onglet_navigateur"><input type="text" name="dsfr_titre_onglet_navigateur" id="dsfr_titre_onglet_navigateur" value="<?php echo esc_attr(get_option('dsfr_titre_onglet_navigateur')); ?>" class="aide_dsfr_titre_onglet_navigateur"/></div>
					<button class="liste_de_lien questionmark_header" type="button" title="Afficher la fenêtre d'aide"  data-value="<?php echo esc_attr(get_option('dsfr_titre_onglet_navigateur')); ?>" data-parent="grp_dsfr_titre_onglet_navigateur" data-fille="aide_dsfr_titre_onglet_navigateur"><span class="bold colorblack">?</span></button>
				</li>
				<li><label for="dsfr_meta_description">Méta description:</label></li>
				<li class="flex">
					<div class="width100 grp_dsfr_meta_description"><textarea name="dsfr_meta_description" id="dsfr_meta_description" rows="6" cols="33"  class="aide_dsfr_meta_description"><?php echo esc_attr(get_option('dsfr_meta_description')); ?></textarea></div>
					<button class="liste_de_lien questionmark_header" type="button" title="Afficher la fenêtre d'aide" data-value="<?php echo esc_attr(get_option('dsfr_meta_description')); ?>" data-parent="grp_dsfr_meta_description" data-fille="aide_dsfr_meta_description"><span class="bold colorblack">?</span></button>
				</li>
				<li><label for="dsfr_addresse_email_responsable">Adresse email du responsable:</label></li>
				<li class="flex">
					<div class="width100 grp_dsfr_addresse_email_responsable"><input type="text" name="dsfr_addresse_email_responsable" id="dsfr_addresse_email_responsable" value="<?php echo esc_attr(get_option('dsfr_addresse_email_responsable')); ?>" class="aide_dsfr_addresse_email_responsable"/></div>
					<button class="liste_de_lien questionmark_header" type="button" title="Afficher la fenêtre d'aide" data-value="<?php echo esc_attr(get_option('dsfr_addresse_email_responsable')); ?>" data-parent="grp_dsfr_addresse_email_responsable" data-fille="aide_dsfr_addresse_email_responsable"><span class="bold colorblack">?</span></button>
				</li>
				<li><label for="dsfr_texte_marianne">Texte de la Marianne:</label></li>
				<li class="flex">	
					<?php $marianne_value = str_replace( "\r\n", '<br>', get_option( 'dsfr_texte_marianne' )); ?>
					<div class="width100 grp_dsfr_texte_marianne"><textarea class="width100 aide_dsfr_texte_marianne" name="dsfr_texte_marianne" id="dsfr_texte_marianne" cols="33" rows="8"><?php echo wp_kses_post( $marianne_value ); ?></textarea></div>
					<button class="liste_de_lien questionmark_header" type="button" title="Afficher la fenêtre d'aide" data-value="<?php echo wp_kses_post( $marianne_value ); ?>" data-parent="grp_dsfr_texte_marianne" data-fille="aide_dsfr_texte_marianne"><span class="bold colorblack">?</span></button>
				</li>
				<li><label for="dsfr_texte_marianne">Concernant les liens externes de ce site, souhaitez-vous laisser l'utilisateur agir ou ouvrir automatiquement une nouvelle fenêtre:<br>- Automatique -> target="_blank"<br>- Utilisateur -> choix utilisateur</label></li>
				<li class="flex"> 						
					<div class="width100 grp_liens_externe_que_faire">
						<?php $options = array('utilisateur', 'automatique');?>
						<select name="liens_externe_que_faire" id="liens_externe_que_faire" class="maxheight35 aide_liens_externe_que_faire">							
							<?php foreach($options as $option): ?>
								<option value="<?php echo $option; ?>" <?php selected(get_option('dsfr_liens_externe_que_faire'), $option); ?>><?php echo $option; ?></option>
							<?php endforeach; ?>
						</select></div>
						<button class="liste_de_lien questionmark_header maxheight35" type="button" title="Afficher la fenêtre d'aide" data-value="<?php echo get_option('dsfr_liens_externe_que_faire'); ?>" data-parent="grp_liens_externe_que_faire" data-fille="aide_liens_externe_que_faire"><span class="bold colorblack">?</span></button>
				</li>
				<li><label for="dsfr_elements_recherche">Nombre de résultats dans la page Recherche :</label></li>
				<li class="flex">
					<div class="width100 grp_dsfr_elements_recherche"><input type="text" name="dsfr_elements_recherche" id="dsfr_elements_recherche" value="<?php echo esc_attr(get_option('dsfr_elements_recherche')); ?>" placeholder="Nombre de resultats par page de recherche" class="width50  aide_dsfr_elements_recherche" /></div>
					<button class="liste_de_lien questionmark_header" type="button" title="Afficher la fenêtre d'aide" data-value="<?php echo esc_attr(get_option('dsfr_elements_recherche')); ?>" data-parent="grp_dsfr_elements_recherche" data-fille="aide_dsfr_elements_recherche"><span class="bold colorblack">?</span></button>
				</li>

			</ul>
			<hr class="hr" />
			<h2>Commandes</h2>
			<ul class="form_commandes">
				<!-- Masquer le titre? -->
				<li class="encadre aide_masquer_le_titre">
					<ul><li><button class="questionmark" type="button" data-parent="masquer_le_titre">?</button></li></ul>
					<ul class="gestion_masquer_le_titre masquer_le_titre_primaire">
						<li class="center bold"><label for="masquer_le_titre">Masquer le titre?</label></li>
						<li class="center"><?php va_et_vient('masquer_le_titre', 'non', get_option('dsfr_masquer_le_titre') );?></li>
						<?php if (get_option('dsfr_masquer_le_titre') === 'non') { ?>
						<li class="_masquer_le_titre">
							<label for="titre_du_site">Titre du site :</label>
							<input class="width100" type="text" name="titre_du_site" id="titre_du_site" value="<?php echo esc_attr(get_option('dsfr_titre_du_site')); ?>" data-parent="masquer_le_titre" required="required"/>
						</li>
						<?php } ?>
					</ul>
				</li>
				<!-- Masquer le slogan? -->
				<li class="encadre aide_masquer_le_slogan">
					<ul><li><button class="questionmark" type="button" data-parent="masquer_le_slogan">?</button></li></ul>
					<ul class="gestion_masquer_le_slogan masquer_le_slogan_primaire">
						<li class="center bold"><label for="masquer_le_slogan">Masquer le slogan?</label></li>
						<li class="center"><?php va_et_vient('masquer_le_slogan', 'non', get_option('dsfr_masquer_le_slogan') ); ?></li>
						<?php if (get_option('dsfr_masquer_le_slogan') === 'non') { ?>
						<li class="_masquer_le_slogan">
							<label for="slogan_du_site">Slogan du site :</label>
							<input class="width100" type="text" name="slogan_du_site" id="slogan_du_site" value="<?php echo esc_attr(get_option('dsfr_slogan_du_site')); ?>" data-parent="slogan_du_site" required="required"/>
						</li>
						<?php } ?>
					</ul>
				</li>
				<!-- Masquer la page contact? -->
				<li class="encadre aide_masquer_page_contact">
					<ul><li><button class="questionmark" type="button" data-parent="masquer_page_contact">?</button></li></ul>
					<ul class="gestion_masquer_page_contact masquer_page_contact_primaire">
						<li class="center bold"><label for="masquer_page_contact">Masquer la page contact?</label></li>
						<li class="center"><?php va_et_vient('masquer_page_contact', 'non', get_option('dsfr_masquer_page_contact') );?></li>
						<?php if (get_option('dsfr_masquer_page_contact') === 'non') { ?>
							<li class="_masquer_page_contact">
							<label for="dsfr_form_contact">Url du formulaire de contact:</label>
							<input type="text" name="dsfr_form_contact" id="dsfr_form_contact" value="<?php echo esc_attr(get_option('dsfr_form_contact')); ?>" placeholder="/contact" class="aide_dsfr_form_contact width100"/>
						</li>
						<li class="_masquer_page_contact">
							<label for="dsfr_titre_url_contact">Titre url de contact:</label>
							<input type="text" name="dsfr_titre_url_contact" id="dsfr_titre_url_contact" value="<?php echo esc_attr(get_option('dsfr_titre_url_contact')); ?>" placeholder="Titre de l'url de contact" class="aide_dsfr_titre_url_contact width100" required="required"/>
						</li>
						<li class="_masquer_page_contact">
							<label for="dsfr_emplacement_contact">Emplacement du lien contact</label>
							<select name="dsfr_emplacement_contact" id="dsfr_emplacement_contact width100" required="required">
								<option value="1"<?php if (get_option('dsfr_emplacement_contact') === '1' ){echo ' selected="selected"';}?>>Liens rapides(Header) + Menu Header + Obligations légales(Footer)</option>
								<option value="2"<?php if (get_option('dsfr_emplacement_contact') === '2' ){echo ' selected="selected"';}?>>Liens rapides(Header) + Menu Header</option>
								<option value="3"<?php if (get_option('dsfr_emplacement_contact') === '3' ){echo ' selected="selected"';}?>>Menu Header + Obligations légales(Footer)</option>
								<option value="4"<?php if (get_option('dsfr_emplacement_contact') === '4' ){echo ' selected="selected"';}?>>Liens rapides(Header) + Obligations légales(Footer)</option>
								<option value="5"<?php if (get_option('dsfr_emplacement_contact') === '5' ){echo ' selected="selected"';}?>>Liens rapides(Header)</option>
								<option value="6"<?php if (get_option('dsfr_emplacement_contact') === '6' ){echo ' selected="selected"';}?>>Menu Header</option>
								<option value="7"<?php if (get_option('dsfr_emplacement_contact') === '7' ){echo ' selected="selected"';}?>>Obligations légales(Footer)</option>
							</select>
						</li>
						<?php } ?>


					</ul>
				</li>
				<li class="encadre aide_drapeau_en_berne">
					<ul><li><button class="questionmark" type="button" data-parent="drapeau_en_berne">?</button></li></ul>
					<ul class="drapeau_en_berne_primaire">
						<li class="center bold"><label for="drapeau_en_berne">Mettre le drapeau en berne?</label></li>
						<li class="center"><?php va_et_vient('drapeau_en_berne', 'non', get_option('dsfr_drapeau_en_berne') ); ?></li>
					</ul>
				</li>
				<li class="encadre aide_masquer_commentaires">
				<ul><li><button class="questionmark" type="button" data-parent="masquer_commentaires">?</button></li></ul>
					<ul class="masquer_commentaires_primaire">
						<li class="center bold"><label for="masquer_commentaires">Masquer les commentaires des articles?</label></li>
						<li class="center"><?php va_et_vient('masquer_commentaires', 'oui', get_option('dsfr_masquer_commentaires') ); ?></li>
					</ul>
				</li>
				<li class="encadre aide_home_en_h1">
					<ul><li><button class="questionmark" type="button" data-parent="home_en_h1">?</button></li></ul>
					<ul class="home_en_h1_primaire">
						<li class="center bold"><label for="home_en_h1">Mettre le titre de la page d'accueil en H1?</label></li>
						<li class="center"><?php va_et_vient('home_en_h1', 'non', get_option('dsfr_home_en_h1') ); ?></li>
					</ul>
				</li>
				<li class="encadre aide_masquer_recherche">
					<ul><li><button class="questionmark" type="button" data-parent="masquer_recherche">?</button></li></ul>
					<ul class="masquer_recherche_primaire">
						<li class="center bold"><label for="masquer_recherche">Masquer la recherche?</label></li>
						<li class="center"><?php va_et_vient('masquer_recherche', 'oui', get_option('dsfr_masquer_recherche') ); ?></li>
					</ul>
				</li>
				<li class="encadre aide_masquer_logo_secondaire">
					<ul><li><button class="questionmark" type="button" data-parent="masquer_logo_secondaire">?</button></li></ul>
					<ul class="gestion_masquer_logo_secondaire masquer_logo_secondaire_primaire">
						<li class="center bold"><label for="masquer_logo_secondaire">Masquer logo secondaire?</label></li>
						<li class="center"><?php va_et_vient('masquer_logo_secondaire', 'oui', get_option('dsfr_masquer_logo_secondaire') ); ?></li>
						<?php if(get_option('dsfr_masquer_logo_secondaire') === 'non') { ?>
						<li class="_masquer_logo_secondaire">
							<label for="dsfr_afficher_selection_type_logo">Type image</label><br>							
							<?php $options = array('fichier', 'code'); ?>
							<select name="dsfr_afficher_selection_type_logo" id="dsfr_afficher_selection_type_logo">
								<?php foreach($options as $option): ?>
								<option value="<?php echo $option; ?>" <?php selected(get_option('dsfr_afficher_selection_type_logo'), $option); ?>><?php echo $option; ?></option>
								<?php endforeach; ?>
							</select>
						</li>				
						<li class="_masquer_logo_secondaire">
							<ul>
								<?php if(get_option('dsfr_afficher_selection_type_logo') === 'code') { ?>
								<li class="image_second_logo suppression-code">
									<label for="code_second_logo">Logo secondaire</label><br>
									<textarea class="width100" id="dsfr_code_second_logo" data-idimage="image_second_logo" data-urlimage="image_second_logo2" data-imagepreview="selected-image-preview" name="dsfr_code_second_logo" data-imagetype="<?php echo get_option('dsfr_afficher_selection_type_logo');?>" placeholder="Insérer le code de votre image (svg)" required="required"><?php echo get_option('dsfr_code_second_logo');?></textarea>
									<?php echo get_option('dsfr_code_second_logo');?>
								</li>
								<?php } else {?>								
								<li class="image_second_logo suppression-image">
									<label for="select-image-button">Logo secondaire</label><br>
									<input class="loadImage" type="button" id="select-image-button" class="button" value="Sélectionner une image" class="button" value="Sélectionner une image"  data-idimage="image_second_logo" data-urlimage="image_second_logo2" data-imagepreview="selected-image-preview" name="select-image-button" data-imagetype="<?php echo get_option('dsfr_afficher_selection_type_logo');?>" required="required"/>
									<input type="hidden" id="image_second_logo" name="image_second_logo" value="<?php echo get_option('dsfr_image_second_logo');?>">
									<input type="hidden" id="image_second_logo2" name="image_second_logo2" value="<?php echo get_option('dsfr_image_second_logo2');?>"><br>
									<img id="selected-image-preview" src="<?php echo get_option('dsfr_image_second_logo2');?>" alt="<?php echo esc_attr(get_option('dsfr_alternative_logo_secondaire')); ?>">								         
								</li>
								<?php } ?>
								<li class="_masquer_logo_secondaire alternative_logo_secondaire suppression-image">
									<label for="alternative_logo_secondaire">Texte alternatif logo secondaire<span class="obligatoire"> (obligatoire)</span>:</label>
									<input class="width100" type="text" name="alternative_logo_secondaire" id="alternative_logo_secondaire" value="<?php echo esc_attr(get_option('dsfr_alternative_logo_secondaire')); ?>" required="required"/>
								</li>
								<li class="suppression-image">
									<hr class="hr width100">
									<span class="bold">Notes:</span>
								</li>
								<li class="alternative_logo_secondaire suppression-image">Vous pouvez choisir une image ou la télécharger dans la bibliothèque d'images de Wordpress.</li>
								
							</ul>
						</li>
						<?php } ?>
					</ul>
				</li>
				<li class="encadre aide_masquer_partage_reseaux_sociaux">
					<ul><li><button class="questionmark" type="button" data-parent="masquer_partage_reseaux_sociaux">?</button></li></ul>
					<ul class="gestion_masquer_partage_reseaux_sociaux masquer_partage_reseaux_sociaux_primaire">
						<li class="center bold"><label for="masquer_partage_reseaux_sociaux">Masquer les partages sur les réseaux sociaux?</label></li>
						<li class="center"><?php va_et_vient('masquer_partage_reseaux_sociaux', 'oui', get_option('dsfr_masquer_partage_reseaux_sociaux') ); ?></li>
						<?php if (get_option('dsfr_masquer_partage_reseaux_sociaux') === 'non') { ?>
						<li class="_masquer_partage_reseaux_sociaux">
							<ul>
								<li>  
									<label for="titre_partage_reseaux_sociaux">Titre du bloc partage:</label>
									<input class="width100" type="text" name="titre_partage_reseaux_sociaux" id="titre_partage_reseaux_sociaux" value="<?php echo esc_attr(get_option('dsfr_titre_partage_reseaux_sociaux')); ?>" placeholder="Ex: Nous suivre sur"/>
								</li>
								<li>  
									<label for="url_facebook">Partager sur Facebook:</label>
									<input class="width100" type="text" name="url_facebook" id="url_facebook" value="<?php echo esc_attr(get_option('dsfr_url_facebook')); ?>"  placeholder="Lien de partage sur Facebook"/>
								</li>
								<li>  
									<label for="url_twitter">Partager sur Twitter:</label>
									<input class="width100" type="text" name="url_twitter" id="url_twitter" value="<?php echo esc_attr(get_option('dsfr_url_twitter')); ?>"  placeholder="Lien de partage sur Twitter"/>
								</li>
								<li>
									<label for="url_youtube">Partager sur Youtube:</label>
									<input class="width100" type="text" name="url_youtube" id="url_youtube" value="<?php echo esc_attr(get_option('dsfr_url_youtube')); ?>" placeholder="Lien de partage sur Youtube"/>
								</li>
								<li>
									<label for="url_instagram">Partager sur Instagram:</label>
									<input class="width100" type="text" name="url_instagram" id="url_instagram" value="<?php echo esc_attr(get_option('dsfr_url_instagram')); ?>" placeholder="Lien de partage sur Instagram"/>
								</li>
								<li>
									<label for="url_linkedin">Partager sur Linkedin:</label>
									<input class="width100" type="text" name="url_linkedin" id="url_linkedin" value="<?php echo esc_attr(get_option('dsfr_url_linkedin')); ?>" placeholder="Lien de partage sur Linkedin"/>
								</li>
								<li>  
									<label for="url_rss">Flux RSS:</label>
									<input class="width100" type="text" name="url_rss" id="url_rss" value="<?php echo esc_attr(get_option('dsfr_url_rss')); ?>" placeholder="Lien du fil RSS"/>
								</li>
							</ul>
						</li>
						<?php } ?>
					</ul>
				</li>
				<li class="encadre aide_masquer_la_newsletter">
					<ul><li><button class="questionmark" type="button" data-parent="masquer_la_newsletter">?</button></li></ul>
					<ul class="gestion_masquer_la_newsletter masquer_la_newsletter_primaire">
						<li class="center bold"><label for="masquer_la_newsletter">Masquer la newsletter?</label></li>
						<li class="center"><?php va_et_vient('masquer_la_newsletter', 'oui', get_option('dsfr_masquer_la_newsletter') ); ?></li>
						<?php if (get_option('dsfr_masquer_la_newsletter') === 'non') { ?>
							<li class="_masquer_la_newsletter">
								<label for="titre_form_newsletter">Titre newsletter:</label>
								<input class="width100" type="text" name="titre_form_newsletter" id="titre_form_newsletter" value="<?php echo esc_attr(get_option('dsfr_titre_form_newsletter')); ?>" />
							</li>
							<li class="_masquer_la_newsletter">
								<label for="slogan_form_newsletter">Slogan Newsletter:</label>
								<input class="width100" type="text" name="slogan_form_newsletter" id="slogan_form_newsletter" value="<?php echo esc_attr(get_option('dsfr_slogan_form_newsletter')); ?>" />
							</li>
							<li class="_masquer_la_newsletter">
								<label for="description_newsletter">Description Newsletter:</label>
								<textarea class="width100" name="description_newsletter" id="description_newsletter" cols="33" rows="8"><?php echo esc_attr(get_option('dsfr_description_newsletter')); ?></textarea>
							</li>
							<li class="_masquer_la_newsletter">
								<label for="type_formulaire">Type de formulaire</label><br>
								<?php $options = array('formulaire', 'lien'); ?>
								<select name="type_formulaire" id="type_formulaire" data-information="select2options" data-remove="lien" data-parent="masquer_la_newsletter" data-link="<?php echo get_template_directory_uri(); ?>">
									<?php foreach($options as $option): ?>
									<option value="<?php echo $option; ?>" <?php selected(get_option('dsfr_type_formulaire_newsletter'), $option); ?>><?php echo $option; ?></option>
									<?php endforeach; ?>
								</select>
							</li>
							<li class="_masquer_la_newsletter">
								<label for="url_formulaire">Url du formulaire:</label>
								<input class="width100" type="text" name="url_formulaire" id="url_formulaire" value="<?php echo esc_attr(get_option('dsfr_url_formulaire_newsletter')); ?>" />
							</li>
							<li class="_masquer_la_newsletter">
								<label for="titre_bouton_sabonner">Titre du bouton S'abonner:</label>
								<input class="width100" type="text" name="titre_bouton_sabonner" id="titre_bouton_sabonner" value="<?php echo esc_attr(get_option('dsfr_titre_bouton_sabonner')); ?>" />
							</li>
							<?php if(get_option('dsfr_type_formulaire_newsletter') === 'formulaire') { ?>							
								<li class="_masquer_la_newsletter gestion_masquer_la_newsletter_select2options">
									<label for="nom_champ_input_form">Nom du champ input: </label>
									<input class="width100" type="text" name="nom_champ_input_form" id="nom_champ_input_form" value="<?php echo esc_attr(get_option('dsfr_nom_champ_input_form')); ?>" />
								</li>
							<?php } ?>
							<li>
								<hr class="hr width100">
								<span class="bold">Notes:</span>
							</li>
							<li>
								<ul>
									<li>Ce thème ne gère pas de newsletter.</li>
									<li>Il ne met uniquement en place que le mécanisme de redirection vers votre newsletter.</li>
									<li>C'est à vous de gérer votre newsletter.</li>									
								</ul>
							</li>
						<?php } ?>
					</ul>
				</li>
				<li class="encadre aide_masquer_liste_acces_rapide">
					<ul><li><button class="questionmark" type="button" data-parent="masquer_liste_acces_rapide">?</button></li></ul>
					<ul class="gestion_masquer_liste_acces_rapide liste_acces_rapide_primaire masquer_liste_acces_rapide_primaire">
						<li class="center bold"><label for="masquer_liste_acces_rapide">Masquer la liste d'accès rapide?</label></li>
						<li class="center"><?php va_et_vient('masquer_liste_acces_rapide', 'oui', get_option('dsfr_masquer_liste_acces_rapide') ); ?></li>
						<?php if (get_option('dsfr_masquer_liste_acces_rapide') === 'non') { ?>
						<li class="_masquer_liste_acces_rapide"> 
							<ul class="liens_acces_rapide">

								<li class="lien_dacces_rapide1">								
									<ul class="l8020">
										<li class="lien_dacces_rapide1 ">  
											<label for="lien_dacces_rapide1">Lien rapide 1:</label>
											<input class="width100" type="text" name="lien_dacces_rapide1" id="lien_dacces_rapide1" value="<?php echo esc_attr(get_option('dsfr_lien_dacces_rapide1')); ?>" placeholder="Cliquez sur le bouton pour le générateur"/>
										</li>
										<li class="lien_dacces_rapide1">
											<button class="configurer_element" type="button" title="Configurer cet élément" data-parent="masquer_liste_acces_rapide"  data-fille="lien_dacces_rapide1" data-type-generateur="simple-icone"><img src="<?php echo get_template_directory_uri().'/assets/images/settings.svg'; ?>" width="12" height="12"></button>
										</li>
									</ul>
								</li>
								<li class="lien_dacces_rapide2">								
									<ul class="l8020">
										<li class="lien_dacces_rapide2 ">  
											<label for="lien_dacces_rapide2 ">Lien rapide 2:</label>
											<input class="width100" type="text" name="lien_dacces_rapide2" id="lien_dacces_rapide2" value="<?php echo esc_attr(get_option('dsfr_lien_dacces_rapide2')); ?>" placeholder="Cliquez sur le bouton pour le générateur"/>
										</li>
										<li class="lien_dacces_rapide2">
											<button class="configurer_element" type="button" title="Configurer cet élément" data-parent="masquer_liste_acces_rapide"  data-fille="lien_dacces_rapide2" data-type-generateur="simple-icone"><img src="<?php echo get_template_directory_uri().'/assets/images/settings.svg'; ?>" width="12" height="12"></button>
										</li>
									</ul>
								</li>
								<?php if (get_option('dsfr_masquer_parametres_affichage') === 'oui') { ?>
								<li class="lien_dacces_rapide3  remove_width_masquer_parametres_affichage">
									<ul class="l8020">
										<li class="lien_dacces_rapide3 remove_width_masquer_parametres_affichage"> 
											<label for="lien_dacces_rapide3">Lien rapide 3:</label>
											<input class="width100" type="text" name="lien_dacces_rapide3" id="lien_dacces_rapide3" value="<?php echo esc_attr(get_option('dsfr_lien_dacces_rapide3')); ?>" placeholder="Cliquez sur le bouton pour le générateur"/>
										</li>
										<li class="lien_dacces_rapide3 remove_width_masquer_parametres_affichage">
											<button class="configurer_element" type="button" title="Configurer cet élément" data-parent="masquer_liste_acces_rapide"  data-fille="lien_dacces_rapide3" data-type-generateur="simple-icone"><img src="<?php echo get_template_directory_uri().'/assets/images/settings.svg'; ?>" width="12" height="12"></button>
										</li>
									</ul>
								</li>
								<?php } ?>
							</ul>
						</li>					
						<?php } ?>
					</ul>
				</li>
				<li class="encadre aide_masquer_parametres_affichage">
					<ul><li><button class="questionmark" type="button" data-parent="masquer_parametres_affichage">?</button></li></ul>
					<ul class="gestion_masquer_parametres_affichage masquer_parametres_affichage_primaire">
						<li class="center bold"><label for="masquer_parametres_affichage">Masquer paramètres affichage(mode sombre)?</label></li>
						<li class="center"><?php va_et_vient('masquer_parametres_affichage', 'non', get_option('dsfr_masquer_parametres_affichage') ); ?></li>
						<?php if (get_option('dsfr_masquer_parametres_affichage') === 'non') { ?>
						<li class="_masquer_parametres_affichage">
							<ul>
								<li>
									<label for="parametre_affichage_defaut">Paramètres d'affichage par défaut:</label><br>
									<?php $options = array('dark', 'light'); ?>
									<select name="parametre_affichage_defaut" id="parametre_affichage_defaut">
										<?php foreach($options as $option): ?>
										<option value="<?php echo $option; ?>" <?php selected(get_option('dsfr_parametre_affichage_defaut'), $option); ?>><?php echo $option; ?></option>
										<?php endforeach; ?>
									</select>
								</li>
								<li>
									<label for="pad_dans_footer">Bouton "Paramètres d'affichage" dans le footer ?</label><br>
									<?php $options = array('oui', 'non');?>
									<select name="pad_dans_footer" id="pad_dans_footer">
										<?php foreach($options as $option): ?>
										<option value="<?php echo $option; ?>" <?php selected(get_option('dsfr_pad_dans_footer'), $option); ?>><?php echo $option; ?></option>
										<?php endforeach; ?>
									</select>
								</li>
								<li>
									<hr class="hr width100">
									<span class="bold">Notes:</span>
								</li>
								<li>
									<ul>
										<li>Les paramètres d'affichage prennent la place du lien d'accès rapide 3</li>
										<li>Les Paramètres d'affichage sont dépendants des liens d'accès -> Liens d'accès automatiquement activés si vous activez paramètres d'affichage</li>
									</ul>
								</li>
							</ul>
						</li>
						<?php } ?>
					</ul>
				</li>
				<li class="encadre aide_masquer_cookies">
					<ul><li><button class="questionmark" type="button" data-parent="masquer_cookies">?</button></li></ul>
					<ul class="gestion_masquer_cookies masquer_cookies_primaire">
						<li class="center bold"><label for="masquer_cookies">Masquer les cookies?</label></li>
						<li class="center"><?php va_et_vient('masquer_cookies', 'oui', get_option('dsfr_masquer_cookies') ); ?></li>
						<?php if (get_option('dsfr_masquer_cookies') === 'non') { ?>		
						<li class="_masquer_cookies">
							<label for="afficher_bouton_cookies">Afficher le lien des cookies dans le footer</label><br>
							<?php $options = array('oui', 'non'); ?>
							<select name="afficher_bouton_cookies" id="afficher_bouton_cookies">
								<?php foreach($options as $option): ?>
								<option value="<?php echo $option; ?>" <?php selected(get_option('dsfr_afficher_bouton_cookies'), $option); ?>><?php echo $option; ?></option>
								<?php endforeach; ?>
							</select>
						</li>			
						<li class="_masquer_cookies">
							<ul>
								<li>  
									<label for="liste_des_cookies">Liste des cookies:</label>
									<textarea class="width100" name="liste_des_cookies" id="liste_des_cookies" cols="33" rows="8" required="required"><?php echo esc_attr(get_option('dsfr_liste_des_cookies')); ?></textarea>
								</li>
							</ul>
						</li>
						<li class="_masquer_cookies">
							<label for="modifier_texte_cookies">Modifier le texte des cookies ?</label><br>
							<?php $options = array('non', 'oui'); ?>
							<select name="modifier_texte_cookies" id="modifier_texte_cookies" data-information="select2options" data-remove="non" data-parent="masquer_cookies" data-link="<?php echo get_template_directory_uri(); ?>">
								<?php foreach($options as $option): ?>
								<option value="<?php echo $option; ?>" <?php selected(get_option('dsfr_modifier_texte_cookies'), $option); ?>><?php echo $option; ?></option>
								<?php endforeach; ?>
							</select>
						</li>
						<?php if ((get_option('dsfr_modifier_texte_cookies') !== false) AND (get_option('dsfr_modifier_texte_cookies') === 'oui')) { ?>
						<li class="_masquer_cookies gestion_masquer_cookies_select2options">
							<label for="titre_texte_cookies">Titre pour le texte des cookies:</label><br>							
							<input class="width100" type="text" name="titre_texte_cookies" id="titre_texte_cookies" value="<?php echo esc_attr(get_option('dsfr_titre_texte_cookies')); ?>" placeholder="Ex : A propos des cookies"/>
						</li>
						<li class="_masquer_cookies gestion_masquer_cookies_select2options">
							<label for="texte_cookies">Texte pour les cookies</label>
							<textarea class="width100" name="texte_cookies" id="texte_cookies" cols="33" rows="8"><?php echo esc_attr(get_option('dsfr_texte_cookies')); ?></textarea>							
						</li>
						<?php } ?>
						<?php } ?>
					</ul>
				</li>
				<li class="encadre aide_masquer_partenaires">
					<ul><li><button class="questionmark" type="button" data-parent="masquer_partenaires">?</button></li></ul>
					<ul class="gestion_masquer_partenaires masquer_partenaires_primaire">
						<li class="center bold"><label for="masquer_partenaires">Masquer les partenaires?</label></li>
						<li class="center"><?php va_et_vient('masquer_partenaires', 'oui', get_option('dsfr_masquer_partenaires') ); ?></li>
						<?php if (get_option('dsfr_masquer_partenaires') === 'non') { ?>	
						<li class="_masquer_partenaires">
							<label for="titre_texte_cookies">Titre du bloc Partenaires:</label><br>							
							<input class="width100" type="text" name="dsfr_titre_bloc_partenaires" id="dsfr_titre_bloc_partenaires" value="<?php echo esc_attr(get_option('dsfr_titre_bloc_partenaires')); ?>" placeholder="Exemple : Nos partenaires"/>
						</li>
						<li class="_masquer_partenaires">

							<h3>Les partenaires principaux</h3>
							<ul class="gestion_partenaires_principaux" style="display:grid;">
								<li class="nombre_de_partenaires_principaux">
									<label for="nombre_de_partenaires_principaux">Nombre de partenaires principaux</label><br>
									<?php $options = array('0', '1', '2');?>
									<select name="nombre_de_partenaires_principaux" id="nombre_de_partenaires_principaux" data-elementname="partenaires_principaux" data-parent="partenaires_principaux" data-select="numeric" data-select-0="0">
										<?php foreach($options as $option): ?>
										<option value="<?php echo $option; ?>" <?php selected(get_option('dsfr_nombre_de_partenaires_principaux'), $option); ?>><?php echo $option; ?></option>
										<?php endforeach; ?>
									</select>
								</li>
								<?php if (get_option('dsfr_nombre_de_partenaires_principaux') > 1) { ?>
								<li class='partenaires_principaux_1' style="order:1;">
									<ul class="l8020">
										<li class="partenaires_principaux">
											<label for="partenaires_principaux_1">Partenaire principal 1:</label>
											<input class="width100" type="text" name="partenaires_principaux_1" id="partenaires_principaux_1" placeholder="Nom partenaire;url partenaire;url image partenaire" value="<?php echo esc_attr(get_option('dsfr_partenaires_principaux_1')); ?>" required="required"/>
										</li>
										<li class="partenaires_principaux">
											<button class="configurer_element" type="button" title="Configurer cet élément" data-parent="masquer_partenaires"  data-fille="partenaires_principaux_1" data-type-generateur="simple-image"><img src="<?php echo get_template_directory_uri().'/assets/images/settings.svg'; ?>" width="12" height="12"></button>
										</li>
									</ul>
								</li>
								<?php } ?>
								<?php if (get_option('dsfr_nombre_de_partenaires_principaux') > 1) {
								
									$nbr_menu =  get_option('dsfr_nombre_de_partenaires_principaux');                 
									for($i=2; $i<$nbr_menu+1; $i++){
										$pprincipal = 'partenaires_principaux_'.$i;
										$bdd_principal = 'dsfr_'.$pprincipal;
										echo '
										<li class="'.$pprincipal.'" style="order:'.$i.';">
											<ul class="l8020">
												<li class="partenaires_principaux">
													<label for="partenaires_principaux_'.$i.'">Partenaire principal '.$i.':</label>
													<input placeholder="Nom partenaire;url partenaire;url image partenaire"class="width100" type="text" name="partenaires_principaux_'.$i.'" id="partenaires_principaux_'.$i.'" value="'. esc_attr(get_option($bdd_principal)).'"  required="required"/>
												</li>
												<li class="partenaires_principaux_'.$i.'">
													<button class="configurer_element" type="button" title="Configurer cet élément" data-parent="masquer_partenaires" data-fille="partenaires_principaux_'.$i.'" data-type-generateur="simple-image"><img src="'.get_template_directory_uri().'/assets/images/settings.svg" width="12" height="12"></button>
												</li>
											</ul>
										</li>';
									}
									}?>
							</ul>
							<h3>Les partenaires secondaires</h3>
							<ul class="gestion_partenaires_secondaires" style="display:grid;">
								<li class="nombre_de_partenaires_secondaires">
									<label for="nombre_de_partenaires_secondaires">Nombre de partenaires secondaires</label><br>
									<?php $options = array('0', '1', '2', '3', '4'); ?>
									<select name="nombre_de_partenaires_secondaires" id="nombre_de_partenaires_secondaires" data-elementname="partenaires_secondaires" data-parent="partenaires_secondaires" data-select="numeric" data-select-0="0">
										<?php foreach($options as $option): ?>
										<option value="<?php echo $option; ?>" <?php selected(get_option('dsfr_nombre_de_partenaires_secondaires'), $option); ?>><?php echo $option; ?></option>
										<?php endforeach; ?>
									</select>
								</li>
								<?php if (get_option('dsfr_nombre_de_partenaires_secondaires') > 0) { ?>
								<li class="partenaires_secondaires_1" style="order:1;">
									<ul class="l8020">
										<li class="partenaires_secondaires" >
											<label for="partenaires_secondaires_1">Partenaire secondaire 1:</label>
											<input class="width100" type="text" name="partenaires_secondaires_1" id="partenaires_secondaires_1" placeholder="Nom partenaire;url partenaire;url image partenaire" value="<?php echo esc_attr(get_option('dsfr_partenaires_secondaires_1')); ?>" />
										</li>
										<li class="partenaires_secondaires_1">
											<button class="configurer_element" type="button" title="Configurer cet élément" data-parent="masquer_partenaires" data-fille="partenaires_secondaires_1" data-type-generateur="simple-image"><img src="<?php echo get_template_directory_uri().'/assets/images/settings.svg'; ?>" width="12" height="12"></button>
										</li>
									</ul>
								</li>
								<?php } ?>
								<?php 
									if (get_option('dsfr_nombre_de_partenaires_secondaires') > 1) {
										$nbr_menu =  get_option('dsfr_nombre_de_partenaires_secondaires');                 
										for($i=2; $i<$nbr_menu+1; $i++){
											$partenaire_secondaire = 'dsfr_partenaires_secondaires_'.$i;

											echo '
											<li class="partenaires_secondaires_'.$i.'" style="order:'.$i.';">
												<ul class="l8020">
													<li class="partenaires_secondaires">
														<label for="partenaires_secondaires_'.$i.'">Partenaire secondaire '.$i.':</label>
														<input placeholder="Nom partenaire;url partenaire;url image partenaire"class="width100" type="text" name="partenaires_secondaires_'.$i.'" id="partenaires_secondaires_'.$i.'" value="'. esc_attr(get_option($partenaire_secondaire)).'" />
													</li>
													<li class="partenaires__secondaires_'.$i.'">
														<button class="configurer_element" type="button" title="Configurer cet élément" data-parent="masquer_partenaires" data-fille="partenaires_secondaires_'.$i.'" data-type-generateur="simple-image"><img src="'.get_template_directory_uri().'/assets/images/settings.svg" width="12" height="12"></button>
													</li>
												</ul>
											</li>';
										}
									}
								?>	
							</ul>
						</li>
						<?php } ?>
					</ul>
				</li>
				<li class="encadre aide_masquer_menu">
					<ul><li><button class="questionmark" type="button" data-parent="masquer_menu">?</button></li></ul>
					<ul class="masquer_menu_primaire">
						<li class="center bold"><label for="masquer_menu">Masquer menu?</label></li>
						<li class="center"><?php va_et_vient('masquer_menu', 'oui', get_option('dsfr_masquer_menu') ); ?></li>
						<li id="_masquer_menu">
							<ul class="gestion_masquer_menu gestion_menu masquer_menu_primaire" style="display:grid;">
								<li class="_masquer_menu nombre_de_menu menumembre">
									<label for="nombre_de_menu">Nombre de menu</label><br>
									<?php $options = array('1', '2', '3', '4', '5', '6', '7', '8', '9'); ?>
									<select name="nombre_de_menu" id="nombre_de_menu" data-select="numeric" data-parent="masquer_menu" data-elementname="menu">
										<?php foreach($options as $option): ?>
										<option value="<?php echo $option; ?>" <?php selected(get_option('dsfr_nombre_de_menu'), $option); ?>><?php echo $option; ?></option>
										<?php endforeach; ?>
									</select>
								</li>
								<?php if (get_option('dsfr_masquer_menu') === 'non') { ?>	
								<li class="_masquer_menu menumembre ">
									<ul class="l8020">
										<li>  
											<label for="menu_1">Menu 1:<span class="obligatoire"> (obligatoire)</span></label>
											<input class="width100" type="text" name="menu_1" id="menu_1" value="<?php echo esc_attr(get_option('dsfr_menu_1')); ?>" autocomplete="off"/>
										</li>
										<li class="_masquer_menu masquer_menu_1">
											<button class="configurer_element" type="button" title="Configurer cet élément" data-parent="menumembre" data-fille="menu_1" data-type-generateur="menu"><img src="<?php echo get_template_directory_uri().'/assets/images/settings.svg'; ?>" width="12" height="12"></button>
										</li>
									</ul>
								</li>
								<?php if ((get_option('dsfr_nombre_de_menu') !== false) and (get_option('dsfr_nombre_de_menu') > 1)) {
									$nbr_menu =  get_option('dsfr_nombre_de_menu');                 
									for($i=2; $i<$nbr_menu+1; $i++){
										$menu = 'menu_'.$i;
										$bdd_menu = 'dsfr_'.$menu;
										echo '
										<li class="_masquer_menu masquer_menu_'.$i.' ">
											<ul class="l8020">
												<li class="_masquer_menu menumembre ">
													<label for="menu_'.$i.'">Menu '.$i.':</label>
													<input class="width100" type="text" name="menu_'.$i.'" id="menu_'.$i.'" value="'. esc_attr(get_option($bdd_menu)).'" autocomplete="off"/>
												</li>
												<li class="_masquer_menu masquer_menu_'.$i.'">
													<button class="configurer_element" type="button" title="Configurer cet élément" data-parent="menumembre" data-fille="menu_'.$i.'" data-type-generateur="menu"><img src="'.get_template_directory_uri().'/assets/images/settings.svg" width="12" height="12"></button>
												</li>
											</ul>
										</li>';
									}
									}?>
								<?php } ?>
							</ul>
						</li>
					</ul>
				</li>
				<li class="encadre aide_masquer_liste_de_lien">
					<ul><li><button class="questionmark" type="button" data-parent="masquer_liste_de_lien">?</button></li></ul>				
					<ul class="gestion_masquer_liste_de_lien masquer_liste_de_lien_primaire">
						<li class="center bold"><label for="masquer_liste_de_lien">Masquer la liste de liens?</label></li>
						<li class="center"><?php va_et_vient('masquer_liste_de_lien', 'oui', get_option('dsfr_masquer_liste_de_lien') ); ?></li>
						<?php if (get_option('dsfr_masquer_liste_de_lien') === 'non') { ?>
						<li class="_masquer_liste_de_lien">
							<ul class="gestion_liste_de_liens" style="display:grid;">
								<li class="_masquer_liste_de_lien nbr_liste">
									<label for="nombre_de_liste_de_liens">Nombre de liste de liens</label><br>
									<?php $options = array('1', '2', '3', '4', '5', '6'); ?>
									<select name="nombre_de_liste_de_liens" id="nombre_de_liste_de_liens" data-elementname="liste_de_liens" data-select="numeric" data-parent="masquer_liste_de_lien">
										<?php foreach($options as $option): ?>
										<option value="<?php echo $option; ?>" <?php selected(get_option('dsfr_nombre_de_liste_de_liens'), $option); ?>><?php echo $option; ?></option>
										<?php endforeach; ?>
									</select>
								</li>
								<li class="_masquer_liste_de_lien liste_externe  masquer_liste_de_lien_1">
									<ul class="l8020">
										<li class="_masquer_liste_de_lien liste_externe">
											<label for="liste_de_liens_1">Liste de liens 1:<span class="obligatoire"> (obligatoire)</span></label>
											<input required="required" class="width100" type="text" name="liste_de_liens_1" id="liste_de_liens_1" value="<?php echo esc_attr(get_option('dsfr_liste_de_liens_1')); ?>"/>
										</li>
										<li class="_masquer_liste_de_lien _masquer_liste_de_lien_1">
											<button class="configurer_element" type="button" title="Configurer cet élément" data-parent="masquer_liste_de_lien_primaire" data-fille="liste_de_liens_1" data-type-generateur="liste_de_liens"><img src="<?php echo get_template_directory_uri().'/assets/images/settings.svg'; ?>" width="12" height="12"></button>
										</li>
									</ul>
								</li>
								<?php if ((get_option('dsfr_nombre_de_liste_de_liens') !== false) and (get_option('dsfr_nombre_de_liste_de_liens') > 1)) {
									$nbr_liste_de_liens =  get_option('dsfr_nombre_de_liste_de_liens');                 
									for($i=2; $i<$nbr_liste_de_liens+1; $i++){
										$liste_de_liens = 'liste_de_liens_'.$i;		
										$bdd_liste_de_liens = 'dsfr_'.$liste_de_liens;						
										echo '
										<li class="_masquer_liste_de_lien masquer_liste_de_lien_'.$i.' liste_externe">
											<ul class="l8020">
												<li class="_masquer_liste_de_lien liste_externe">
													<label for="liste_de_liens_'.$i.'">Liste de lien '.$i.':<span class="obligatoire"> (obligatoire)</span></label>
													<input required="required" class="width100" type="text" name="liste_de_liens_'.$i.'" id="liste_de_liens_'.$i.'" value="'. esc_attr(get_option($bdd_liste_de_liens)).'"/>
												</li>
												<li class="_masquer_liste_de_lien _masquer_liste_de_lien_'.$i.'">
													<button class="configurer_element" type="button" title="Configurer cet élément" data-parent="masquer_liste_de_lien_primaire" data-fille="liste_de_liens_'.$i.'" data-type-generateur="liste_de_liens"><img src="'.get_template_directory_uri().'/assets/images/settings.svg" width="12" height="12"></button>
												</li>
											</ul>
										</li>';
									}
									}?>
							</ul>
						</li>
						<?php } ?>	
					</ul>
				</li>
				<li class="encadre aide_masquer_haut_de_page">
					<ul><li><button class="questionmark" type="button" data-parent="masquer_haut_de_page">?</button></li></ul>
					<ul class="masquer_haut_de_page_primaire">
						<li class="center bold"><label for="masquer_haut_de_page">Masquer Back to top?</label></li>
						<li class="center"><?php va_et_vient('masquer_haut_de_page', 'oui', get_option('dsfr_masquer_haut_de_page') ); ?></li>
					</ul>
				</li>
				<li class="encadre aide_masquer_statistiques">
					<ul><li><button class="questionmark" type="button" data-parent="masquer_statistiques">?</button></li></ul>	
					<ul class="gestion_masquer_statistiques masquer_statistiques_primaire">
						<li class="center bold"><label for="masquer_statistiques">Masquer les statistiques?</label></li>
						<li class="center"><?php va_et_vient('masquer_statistiques', 'oui', get_option('dsfr_masquer_statistiques') ); ?></li>
						<?php if (get_option('dsfr_masquer_statistiques') === 'non') { ?>
						<li class="_masquer_statistiques">
							<ul>
								<li>
									<label for="script_statistiques">Script statistiques (sans &lt;script&gt; et &lt;/script&gt;):</label>
									<textarea name="script_statistiques" id="script_statistiques" cols="33" rows="8" class="width100"><?php echo esc_attr(get_option('dsfr_script_statistiques')); ?></textarea>
								</li>
								<li>
									<hr class="hr width100">
									<span class="bold">Notes:</span>
								</li>
								<li>Le script de statistiques ne sera appliqué que sur le domaine "<?php echo get_option('dsfr_nom_de_domaine_en_production');?>".<br>Pour modifier le domainesur lequel s'applique le script de statistiques, il faudra modifier le champ "Nom du domaine en production" ou <a href ="#dsfr_nom_de_domaine_en_production">Cliquer ici</a>
							</ul>
						</li>
						<?php } ?>
					</ul>
				</li>
				<li class="encadre aide_masquer_message_alerte">
					<ul><li><button class="questionmark" type="button" data-parent="masquer_message_alerte">?</button></li></ul>				
					<ul class="gestion_masquer_message_alerte masquer_message_alerte_primaire">
						<li class="center bold"><label for="masquer_message_alerte">Masquer le message d'alerte?</label></li>
						<li class="center"><?php va_et_vient('masquer_message_alerte', 'oui', get_option('dsfr_masquer_message_alerte') ); ?></li>
						<?php if(get_option('dsfr_masquer_message_alerte') === 'non'): ?>
						<li class="_masquer_message_alerte">
							<ul>
								<li>
									<label for="message_alerte">Contenu du message d'alerte:</label>
									<textarea name="message_alerte" id="message_alerte" cols="33" rows="8" class="width100"><?php echo esc_attr(get_option('dsfr_message_alerte')); ?></textarea>
								</li>
							</ul>
						</li>
						<li class="_masquer_message_alerte">
							<hr class="hr width100">
							<span class="bold">Notes DSFR:</span>
						</li>
						<li class="_masquer_message_alerte">
							Le bandeau d’information importante doit être utilisé uniquement pour une information primordiale et temporaire. (Une utilisation excessive ou continue risque de “noyer” le composant).<br><br>
							Le bandeau doit être visible sur toutes les pages du site, quelque soit l’appareil utilisé.<br><br>
							Pour une information vitale comme une alerte enlèvement, il est possible d’utiliser une bannière spécifique.
						</li>
						<?php endif; ?>
					</ul>
				</li>
				<li class="encadre aide_masquer_liens_ecosysteme">
					<ul>
						<li><button class="questionmark" type="button" data-parent="masquer_liens_ecosysteme">?</button></li>
					</ul>
					<ul class="gestion_masquer_liens_ecosysteme masquer_liens_ecosysteme_primaire">
						<li class="center bold"><label for="masquer_liens_ecosysteme">Masquer Liens écosystème constitutionnel?</label></li>
						<li class="center"><?php va_et_vient('masquer_liens_ecosysteme', 'non', get_option('masquer_liens_ecosysteme') ); ?></li>
						<li class="_masquer_liens_ecosysteme">
							<ul class="gestion_lien_eco ecosystem_links" id="ecosysteme" style="display:grid;">
								<li class="nombre_de_liens_ecosysteme ecosysteme">
									<label for="nombre_de_liens_ecosysteme">Nombre de liens écosystème</label><br>
									<?php $options = array('1', '2', '3', '4', '5', '6'); ?>
									<select name="nombre_de_liens_ecosysteme" id="nombre_de_liens_ecosysteme" disabled="disabled">
										<?php foreach($options as $option): ?>
										<option value="<?php echo $option; ?>" <?php selected(get_option('dsfr_nombre_de_liens_ecosysteme'), $option); ?>><?php echo $option; ?></option>
										<?php endforeach; ?>
									</select>
								</li>					
								<li class="ecosysteme"> 
									<ul class="l8020"> 
										<li class="ecosysteme"> 
											<label for="lien_eco_1">Lien 1:</label>
											<input class="width100" type="text" name="lien_eco_1" id="lien_eco_1" value="<?php echo esc_attr(get_option('dsfr_lien_eco_1')); ?>" required="required"/>
										</li>
										<li>
											<button class="configurer_element" type="button" title="Configurer cet élément" data-parent="ecosystem_links" data-fille="lien_eco_1" data-type-generateur="simple-icone"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/settings.svg" width="12" height="12"></button>
										</li>
									</ul>
								</li>
								<?php 
									if ((get_option('dsfr_nombre_de_liens_ecosysteme') !== false) and (get_option('dsfr_nombre_de_liens_ecosysteme') > 1)) {
										$nbr_lien_eco = get_option('dsfr_nombre_de_liens_ecosysteme');                 
										for($i=2; $i<$nbr_lien_eco+1; $i++){
										$lien_eco = 'lien_eco_'.$i;
										$bdd_lien_eco = 'dsfr_'.$lien_eco;
										echo '
											<li class="ecosysteme nombre_de_liens_ecosysteme_'.$i.' ">
												<ul class="l8020"> 
													<li class="ecosysteme"> 
														<label for="lien_eco_'.$i.'">Lien '.$i.':</label>
														<input class="width100" type="text" name="lien_eco_'.$i.'" id="lien_eco_'.$i.'" value="'. esc_attr(get_option($bdd_lien_eco)).'" required="required"/>
													</li>
													<li>
														<button class="configurer_element" type="button" title="Configurer cet élément" data-parent="ecosystem_links" data-fille="lien_eco_'.$i.'" data-type-generateur="simple-icone"><img src="'.get_template_directory_uri().'/assets/images/settings.svg" width="12" height="12"></button>
													</li>
												</ul>
											</li>';
										}
									}
								?>
							</ul>

						</li>
						<li class="_masquer_liens_ecosysteme">
							<hr class="hr width100">
							<span class="bold">Notes DSFR:</span>
						</li>
						<li class="_masquer_liens_ecosysteme">
							Il s'agit des 4 liens de références de l'écosystème institutionnel. Ces mentions sont obligatoires sur les sites institutionnels.<br><br>
							Leur ordre est défini par le décret n° 2017-638 du 26 avril 2017 relatif aux mentions légales des sites internet des services de communication au public en ligne de l'Etat, des collectivités territoriales et des personnes de droit public ou privé chargées d'une mission de service public administratif.<br><br>
							Le mécanisme de gestion de ces liens a été ajusté en fonction du caractère obligatoire de ces liens.<br><br>

						</li>

					</ul>
				</li>
				<li class="encadre aide_masquer_liens_legaux">
					<ul>
						<li><button class="questionmark" type="button" data-parent="masquer_liens_legaux">?</button></li>
					</ul>
					<ul class="gestion_masquer_liens_legaux masquer_liens_legaux_primaire">
						<li class="center bold"><label for="masquer_liens_legaux">Masquer les liens obligations légales?</label></li>
						<li class="center"><?php va_et_vient('masquer_liens_legaux', 'non', get_option('dsfr_masquer_liens_legaux') ); ?> </li>
						<?php if(get_option('dsfr_masquer_liens_legaux') === 'non') { ?>	
						<li class="_masquer_liens_legaux">
							<ul class="legallinks gestion_lien_legal" style="display:grid;">
								<li class="nombre_de_liens_obligations_legales legallinks">
									<label for="nombre_de_liens_obligations_legales">Nombre de liens obligation légale</label><br>
									<?php $options = array('1', '2', '3', '4', '5', '6', '7', '8', '9'); ?>
									<select name="nombre_de_liens_obligations_legales" id="nombre_de_liens_obligations_legales">
										<?php foreach($options as $option): ?>
										<option value="<?php echo $option; ?>" <?php selected(get_option('dsfr_nombre_de_liens_obligations_legales'), $option); ?>><?php echo $option; ?></option>
										<?php endforeach; ?>
									</select>
								</li>
								<li class="legallinks nombre_de_liens_obligations_legales_1 ">  
									<ul class="l8020"> 
										<li>  
											<label for="lien_legal_1">Lien 1:</label>
											<input class="width100" type="text" name="lien_legal_1" id="lien_legal_1" value="<?php echo esc_attr(get_option('dsfr_lien_legal_1')); ?>" required="required" />
										</li>
										<li>
											<button class="configurer_element" type="button" title="Configurer cet élément" data-parent="legallinks" data-fille="lien_legal_1" data-type-generateur="simple-icone"><img src="<?php echo get_template_directory_uri().'/assets/images/settings.svg'; ?>" width="12" height="12"></button>
										</li> 
									</ul>
								</li>
								<?php 
									if ((get_option('dsfr_nombre_de_liens_obligations_legales') !== false) and (get_option('dsfr_nombre_de_liens_obligations_legales') > 1)) {
										$nbr_lien_legal =  get_option('dsfr_nombre_de_liens_obligations_legales');                 
										for($i=2; $i<$nbr_lien_legal+1; $i++){
											$lien_legal = 'dsfr_lien_legal_'.$i;
											echo '
											<li class="legallinks nombre_de_liens_obligations_legales_'.$i.' ">
												<ul class="l8020">
													<li>
														<label for="lien_legal_'.$i.'">Lien légal '.$i.':</label>
														<input class="width100" type="text" name="lien_legal_'.$i.'" id="lien_legal_'.$i.'" value="'. esc_attr(get_option($lien_legal)).'" "/>
													</li>
													<li>
														<button class="configurer_element" type="button" title="Configurer cet élément" data-parent="legallinks" data-fille="lien_legal_'.$i.'" data-type-generateur="simple-icone"><img src="'.get_template_directory_uri().'/assets/images/settings.svg" width="12" height="12"></button>
													</li>
												</ul>
											</li>';
										}
									}
								?>
							</ul>
						</li>
						<li class="_masquer_liens_legaux">
							<hr class="hr width100">
							<span class="bold">Notes DSFR:</span>
						</li>
						<li class="_masquer_liens_legaux">
							Il s'agitde la liste de liens qui doit être définie en fonction du site<br><br>
							Toutefois, à la configuration de ce thème, le responsable devra porter attention aux liens & contenus suivants qui sont obligatoires : <br>- “accessibilité : non/partiellement/totalement conforme”,<br>- mentions légales,<br>- données personnelles et gestion des cookies<br><br>

						</li>
						<?php } ?>
					</ul>
				</li>
				<li class="encadre aide_masquer_licence">
					<ul><li><button class="questionmark" type="button" data-parent="masquer_licence">?</button></li></ul>				
					<ul class="gestion_masquer_licence masquer_licence_primaire">
						<li class="center bold"><label for="masquer_licence">Masquer le message de licence?</label></li>
						<li class="center"><?php va_et_vient('masquer_licence', 'non', get_option('dsfr_masquer_licence') ); ?></li>
						
						<li class="_masquer_licence">
							<ul>
								<li>
									<label for="dsfr_licence">Contenu du message de licence:</label>
									<textarea name="dsfr_licence" id="dsfr_licence" cols="33" rows="8" class="width100" placeholder="Ici votre texte pour le type de licence si besoin, laisser si pas de licence" required="required"><?php echo get_option('dsfr_licence'); ?></textarea>
								</li>
							</ul>
						</li>
						
						
					</ul>
				</li>
				<li class="encadre aide_masquer_description_footer">
					<ul><li><button class="questionmark" type="button" data-parent="masquer_description_footer">?</button></li></ul>				
					<ul class="gestion_masquer_description_footer masquer_description_footer_primaire">
						<li class="center bold"><label for="masquer_description_footer">Masquer la description dans le footer?</label></li>
						<li class="center"><?php va_et_vient('masquer_description_footer', 'oui',get_option('dsfr_masquer_description_footer') ); ?></li>
						<?php if(get_option('dsfr_masquer_description_footer') === 'non') { ?>	

							<li class="_masquer_description_footer">
								<ul>
									<li>
										<label for="dsfr_description_footer">Description apparaissant dans le footer:</label>
										<textarea name="dsfr_description_footer" id="dsfr_description_footer" cols="33" rows="8" class="width100" placeholder="Ce texte apparaît dans la description du footer" required="required"><?php echo get_option('dsfr_description_footer'); ?></textarea>
									</li>
								</ul>
							</li>
						<?php } ?>
						
					</ul>
				</li>
			</ul>
			
			<ul class="form_configuration">
				<li></li>
				<li>
					<p class="submitFormulaire">
						<input type="submit" name="submit" id="submit" class="button button-primary" value="Enregistrer les modifications">
					</p>
				</li>
			</ul>
		</form>
	</div>
<?php
	}
	add_action( 'admin_menu', 'register_ma_page_de_configuration' );
	
	// Générateur identifiant unique
	function generate_unique_id() {
	//$prefix = 'my_id_';
	$suffix = uniqid();
	$unique_id = $suffix;
	return $unique_id;
	}
	
	//Ajout du menu Configuration DSFR dans le backoffice
	function register_ma_page_de_configuration() {
	add_menu_page( 'Formulaire de configuration', 'Configuration DSFR', 'manage_options', 'form_config', 'page_de_configuration', 'dashicons-admin-generic' );
	//add_submenu_page( 'form_config', __('DSFR CSS Admin','dsfr-admin-css'), __('DSFR CSS Admin','dsfr-admin-css'), 'manage_options', 'form_config_submenu', 'dsfr_admin_css_admin_interface_render' );
	}
	function is_form_config_page() {
		global $pagenow;
		return (is_admin() && 'admin.php' == $pagenow && isset($_GET['page']) && 'form_config' == $_GET['page']);
	}
	function wpdocs_enqueue_custom_admin_style() {		
		wp_enqueue_style( 'style-admin', get_theme_file_uri() . '/admin/css/style-admin.css', false, generate_unique_id() ); //1.0.35
		wp_enqueue_script( 'dsfr-wordpress-admin-theme-default-custom-script', get_theme_file_uri(). '/admin/js/admin-theme-default-custom-script.js', array( 'jquery' ), generate_unique_id(), false ); //13.5
		if (is_form_config_page()) {
			wp_enqueue_style( 'custom-admin-style', get_theme_file_uri() . '/admin/css/admin-theme-style.css', false, generate_unique_id() ); //1.0.35
			wp_enqueue_script( 'dsfr-wordpress-admin-theme-script', get_theme_file_uri(). '/admin/js/admin-theme-script.js', array( 'jquery' ), generate_unique_id(), false ); //13.5
		}
	}
	add_action( 'admin_enqueue_scripts', 'wpdocs_enqueue_custom_admin_style' );
	
	// Debug function
	function pre($var,$var2=''){
	echo "<br>$var2<br><pre>";
	print_r($var);
	echo '</pre><br>';
	}
	function my_option_filter( $value, $option ) {
	if ( $option == 'dsfr_texte_marianne' ) {
	 $value = str_replace( "\n", '<br>', $value );
	}
	return $value;
	}
	add_filter( 'pre_update_option_dsfr_texte_marianne', 'my_option_filter', 10, 2 );
	
	function custom_document_title( $title_parts ) {
		// Modifier $title_parts['title'], $title_parts['page'], $title_parts['tagline'], $title_parts['site'] comme vous le souhaitez
		$title_parts['site'] = get_option('dsfr_titre_onglet_navigateur');
		return $title_parts;
	}
	add_filter( 'document_title_parts', 'custom_document_title' );
	
	function custom_error_pages( $error ) {
		if ( $error == 500 ) {
		  status_header(500);
		  include( get_theme_file_uri() . '/500.php' );
		  exit();
		}
	}
	add_action( 'wp_die_handler', 'custom_error_pages' );
	  
	// Ajoute une meta-box lorsque l'édition d'un post ou d'une page est chargée
	function ajout_custom_box() {
	   $screens = ['post', 'page'];
	   foreach ($screens as $screen) {
	       add_meta_box(
	           'parametres_theme_dsfr',           // Unique ID
	           'Paramètres du thème DSFR',           // Box title
	           'custom_box_html',              // Content callback, must be of type callable
	           $screen                         // Post type
	       );
	   }
	}
	add_action('add_meta_boxes', 'ajout_custom_box');
	
	// Le HTML pour la meta-box
	function custom_box_html($post) {
	   $value1 = get_post_meta($post->ID, '__cacher_date_publication', true);
	   $value2 = get_post_meta($post->ID, '__cacher_nom_auteur', true);
	   ?>
<div class="editor-post-taxonomies__hierarchical-terms-list" tabindex="0" role="group" aria-label="Catégories">
	<div class="editor-post-taxonomies__hierarchical-terms-choice">
		<div class="components-base-control components-checkbox-control css">
			<div class="components-base-control__field css">
				<span class="components-checkbox-control__input-container">
					<input id="_cacher_date_publication" name="_cacher_date_publication" class="components-checkbox-control__input" type="checkbox" value="oui" <?php checked($value1, 'oui'); ?>>
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" role="presentation" class="components-checkbox-control__checked" aria-hidden="true" focusable="false">
						<path d="M16.7 7.1l-6.3 8.5-3.3-2.5-.9 1.2 4.5 3.4L17.9 8z"></path>
					</svg>
				</span>
				<label class="components-checkbox-control__label" for="_cacher_date_publication">Cocher pour cacher la date de publication</label>
			</div>
		</div>
	</div>
</div>
<div class="editor-post-taxonomies__hierarchical-terms-list" tabindex="0" role="group" aria-label="Catégories">
	<div class="editor-post-taxonomies__hierarchical-terms-choice">
		<div class="components-base-control components-checkbox-control css">
			<div class="components-base-control__field css">
				<span class="components-checkbox-control__input-container">
					<input id="_cacher_nom_auteur" name="_cacher_nom_auteur" class="components-checkbox-control__input" type="checkbox" value="oui" <?php checked($value2, 'oui'); ?>>
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" role="presentation" class="components-checkbox-control__checked" aria-hidden="true" focusable="false">
						<path d="M16.7 7.1l-6.3 8.5-3.3-2.5-.9 1.2 4.5 3.4L17.9 8z"></path>
					</svg>
				</span>
				<label class="components-checkbox-control__label" for="_cacher_nom_auteur">Cocher pour cacher le nom de l'auteur</label>
			</div>
		</div>
	</div>
</div>
<?php
	}
	
	// Sauvegarde la valeur des cases à cocher lors de l'enregistrement du post
	function save_postdata($post_id) {
	    if (array_key_exists('_cacher_date_publication', $_POST)) {
	        update_post_meta(
	            $post_id,
	            '__cacher_date_publication',
	            $_POST['_cacher_date_publication']
	        );
	    } else {
	        update_post_meta(
	            $post_id,
	            '__cacher_date_publication',
	            'non'
	        );
	    }
	
	    if (array_key_exists('_cacher_nom_auteur', $_POST)) {
	        update_post_meta(
	            $post_id,
	            '__cacher_nom_auteur',
	            $_POST['_cacher_nom_auteur']
	        );
	    } else {
	        update_post_meta(
	            $post_id,
	            '__cacher_nom_auteur',
	            'non'
	        );
	    }
	}
	add_action('save_post', 'save_postdata');

	function custom_admin_footer_text() {
		echo 'Wordpress Thème DSFR<br>Développements effectués par <a href="mailto:mikael.folio@developpement-durable.gouv.fr?subject=DSFR Wordpress">Mikaël Folio CGDD/SDSED/BUN/Bureau Web</a>.<br>Version 2.0.0';
		
	}
	add_filter( 'admin_footer_text', 'custom_admin_footer_text' );

// Hook Nombre de reésultats par page de recherche
function limit_search_results_per_page( $query ) {
	if ( $query->is_search && !is_admin() ) {
		$query->set( 'posts_per_page', get_option('dsfr_elements_recherche') );
	}
	return $query;
	}
	add_action( 'pre_get_posts', 'limit_search_results_per_page' );
// Empêcher l'accès au backend pour les rôles "Subscriber"
function restrict_backend_access() {
    if (is_admin() && !current_user_can('edit_posts') && !wp_doing_ajax()) {
        wp_redirect(home_url());
        exit;
    }
}
add_action('admin_init', 'restrict_backend_access');

// Masquer la barre d'administration pour les rôles "Subscriber"
function hide_admin_bar() {
    if (current_user_can('subscriber')) {
        show_admin_bar(false);
    }
}
add_action('after_setup_theme', 'hide_admin_bar');

	function my_custom_enqueue_media() {
		wp_enqueue_media();
	}
	add_action('admin_enqueue_scripts', 'my_custom_enqueue_media');

	function custom_login_logo() {
		echo '<style type="text/css">
			.login h1 a {
				background-image: url(/wp-content/themes/a-wordpress-dsfr-theme-project-wdtp/assets/images/marianne.jpg) !important;
				background-size: contain !important;
				width: 100% !important;
			}
		</style>';
	}
	add_action('login_head', 'custom_login_logo');
	
	function custom_login_logo_url() {
		return home_url();
	}
	add_filter('login_headerurl', 'custom_login_logo_url');
	
	function custom_login_logo_url_title() {
		return get_bloginfo('name');
	}
	add_filter('login_headertext', 'custom_login_logo_url_title');



?>