<?php
/**
 * The template for displaying image attachments.
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="contenu" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<div class="entry-attachment">
							<?php
							/**
							 * Filter the default image attachment size.
							 *
							 * @since Wordpress_DSFR_Project 1.0
							 *
							 * @param string $image_size Image size. Default 'large'.
							 */
							$image_size = apply_filters( 'wordpress_dsfr_project_attachment_size', 'large' );

							echo wp_get_attachment_image( get_the_ID(), $image_size );
							?>

							<nav id="image-navigation" class="navigation image-navigation" role="navigation">
								<div class="nav-links">
									<div class="nav-previous"><?php previous_image_link( false, __( 'Previous Image', 'wordpress-dsfr-project' ) ); ?></div>
									<div class="nav-next"><?php next_image_link( false, __( 'Next Image', 'wordpress-dsfr-project' ) ); ?></div>
								</div><!-- .nav-links -->
							</nav><!-- #image-navigation -->
						</div><!-- .entry-attachment -->

						<?php
						if ( has_excerpt() ) :
							?>
							<div class="entry-caption">
								<?php the_excerpt(); ?>
							</div><!-- .entry-caption -->
							<?php
						endif;
						?>

						<?php the_content(); ?>
						<?php
						wp_link_pages(
							array(
								'before' => '<div class="page-links">' . __( 'Pages:', 'wordpress-dsfr-project' ),
								'after'  => '</div>',
							)
						);
						?>
					</div><!-- .entry-content -->

					<footer class="entry-footer">
						<?php
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
						?>
					</footer><!-- .entry-footer -->
				</article><!-- #post-<?php the_ID(); ?> -->

			<?php endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
?>
