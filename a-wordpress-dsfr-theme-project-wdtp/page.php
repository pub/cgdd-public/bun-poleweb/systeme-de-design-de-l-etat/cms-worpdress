<?php
/**
 * Template Name: Page personnalisée
 *
 * @package WordPress
 * @subpackage Nom de votre thème
 * @since Nom de votre thème 1.0
 */

get_header(); ?>

	<div id="contenu" class="content-area">
		<main id="contenu" class="site-main">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'template-parts/content/content', 'page' );

			// If comments are open or there is at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

			// End of the loop.
		endwhile;
		?>

		</main><!-- #contenu -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
?>
