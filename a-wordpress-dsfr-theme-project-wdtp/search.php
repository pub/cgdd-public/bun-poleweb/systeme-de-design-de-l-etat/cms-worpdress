<?php
/**
 * The template for displaying search results pages.
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */

 get_header(); ?>

<main role="main" id="contenu">  
  <div class="fr-container-fluid ds_banner" id="contenu">
    <div class="fr-container">
      <div class="fr-grid-row fr-grid-row--center fr-grid-row--gutters  fr-mb-3v">                
          <div class="fr-col-12 fr-col-md-12 fr-col-lg-8">
          <section id="search-section">
            <h1>Résultats de recherche pour : <?php echo get_search_query(); ?></h1>

            <!-- Formulaire de recherche -->
            <form action="<?php echo home_url( '/' ); ?>" method="get">
              <div class="fr-search-bar" id="search-64f81e8200035" role="search">
                <label class="fr-label" for="search-64f81e8200035-input">
                  Rechercher
                </label>
                <input type="hidden" name="page" value="recherche">
                <input type="hidden" name="lang" value="fr">
                <input class="fr-input" title="Rechercher" placeholder="Rechercher" type="search" id="search-64f81e8200035-input" name="s" required="" value="<?php echo $_GET['s'];?>">
                <button class="fr-btn" title="Rechercher">
                  Rechercher
                </button>
              </div>
            </form>
            <div class="resultats_par_page">Nombre de résultats par page : <?php echo get_option('dsfr_elements_recherche'); ?></div>
            <div class="nbresultats">il y a <?php echo $wp_query->found_posts; ?> résultats pour : <?php echo get_search_query(); ?></div>
            <?php if ( have_posts() ) : ?>
              <ul class="recherche-article">
                <?php while ( have_posts() ) : the_post(); ?>
                  <li class="fr-mb-5v">
                    <div class="fr-grid-row fr-grid-row--gutters fullpage">
                      <div class="fr-col-12 fr-col-md-12  fr-col-lg-12">
                        <div class="fr-card fr-enlarge-link fr-card--horizontal fr-card--sm  fr-mb-5v ">
                          <div class="fr-card__body">
                            <div class="fr-card__content fr-background-alt--green-bourgeon">
                              <p class="fr-card__title">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                              </p>
                              <div class="fr-card__start">
                                <small>Publié le <?php the_time('j F Y'); ?></small>
                              </div>
                              <div class="fr-card__desc">
                                <?php the_excerpt(); ?>
                              </div>
                            </div>
                          </div>
                          <div class="fr-card__header">
                            <div class="fr-card__img">
                              <?php if ( has_post_thumbnail() ) : ?>
                              <?php the_post_thumbnail(); ?>
                              <?php else : ?>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/marianne.svg" alt="Marianne" />
                              <?php endif; ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                <?php endwhile; ?>
              </ul>
              <!-- Pagination -->
              <div class="pagination flex fr-grid-row--center">
              <?php
                echo paginate_links( array(
                  'prev_text' => 'Précédent',
                  'next_text' => 'Suivant',
                ) );
              ?>
            </div>
            <?php else : ?>
              <p>Nous sommes désolés, mais votre recherche n'a donné aucun résultat pour : <?php echo get_search_query(); ?></p>
              <p>Voici quelques suggestions pour améliorer votre recherche :</p>
              <ul>
                <li><span class="strong">Vérifiez l'orthographe</span> : Assurez-vous que tous les mots sont correctement orthographiés.</li>
                <li><span class="strong">Utilisez des mots-clés différents</span> : Essayez avec des termes différents ou plus généraux.</li>
                <li><span class="strong">Éliminez les filtres</span> : Si vous avez appliqué des filtres de recherche, essayez de les enlever pour élargir votre recherche.</li>
                <li><span class="strong">Élargissez la zone géographique</span> : Si votre recherche est limitée à une certaine région, essayez d'élargir la zone.</li>
              </ul>
              <p>Vous pouvez également revenir à la <a href="/">page d'accueil</a> ou <a href="<?php echo get_option('dsfr_form_contact');?>">nous contacter</a> si le problème persiste.<p>
            <?php endif; ?>
          </section>
        </div>
      </div>
    </div>
  </div>
</main>

<?php get_footer(); ?>

</filesnippet>

