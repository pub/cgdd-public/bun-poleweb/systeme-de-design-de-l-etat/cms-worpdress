<?php
/**
 * The template for displaying search forms.
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */
?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'wordpress-dsfr-project' ); ?></span>
		<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'wordpress-dsfr-project' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	</label>
	<button type="submit" class="search-submit"><?php echo _x( 'Search', 'submit button', 'wordpress-dsfr-project' ); ?></button>
</form>
</div><!-- .site-content -->
<?php get_footer(); ?>
</body>
</html>
