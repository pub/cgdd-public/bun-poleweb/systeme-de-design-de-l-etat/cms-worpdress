<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
* @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */

 get_header(); ?>

 <main id="contenu" class="site-main" role="main">

	 <?php while ( have_posts() ) : the_post(); ?>

		 <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		 <?php get_template_part( 'template-parts/excerpt/excerpt', 'title' ); ?>

			 <div class="entry-content">
				 <?php 
				 the_content(); ?>
			 </div><!-- .entry-content -->
			 <?php get_template_part( 'template-parts/excerpt/excerpt', 'author' ); ?>
</div>

		 </article><!-- #post-<?php the_ID(); ?> -->

		 <?php
			 // Si les commentaires sont ouverts ou qu'il y a au moins un commentaire, affiche le modèle de commentaires
			 if ( comments_open() || get_comments_number() ) :
				 comments_template();
			 endif;
		 ?>

	 <?php endwhile; // Fin de la boucle principale. ?>

 </main><!-- #contenu -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>