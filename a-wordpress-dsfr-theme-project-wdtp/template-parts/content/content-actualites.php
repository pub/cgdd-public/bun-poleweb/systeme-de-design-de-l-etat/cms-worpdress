<?php
/**
 * Template part for displaying posts of the format "Actualités"
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php

		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
				<?php
				// Display post date
				echo '<span>' . get_the_date() . '</span>';

				// Display post author
				echo ' • <span class="byline">' . esc_html__( 'par', 'wp-theme-dsfr' ) . ' ' . get_the_author() . '</span>';
				?>
			</div><!-- .entry-meta -->
			<?php
		endif;
		?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
		the_content( sprintf(
			/* translators: %s: Name of current post. */
			wp_kses( __( 'Lire la suite<span class="screen-reader-text"> "%s"</span>', 'wp-theme-dsfr' ), array( 'span' => array( 'class' => array() ) ) ),
			get_the_title()
		) );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->