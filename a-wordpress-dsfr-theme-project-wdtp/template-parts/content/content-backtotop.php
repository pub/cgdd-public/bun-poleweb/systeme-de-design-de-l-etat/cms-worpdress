<?php
/**
 * Template part for displaying post archives and search results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
* @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */

?>
<div class="scroll-top"> 
    <a class="fr-btn fr-fi-arrow-up-line scroll-top__link dontshow" href="#top" id="topbtn">Haut de page</a>
</div>
