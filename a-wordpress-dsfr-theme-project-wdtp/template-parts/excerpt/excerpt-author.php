<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
* @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */
$auteur = get_post_meta( get_the_ID(), 'auteur_meta_key', true );
$cacher_auteur = get_post_meta($post->ID, '__cacher_nom_auteur', true);
 if($cacher_auteur !== 'oui') {
 ?>
 <section class="bloctitre  fr-mb-4v">
	<style>
		/*.darkmode{display:none;width:100%;}
		.lightmode{display:block;width:100%;}
		[data-fr-theme="dark"] .darkmode {display:block;margin-top:-1rem;}
		[data-fr-theme="dark"] .lightmode{display:none;margin-top:-1rem;}
		[data-fr-theme="dark"] .bg_dark_mode{background-color: #696969;}*/
		.fr-h4 {
		order: 0;
		margin-bottom: 1rem;
		}
		.fr-card.fr-enlarge-link:not(.fr-card--no-arrow) .fr-card__body > .fr-card__desc, .fr-card.fr-enlarge-link:not(.fr-card--no-arrow) .fr-card__body > .fr-card__title {
		margin-bottom: 1rem;
		}
		.fr-card.fr-enlarge-link:not(.fr-card--no-arrow) .fr-card__body > .fr-card__desc {
		margin-top: 1rem;
		}
		.fr-tags-group > li {
		line-height: initial;
		}
		.bloctitre .motcle:hover {
		background: var(--grey-50-1000);
		color: var(--background-contrast-grey);
		}
		.fr-tile__img {
		width:100%;
		height:100%;
		margin:0;
		}
	</style>
	<div class=" fr-pb-1v bg_dark_mode ">
		<div class="fr-grid-row fr-grid-row--gutters ">
			<div class="fr-col-12 fr-col-md-9">
				
			</div>
			<?php if (!$auteur) { ?>
				<div class="fr-col-12 fr-col-md-3" style="text-align:end;">
					<p class="fr-tag fr-icon-parent-fill fr-tag--icon-left fr-tag--sm fr-mt-3v">
						Publié par&nbsp;: <?php  echo get_the_author(); ?>
					</p>
				</div>
			<?php } ?>
		</div>
		<!--img src="/IMG/png/bandeau_sndi_light-min.png" class="lightmode">
		<img src="/IMG/png/bandeau_sndi_dark-min.png" class="darkmode"-->
	</div>
</section>
<?php } ?>