<?php
/**
 * Template part for displaying posts of the format "Actualités"
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */
?>

<?php 
function generate_unique_id() {
    //$prefix = 'my_id_';
    $suffix = uniqid();
    $unique_id = $suffix;
    return $unique_id;
    }
  $idgen = generate_unique_id();
?>
<ul class="affichage_generateur">
    <li>
        <ul class="form_configuration resultat_liste_de_lien dontshow les-resultats">
        <li></li>
        <li>
            <fieldset>
            <legend>Résultat obtenu</legend>
            <nav class="fr-nav" id="navigation-<?php echo $idgen; ?>" role="navigation" aria-label="Menu principal" data-fr-js-navigation="true">
                <ul class="fr-nav__list">
                <li class="fr-nav__item" data-fr-js-navigation-item="true">

                    <div class="fr-collapse fr-menu" id="menu-<?php echo $idgen; ?>" data-fr-js-collapse="true" style="display:none;">
                        <ul class="fr-menu__list">
                        <li><a class="fr-nav__link" href="#" target="_self">Sousmenu1</a></li>
                        </ul>
                    </div>
                </li>         
                </ul>
            </nav>
            </fieldset>
        </li>
        </ul>
        <ul>
            <li>
                <input type="hidden" name="type_generateur" id="type_generateur" value="<?php echo $_GET['typeGenerateur']; ?>" />
                <input type="hidden" name="element_parent" id="element_parent" value="<?php echo $_GET['componentFille']; ?>" />
                <label for="liste_de_liens_titre">Titre de la liste de lien ?</label><br>
                <input type="text" name="liste_de_liens_titre" id="liste_de_liens_titre" placeholder="Ex : Liste de liens 1" class="width100"/>
            </li>
            <li>
                <label for="liste_de_liens_entete">Entête titre liste de liens ?</label><br>
                <?php $options = array('h1', 'h2', 'h3', 'h4', 'h5', 'h6'); ?>
                <select name="liste_de_liens_entete" id="liste_de_liens_entete">
                <?php foreach($options as $option): ?>
                <option value="<?php echo $option; ?>" <?php if ($option === 'h2') {echo 'selected="selected"';}?>><?php echo $option; ?></option>
                <?php endforeach; ?>
                </select>      
	        </li>
            <li>
                <label for="liste_de_liens_icon">Icône:</label><br>
                <input type="text" name="liste_de_liens_icon" id="liste_de_liens_icon" class="width100" placeholder="Voir définition icones du DSFR"/>
            </li>
            <li>
                <label for="nombre_de_liens_dans_liste">Nombre de liens dans la liste?</label><br>
                <?php $options = array('1', '2', '3', '4', '5', '6'); ?>
                <select name="nombre_de_liens_dans_liste" id="nombre_de_liens_dans_liste" data-elementname="liste_de_liens"  data-emplacement="ajoutListeDeLiens">
                <?php foreach($options as $option): ?>
                <option value="<?php echo $option; ?>"><?php echo $option; ?></option>
                <?php endforeach; ?>
                </select>
            </li>
            <li class="gestion_ajoutListeDeLiens" style="display:grid;">
                <ul class="form_configuration ajoutListeDeLiens_1" style="order:1;">
                    <li>
                        <label for="liste_de_liens_titre_1"><span class="redbold">*</span> Titre lien 1:</label><br>
                        <input type="text" name="liste_de_liens_titre_1" id="liste_de_liens_titre_1" placeholder="Ex : Lien 1" class="width100 text" required="required">
                    </li>
                    <li>
                        <label for="liste_de_liens_url_1"><span class="redbold">*</span> Url lien 1:</label><br>
                        <input type="text" name="liste_de_liens_url_1" id="liste_de_liens_url_1" placeholder="Ex : https://lien_1 ou /lien_1" class="width100 text" required="required">
                    </li>
                </ul>
            </li>
        </ul>
    </li>
</ul>

