<?php
	/**
	 * Template part for megamenu
	 *
	 * @package WordPress
	 * @subpackage Wordpress_DSFR_Project
	 * @since Wordpress_DSFR_Project 1.0
	 */
	function generate_unique_id() {
        //$prefix = 'my_id_';
        $suffix = uniqid();
        $unique_id = $suffix;
        return $unique_id;
        }

	  $idgen = generate_unique_id();
	?>
<ul class="form_configuration resultat_menu_megamenu dontshow">
	<li></li>
	<li>
		<fieldset>
			<legend>Résultat obtenu</legend>
			<ul>
				<li>
					<button class="fr-nav__btn" aria-expanded="true" aria-controls="resultat_menu_megamenu" data-fr-js-collapse-button="true" id="titre_bouton_megamenu">Mégamenu</button>
					<ul class="megamenu_content">
						<li>
							<ul class="form_configuration">
								<li></li>
								<li style="text-align: end;margin-right: 2rem;"><button aria-expanded="true" aria-controls="megamenu_content" data-fr-js-collapse-button="true" class="megamenu_close">Fermer</button></li>
							</ul>
						</li>
						<li>
							<ul class="form_configuration">
								<li class="megamenu_image dontshow">
									<!-- /wp-content/themes/wordpress-dsfr-project/assets/images/theme.svg -->
									<img loading="lazy" class="arrondi" src="" alt="">
								</li> 
								<li>
									<ul class="megamenu_html">
										<li class="megamenu_titre_comtent">
											<h2 id="titre_megamenu">Titre</h2>
											<div class="fr-my-3v" id="description_de_megamenu">description</div>
											<a id="lien_categorie" class="fr-link fr-fi-arrow-right-line fr-link--icon-right" href="url de la catégorie">Titre url catégorie</a>
										</li>
										<li>
											<ul class="form_commandes colonnes_menu">
												<li class="blocmenu1 dontshow">
													<h3 class="fr-mega-menu__category">Titre 1er menu M1</h3>
													<ul class="fr-mega-menu__list" id="sousmenus_1" >
														<li><a class="fr-nav__link" href="url_ssmenu1Menu1" target="_self">Ssmenu1 M1</a></li>
													</ul>											
												</li>
												<li class="blocmenu2 dontshow">
													<h3 class="fr-mega-menu__category ">Titre 2nd menu M2</h3>
													<ul class="fr-mega-menu__list" id="sousmenus_2">
														<li><a class="fr-nav__link" href="url_ssmenu1Menu2" target="_self">Ssmenu1 M2</a></li>														
													</ul>
												</li>
												<li class="blocmenu3 dontshow">
													<h3 class="fr-mega-menu__category ">Titre 3eme menu M3</h3>
													<ul class="fr-mega-menu__list" id="sousmenus_3">
														<li><a class="fr-nav__link" href="url_ssmenu1Menu3" target="_self">Ssmenu1 M3</a></li>														
													</ul>
												</li>
											</ul>
										</li>
									</ul>
								</li>
							</ul>								
						</li>
					</ul>
				</li>
			</ul>
		</fieldset>
	</li>
</ul>
<ul class="form_commandes">
	<li>
		<label for="menu-megamenu-titre"><span class="redbold">*</span> Titre du menu megamenu:</label><br>
		<input type="text" name="menu-megamenu-titre" id="menu-megamenu-titre" class="width100" placeholder="Ex: Mégamenu" required="required"/><br><label for="contenu-megamenu-titre"><span class="redbold">*</span> Titre du megamenu:</label><br>
		<input type="text" name="contenu-megamenu-titre" id="contenu-megamenu-titre" class="width100" placeholder="Ex: Ceci est le tire du contenu de mon mégamenu" />
		<label for="entete_titre_megamenu">Entête titre Mégamenu ?</label><br>
		<?php $options = array('h1', 'h2', 'h3', 'h4', 'h5', 'h6'); ?>
		<select name="entete_titre_megamenu" id="entete_titre_megamenu">
		<?php foreach($options as $option): ?>
		<option value="<?php echo $option; ?>" <?php if ($option === '0') {echo 'selected="selected"';}?>><?php echo $option; ?></option>
		<?php endforeach; ?>
		</select>      
	</li>
	<li>
		<label for="menu-megamenu-description">Description du megamenu:</label><br>
		<textarea name="menu-megamenu-description" id="menu-megamenu-description" class="width100" placeholder="Ma description pour mon megamenu" rows="4"/></textarea>
	</li>
	<li>
		<label for="categorie-megamenu-titre">Titre de l'url de catégorie:</label><br>
		<input type="text" name="categorie-megamenu-titre" id="categorie-megamenu-titre" class="width100" placeholder="Ex: Catégorie" />
		<label for="categorie-megamenu-url">Lien de l'url de catégorie</label><br>
		<input type="text" name="categorie-megamenu-url" id="categorie-megamenu-url" class="width100" placeholder="Ex: https:// ou Lien directe" />
	</li>
	<li>
		<label for="menu-megamenu-image">Lien de l'image:</label><br>
		<input type="text" name="menu-megamenu-image" id="menu-megamenu-image" class="width100" placeholder="Lien de l'image"/>
	</li>
</ul>
<ul>
  <li>
    <label for="nombre_de_menus_megamenu">Nombre de blocs de menus ?</label><br>
    <?php $options = array('0', '1', '2', '3'); ?>
    <select name="nombre_de_menus_megamenu" id="nombre_de_menus_megamenu">
      <?php foreach($options as $option): ?>
      <option value="<?php echo $option; ?>" <?php if ($option === '0') {echo 'selected="selected"';}?>><?php echo $option; ?></option>
      <?php endforeach; ?>
    </select>      
  </li>
</ul>
<ul class="form_commandes megame_menublock dontshow">
<li class="colonne1 dontshow">
		<ul class="megamenu_menu_titre1">
			<li>
				<label for="menu-megamenu-titre-menu-bloc1"><span class="redbold">*</span> Nom du bloc menu 1:</label><br>
				<input type="text" id="menu-megamenu-titre-menu-bloc1" name="menu-megamenu-titre-menu-bloc1" placeholder="*Titre blocMenu 1" value="">
			</li>
			<li>
				<label for="nombre_de_menus_megamenu_bloc1">Nombre de menus du bloc 1?</label><br>
				<select name="nombre_de_menus_megamenu_bloc1" id="nombre_de_menus_megamenu_bloc1" class="nombre_de_menus_megamenu_bloc">
					<?php $options_menu1 = array('1', '2', '3', '4', '5', '6', '7', '8', '9'); ?>
					<?php foreach($options_menu1 as $options_menu1): ?>
						<option value="<?php echo $options_menu1; ?>"><?php echo $options_menu1; ?></option>
          			<?php endforeach; ?>
				</select>
			</li>
			<li class="menu-inputs1 menu_1_1">
				<input type="text" name="titre1menu1" placeholder="* Titre 1 menu 1">
				<input type="text" name="url1menu1" placeholder="* URL 1 menu 1">
			</li>
		</ul>
	</li>  
	<li class="colonne2 dontshow">
		<ul class="megamenu_menu_titre2">
			<li>
				<label for="menu-megamenu-titre-menu-bloc2"><span class="redbold">*</span> Nom du bloc menu 2:</label><br>
				<input type="text" id="menu-megamenu-titre-menu-bloc1" name="menu-megamenu-titre-menu-bloc2" placeholder="*Titre blocMenu 2" value="">
			</li>
			<li>
				<label for="nombre_de_menus_megamenu_bloc2">Nombre de menus du bloc 2?</label><br>
				<select name="nombre_de_menus_megamenu_bloc2" id="nombre_de_menus_megamenu_bloc2" class="nombre_de_menus_megamenu_bloc">
					<?php $options_menu2 = array('1', '2', '3', '4', '5', '6', '7', '8', '9'); ?>
					<?php foreach($options_menu2 as $options_menu2): ?>
          				<option value="<?php echo $options_menu2; ?>"><?php echo $options_menu2; ?></option>
          			<?php endforeach; ?>
				</select>
			</li>
			<li class="menu-inputs2 menu_2_1">
				<input type="text" name="titre1menu2" placeholder="* Titre 1 menu 2">
				<input type="text" name="url1menu2" placeholder="* URL 1 menu 2">
			</li>
		</ul>
	</li>  
	<li class="colonne3 dontshow">
		<ul class="megamenu_menu_titre3">
			<li>
				<label for="menu-megamenu-titre-menu-bloc3"><span class="redbold">*</span> Nom du bloc menu 3:</label><br>
				<input type="text" id="menu-megamenu-titre-menu-bloc3" name="menu-megamenu-titre-menu-bloc3" placeholder="Titre blocMenu 3" value="">
			</li>
			<li>
				<label for="nombre_de_menus_megamenu_bloc3">Nombre de menus du bloc 3?</label><br>
				<select name="nombre_de_menus_megamenu_bloc3" id="nombre_de_menus_megamenu_bloc3" class="nombre_de_menus_megamenu_bloc">
					<?php $options_menu3 = array('1', '2', '3', '4', '5', '6', '7', '8', '9'); ?>
					<?php foreach($options_menu3 as $options_menu3): ?>
						<option value="<?php echo $options_menu3; ?>"><?php echo $options_menu3; ?></option>
          			<?php endforeach; ?>
				</select>
			</li>
			<li class="menu-inputs3 menu_3_1">
				<input type="text" name="titre1menu3" placeholder="* Titre 1 menu 3">
				<input type="text" name="url1menu3" placeholder="* URL 1 menu 3">
			</li>
		</ul>
	</li>  
</ul>



<style>
  .dropdown-menu__image {
    margin: 0;
    width: 30%;
  }
  .fr-nav__list > *:first-child:nth-last-child(2) ~ *, .fr-nav__list > *:first-child:nth-last-child(3) ~ *, .fr-nav__list > *:first-child:nth-last-child(4) ~ * {
    margin-left: 1.25rem;
  }
  .dropdown-menu__image img, img.arrondi {
    border-radius: 1rem;
    max-width: 100%;
  }
  input[required]::placeholder {
  font-weight: bold;
}
fieldset {
  border: 1px solid #ccc;
  padding: 10px;
  min-height: 72px;
  margin-top: -80px;
}
.fr-nav__btn[aria-expanded="true"] {
  color: #000091;
  background-color: #e3e3fd;
  --idle: transparent;
  --hover: #c1c1fb;
  --active: #adadf9;
  padding: 0 1rem;
}
.fr-nav__link, .fr-nav__btn {
  font-size: 0.875rem;
  line-height: 1.5rem;
}
.fr-nav__link, .fr-nav__btn {
  padding: 0.75rem 0;
  font-size: 1rem;
  line-height: 1.5rem;
  text-align: left;
  --hover-tint: #f6f6f6;
  --active-tint: #ededed;
  color: #161616;
  padding: 0 1rem;
}
.fr-nav__btn {
  padding: 1rem;
  margin: 0;
  width: auto;
  height: 100%;
  min-height: 3.5rem;
  font-weight: normal;
  border: none;
  max-width: fit-content;
  background: #fff;
    background-color: rgb(255, 255, 255);
  padding: 0 1rem;
}
.megamenu_content{
	position: absolute;
	border: 1px solid #ccc;
	width: 1191px;
	background: #fff;
	left:0;
}
img.arrondi{
	border:1px solid #ddd;
	max-height:250px;
}
.megamenu_html{
	min-width: 950px;
}
.megamenu_content{
	padding-left: 2rem;
}
h2 {
  font-size: 2rem;
  line-height: 2.5rem;
}
.fr-mb-3v, .fr-my-3v {
  margin-bottom: 0.75rem !important;
  margin-top: 0.75rem !important;
}
.fr-mt-4v, .fr-mt-2w, .fr-my-4v, .fr-my-2w {
  margin-top: 1rem !important;
}
.colonnes_menu{
	margin-top: 2rem;
}
.colonens_menu h3{
	font-weight: 600;
}
.colonnes_menu .fr-mega-menu__list a{
	padding:0
}
.colonnes_menu .fr-mega-menu__list{
	margin-top: .5rem;
}
.megamenu_close{
	background: #fff;
	border: none;
}
.megamenu_close::after{
	content: " X";
}
</style>