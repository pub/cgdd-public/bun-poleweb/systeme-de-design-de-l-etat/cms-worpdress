<?php
/**
 * Template part for displaying posts of the format "Actualités"
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */
?>

<?php 
  function generate_unique_id() {
    //$prefix = 'my_id_';
    $suffix = uniqid();
    $unique_id = $suffix;
    return $unique_id;
    }
  $idgen = generate_unique_id();
?>

    <ul class="form_configuration resultat_deroulant dontshow">
      <li></li>
      <li>
        <fieldset>
          <legend>Résultat obtenu</legend>
          <nav class="fr-nav" id="navigation-<?php echo $idgen; ?>" role="navigation" aria-label="Menu principal" data-fr-js-navigation="true">
            <ul class="fr-nav__list">
              <li class="fr-nav__item" data-fr-js-navigation-item="true">
                <button class="fr-nav__btn menu_deroulant_btn" aria-expanded="false" aria-controls="menu-<?php echo $idgen; ?>" data-fr-js-collapse-button="true">Menu déroulant</button>
                <div class="fr-collapse fr-menu" id="menu-<?php echo $idgen; ?>" data-fr-js-collapse="true" style="display:none;">
                    <ul class="fr-menu__list">
                      <li><a class="fr-nav__link" href="https://dsfr.digital-vision-pro.com/1" target="_self">Sousmenu1</a></li>
                    </ul>
                </div>
              </li>         
            </ul>
          </nav>
        </fieldset>
      </li>
    </ul>
    <ul>
      <li>
        <label for="menu_deroulant_titre"><span class="redbold">*</span> Titre du menu ?</label><br>
        <input type="text" name="menu_deroulant_titre" id="menu_deroulant_titre" placeholder="Ex : Menu 1" class="width100"/>
      </li>
      <li>
        <label for="menu_deroulant_icon">Icône du menu:</label><br>
        <input type="text" name="menu_deroulant_icon" id="menu_deroulant_icon" class="width100" placeholder="Voir définition icones du DSFR"/>
      </li>
      <li>
        <label for="nombre_de_sousmenus_deroulant">Nombre de sous-menus ?</label><br>
        <?php $options = array('1', '2', '3', '4', '6', '7', '8', '9'); ?>
        <select name="nombre_de_sousmenus_deroulant" id="nombre_de_sousmenus_deroulant">
          <?php foreach($options as $option): ?>
          <option value="<?php echo $option; ?>"><?php echo $option; ?></option>
          <?php endforeach; ?>
        </select>
      </li>
    </ul>
    <ul class="mes_menus_deroulants">
      <li>
      <ul class="form_configuration">
          <li>
            <label for="menuderoulant_titre1"><span class="redbold">*</span> Titre sousmenu 1:</label><br>
            <input type="text" name="menuderoulant_titre1" id="menuderoulant_titre1" placeholder="Ex : SousMenu 1" class="width100"/>
          </li>
          <li>
            <label for="menuderoulant_url1"><span class="redbold">*</span> Url sous-menu 1:</label><br>
            <input type="text" name="menuderoulant_url1" id="menuderoulant_url1" placeholder="Ex : https://lien_ssmenu1 ou /ssmenu1" class="width100"/>
          </li>          
        </ul>
      </li>
      <li class="mes_menus_deroulants_li">
        
      </li>
    </ul>

