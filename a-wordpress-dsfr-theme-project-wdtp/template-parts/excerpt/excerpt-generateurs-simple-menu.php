<?php
/**
 * Template part for displaying posts of the format "Actualités"
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */
?>

<?php 
  $current_url = wp_get_referer(); 
  $idgen = generate_unique_id();
?>
 <ul class="form_configuration resultat_menu_simple dontshow">
      <li></li>
      <li>
        <fieldset>
          <legend>Résultat obtenu</legend>
          <nav class="fr-nav" id="navigation-<?php echo $idgen; ?>" role="navigation" aria-label="Menu principal" data-fr-js-navigation="true">
            <ul class="fr-nav__list">
              <li class="fr-nav__item" data-fr-js-navigation-item="true">
                  <a href="xxxxxx" class="button-home ">xxxxxx</a>
              </li>         
            </ul>
          </nav>
        </fieldset>
      </li>
    </ul>
    <ul class="form_commandes">      
      <li>
        <label for="menu-simple-titre"><span class="redbold">*</span> Nom du menu:</label><br>
        <input type="text" name="menu-simple-titre" id="menu-simple-titre" class="width100" placeholder="Ex: Accueil" autocomplete="off"/>
      </li>
      <li>
        <label for="menu-simple-url"><span class="redbold">*</span> URL du menu:</label><br>
        <input type="text" name="menu-simple-url" id="menu-simple-url" class="width100" placeholder="Ex: https://... ou /xxxxxx" autocomplete="off"/>
      </li>
      <li>
        <label for="menu-simple-icon">icon du menu:</label><br>
        <input type="text" name="menu-simple-icon" id="menu-simple-icon" class="width100" placeholder="Voir définition icones du DSFR" autocomplete="off"/>
      </li>
    </ul>


