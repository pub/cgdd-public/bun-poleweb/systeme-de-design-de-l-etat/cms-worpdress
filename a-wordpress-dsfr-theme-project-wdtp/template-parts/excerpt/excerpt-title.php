<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */
	$date_de_publication = get_post_meta( get_the_ID(), 'date_meta_key', true ); 
	$cacher_date_publication = get_post_meta($post->ID, '__cacher_date_publication', true);
?>
 <section class="bloctitre  fr-mb-4v">

<div class=" fr-pb-1v bg_dark_mode ">
	<div class="fr-grid-row fr-grid-row--gutters ">
		<div class="fr-col-12 fr-col-md-12">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<?php if($cacher_date_publication !== 'oui') { ?>
				<p class="fr-tag fr-fi-calendar-line fr-tag--icon-left fr-tag--sm fr-mt-3v">
					Publié le&nbsp;: <?php echo get_the_date( '', $date_de_publication ); ?>
				</p>
			<?php } ?>
		</div>
	</div>
	<!--img src="/IMG/png/bandeau_sndi_light-min.png" class="lightmode">
	<img src="/IMG/png/bandeau_sndi_dark-min.png" class="darkmode"-->
</div>
</section>
