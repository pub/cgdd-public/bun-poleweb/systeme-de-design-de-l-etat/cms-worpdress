<?php
/**
 * The footer for our theme.
 *
 * Displays the links list share section of the site.
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */

$nombre_de_liens_ecosysteme = get_option('dsfr_nombre_de_liens_ecosysteme');
    if (get_option('dsfr_masquer_description_footer') === 'non') {echo '<p class="fr-footer__content-desc">'.get_option('dsfr_description_footer').'</p>';}
    echo '
    <ul class="fr-footer__content-list">';
        for ($i = 1; $i <= $nombre_de_liens_ecosysteme; $i++) {
        $liste_de_liens_ecosysteme = explode(';', get_option('dsfr_lien_eco_'.$i));
            echo '
                <li class="fr-footer__content-item">
                    <a class="fr-footer__content-link" href="'.$liste_de_liens_ecosysteme[2].'">'.$liste_de_liens_ecosysteme[1].'</a>
                </li>
            ';
        }
echo '
    </u>';
?>
                         
