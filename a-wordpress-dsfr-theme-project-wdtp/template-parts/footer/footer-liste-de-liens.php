<?php
/**
 * The footer for our theme.
 *
 * Displays the links list share section of the site.
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */
echo '
<div class="fr-footer__top">
    <div class="fr-container">
        <div class="fr-grid-row fr-grid-row--start fr-grid-row--gutters">';      
            $nombre_de_liste_de_liens = get_option('dsfr_nombre_de_liste_de_liens');
            for ($i = 1; $i <= $nombre_de_liste_de_liens; $i++) {
                $liste_de_liens_non_traitee = explode(';', get_option('dsfr_liste_de_liens_'.$i));
                $titre_colonne = $liste_de_liens_non_traitee[0];
                $entete_titre_colonne = $liste_de_liens_non_traitee[1];
                // suppression des 2 éléments précédents de la liste
                $liste_de_liens_traitee = array_slice($liste_de_liens_non_traitee, 2);
                echo '
                <div class="fr-col-12 fr-col-sm-3 fr-col-md-2">
                    <'.$entete_titre_colonne.' class="fr-footer__top-cat">'.$titre_colonne.'</'.$entete_titre_colonne.'>
                    <ul class="fr-footer__top-list">';                       
                    for($j = 0; $j < count($liste_de_liens_traitee); $j++) {
                        if ($j % 2 == 0) {
                            $titre_liste_de_liens = $liste_de_liens_traitee[$j];
                        } else {
                            echo '<li>
                                <a class="fr-footer__top-link" href="'.$liste_de_liens_traitee[$j].'">'.$titre_liste_de_liens.'</a>
                            </li>';
                        }

                    }                        
                    echo '</ul>
                    
                </div>';
            }
    echo '
        </div>
    </div>
</div>';
?>
                         
