<?php
/**
 * The footer for our theme.
 *
 * Displays the newsletter section of the site.
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */

if (get_option('dsfr_masquer_la_newsletter') === 'non') { ?>
    <div class='fr-col-12 fr-col-md-<?php echo ((get_option('dsfr_masquer_partage_reseaux_sociaux') === 'non') ? '8' : '12'); ?>'> 
        <div class="fr-follow__newsletter"> 
            <div class="width100">
                <p class="fr-h5"><?php echo get_option('dsfr_titre_form_newsletter'); ?></p>
                <p class="fr-text--sm"><?php echo get_option('dsfr_slogan_form_newsletter'); ?></p>
            </div>
            <div class="width100">
                <?php if (get_option('dsfr_type_formulaire_newsletter') === 'formulaire') { ?>	
                    <form action="<?php echo get_option('dsfr_url_formulaire_newsletter'); ?>">
                        <div class="fr-input-group">
                            <label class="fr-label" for="<?php echo get_option('dsfr_nom_champ_input_form'); ?>">
                                Votre adresse électronique (ex. : nom@domaine.fr)
                            </label>
                            <div class="fr-input-wrap fr-input-wrap--addon">
                                <input class="fr-input" title="Votre adresse électronique (ex. : nom@domaine.fr)" autocomplete="email" aria-describedby="newsletter-email-hint-text newsletter-email-messages" placeholder="Votre adresse électronique (ex. : nom@domaine.fr)" id="<?php echo get_option('dsfr_nom_champ_input_form'); ?>" type="email" name="<?php echo get_option('dsfr_nom_champ_input_form'); ?>">
                                <button class="fr-btn width100" id="newsletter-button" title="S‘abonner à notre lettre d’information" type="submit">
                                    <?php echo get_option('dsfr_titre_bouton_sabonner'); ?>
                                </button>
                            </div>
                            <div class="fr-messages-group" id="newsletter-email-messages" aria-live="assertive">
                            </div>
                        </div>
                        <p id="newsletter-email-hint-text" class="fr-hint-text"><?php echo get_option('dsfr_description_newsletter'); ?></p>
                    </form>
                <?php } else { ?>
                    <button class="fr-btn width100" title="S‘abonner à notre lettre d’information">
                        <?php echo get_option('dsfr_titre_bouton_sabonner'); ?>
                    </button>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>