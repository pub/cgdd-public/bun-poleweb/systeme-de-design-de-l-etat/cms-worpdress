<?php
/**
 * The footer for our theme.
 *
 * Displays the liens obligations légales section of the site.
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */
if (get_option('dsfr_masquer_liens_legaux') === 'non') {
    $nombre_de_liens_obligations_legales = get_option('dsfr_nombre_de_liens_obligations_legales');

    for ($i = 1; $i <= $nombre_de_liens_obligations_legales; $i++) {
        $liste_de_liens_obligations_legales = explode(';', get_option('dsfr_lien_legal_'.$i));
        echo '<li class="fr-footer__bottom-item"><a class="fr-footer__bottom-link" href="'.$liste_de_liens_obligations_legales[2].'">'.$liste_de_liens_obligations_legales[1].'</a></li>';
    }
}
?>
                         
