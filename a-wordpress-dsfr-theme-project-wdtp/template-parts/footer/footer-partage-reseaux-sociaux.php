<?php
/**
 * The footer for our theme.
 *
 * Displays the social engine share section of the site.
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */

if (get_option('dsfr_masquer_partage_reseaux_sociaux') === 'non') { ?>
    <div class='fr-col-12 fr-col-md-<?php echo ((get_option('dsfr_masquer_la_newsletter') === 'non') ? '4' : '12'); ?>'>
        <div class="fr-follow__social">
            <p class="fr-h5"><?php echo get_option('dsfr_titre_partage_reseaux_sociaux'); ?></p>
            <ul class="fr-btns-group">
                <?php if ((get_option('dsfr_url_facebook') !== false) AND (!(empty(get_option('dsfr_url_facebook'))))) { ?>
                    <li>
                        <button class="fr-btn fr-icon-facebook-circle-fill fr-btn--icon-left wdsfr-button" data-url="<?php echo get_option('dsfr_url_facebook'); ?>" title="Suivez-nous sur Facebook - Nouvelle fenêtre">
                            Suivez-nous sur Facebook
                        </button>
                    </li>
                <?php } ?>
                <?php if ((get_option('dsfr_url_twitter') !== false) AND (!(empty(get_option('dsfr_url_twitter'))))) { ?>
                    <li>
                        <button class="fr-btn fr-icon-twitter-fill fr-btn--icon-left wdsfr-button" data-url="<?php echo get_option('dsfr_url_twitter'); ?>" title="Suivez-nous sur Twitter - Nouvelle fenêtre">
                            Suivez-nous sur Twitter
                        </button>	
                    </li>
                <?php } ?>
                <?php if ((get_option('dsfr_url_instagram') !== false) AND (!(empty(get_option('dsfr_url_instagram'))))) { ?>
                    <li>
                        <button class="fr-btn fr-icon-instagram-fill fr-btn--icon-left wdsfr-button" data-url="<?php echo get_option('dsfr_url_instagram'); ?>" title="Suivez-nous sur Instagram - Nouvelle fenêtre">
                            Suivez-nous sur Instagram
                        </button>	
                    </li>
                <?php } ?>
                <?php if ((get_option('dsfr_url_linkedin') !== false) AND (!(empty(get_option('dsfr_url_linkedin'))))) { ?>
                    <li>
                        <button class="fr-btn fr-icon-linkedin-box-fill fr-btn--icon-left wdsfr-button" data-url="<?php echo get_option('dsfr_url_linkedin'); ?>"  title="Suivez-nous sur Linkedin - Nouvelle fenêtre">
                            Suivez-nous sur Linkedin
                        </button>		
                    </li>
                <?php } ?>
                <?php if ((get_option('dsfr_url_youtube') !== false) AND (!(empty(get_option('dsfr_url_youtube'))))) { ?>
                    <li>										
                        <button class="fr-btn fr-icon-youtube-fill fr-btn--icon-left wdsfr-button" data-url="<?php echo get_option('dsfr_url_youtube'); ?>" title="Suivez-nous sur Youtube - Nouvelle fenêtre">
                            Suivez-nous sur Youtube
                        </button>			
                    </li>
                <?php } ?>
                <?php if ((get_option('dsfr_url_rss') !== false) AND (!(empty(get_option('dsfr_url_rss'))))) { ?>
                    <li>
                        <button class="fr-btn fr-icon-rss-fill fr-btn--icon-left wdsfr-button" data-url="<?php echo get_option('dsfr_url_rss'); ?>" title="Suivez-nous sur notre fil RSS">
                            Suivez-nous sur notre fil RSS
                        </button>										
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
<?php } ?>