<?php
/**
 * The footer for our theme.
 *
 * Displays the links list share section of the site.
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */
$nombre_de_liens_partenaires_principaux = get_option('dsfr_nombre_de_partenaires_principaux');
$nombre_de_partenaires_secondaires = get_option('dsfr_nombre_de_partenaires_secondaires');

echo '
    <div class="fr-footer__partners">
		<h2 class="fr-footer__partners-title">'.get_option('dsfr_titre_bloc_partenaires').'</h2>
			<div class="fr-footer__partners-logos">
				<div class="fr-footer__partners-main">
					<ul>';
                    for ($i = 1; $i <= $nombre_de_liens_partenaires_principaux; $i++) {
                        $liens_partenaires_principaux = explode(';', get_option('dsfr_partenaires_principaux_'.$i));
                        echo '
                            <li>';
                                if (!empty($liens_partenaires_principaux[3])) {
                                    echo '<a class="fr-footer__partners-link" href="'.$liens_partenaires_principaux[2].'" title="'.$liens_partenaires_principaux[1].'&nbsp;- Lien externe"><img class="fr-footer__logo" src="'.$liens_partenaires_principaux[3].'" alt="'.$liens_partenaires_principaux[1].'" style="width:200px;height:auto;"></a>';
                                } else {
                                    echo '<a class="fr-footer__bottom-link" href="'.$liens_partenaires_principaux[2].'" title="'.$liens_partenaires_principaux[1].'&nbsp;- Lien externe">'.$liens_partenaires_principaux[1].'</a>';
                                }
                            
                        echo '  </a><br>
                            </li>';
                    }
echo '
                    </ul>
				</div>
				<div class="fr-footer__partners-sub">
					<ul>';
                    for ($j = 1; $j <= $nombre_de_partenaires_secondaires; $j++) {
                        $liens_partenaires_secondaires = explode(';', get_option('dsfr_partenaires_secondaires_'.$j));
                        echo'
                            <li>
                                <a class="fr-footer__partners-link" href="'.$liens_partenaires_secondaires[2].'" title="'.$liens_partenaires_secondaires[1].'&nbsp;- Lien externe"><img class="fr-footer__logo" src="'.$liens_partenaires_secondaires[3].'" alt="'.$liens_partenaires_secondaires[1].'" style="width:200px;height:auto;"></a>
                            </li>';
                    }
echo '
					</ul>
				</div>
			</div>
		</div>';
?>
                         
