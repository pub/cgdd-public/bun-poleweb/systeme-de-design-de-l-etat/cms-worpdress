<?php
/**
 * The footer for our theme.
 *
 * Traitement concernant le second logo dans le footer
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */

 echo '<style>
            .second-logo-footer{
                height: 130px;
                width:auto;
                order:1;
                margin-left: 3rem;
            }
 </style>'; 
// Set the image ID
$image_id = get_option('dsfr_image_second_logo');


// Set the image size to fetch from metadata
$image_size = 'full';

// Get the media URL of image size $image_size from the image ID
$image_url = wp_get_attachment_image_src( $image_id, $image_size )[0];

// If image URL is empty, it means the image does not exist
if ( empty( $image_url ) ) {
    $image_url = get_template_directory_uri() . '/assets/images/second_logo.png';
} 
?>
<img src="<?php echo $image_url; ?>" alt="<?php echo get_option('dsfr_alternative_logo_secondaire'); ?>" class="second-logo-footer" />

