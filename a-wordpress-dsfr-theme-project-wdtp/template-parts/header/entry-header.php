<?php
/**
 * Displays the post header
 *
* @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */

mon_fil_dariane(); 
the_title( '<h1 class="entry-title">', '</h1>' );
