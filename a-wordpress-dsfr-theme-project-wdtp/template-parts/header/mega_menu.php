<?php 
// megamenu;Mégamenu;/wp-content/themes/wp-dsfr/images/image_du_mega_menu.svg;titre;h2;description;Titre url catégorie;url de la catégorie;menu1;Titre 1er menu M1;h3;Ssmenu1 M1;url_ssmenu1Menu1;menu2;Titre 2nd menu M2;h3;Ssmenu1 M2;url_ssmenu1Menu2;Ssmenu2 M2;url_ssmenu2Menu2;Ssmenu3 M2;url_ssmenu3Menu2;Ssmenu4 M2;url_ssmenu4Menu2;Ssmenu5 M2;url_ssmenu5Menu2;Ssmenu6 M2;url_ssmenu6Menu2;Ssmenu7 M2;url_ssmenu7Menu2;Ssmenu8 M2;url_ssmenu8Menu2;menu3;Titre 3eme menu M3;h3;Ssmenu1 M3;url_ssmenu1Menu3;Ssmenu2 M3;url_ssmenu2Menu3;menu3ss M3;url_ssmenu3Menu3;Ssmenu4 M3;url_ssmenu4Menu3
$local_url = 'https://' . $_SERVER['HTTP_HOST'].'/';
$current_url = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];   
$mon_menu_deroulant = explode(';', get_option($_SESSION['menu_a_afficher'])); 
$lien_image_megamenu = $mon_menu_deroulant[2];
$titre_megamenu = "<$mon_menu_deroulant[4] class='class=\"fr-h4 fr-mb-2v\"'>$mon_menu_deroulant[3]</$mon_menu_deroulant[4]>";
$description_megamenu = $mon_menu_deroulant[5];
$titre_categorie_megamenu = $mon_menu_deroulant[6];
$categorie_megamenu = (!empty($mon_menu_deroulant[6])) ? '<a class="fr-link fr-fi-arrow-right-line fr-link--icon-right" href="'.$mon_menu_deroulant[7].'">'.$titre_categorie_megamenu.'</a>' : '';

$id_menu = generate_unique_id();  
$tableau_original  = $mon_menu_deroulant;
// Nouveau tableau pour stocker les valeurs
$nouveaux_tableaux = array();
// Parcours du tableau d'origine
for ($i = 0; $i < count($tableau_original); $i++) {
    // Si le champ courant commence par "menu" et contient une des valeurs "menu1", "menu2" ou "menu3"
    if (strpos($tableau_original[$i], "menu") === 0 && in_array($tableau_original[$i], array("menu1", "menu2", "menu3"))) {
        // Récupération du numéro de tableau
        $numero_tableau = substr($tableau_original[$i], -1);
        // Nouveau tableau pour stocker les prochaines valeurs
        $nouveau_tableau = array();
        // Parcours des prochaines valeurs jusqu'au prochain "menu[n]"
        for ($j = $i + 1; $j < count($tableau_original); $j++) {
            if (strpos($tableau_original[$j], "menu") === 0) {
                break;
            } else {
                $nouveau_tableau[] = $tableau_original[$j];
            }
        }
        // Stockage du nouveau tableau dans le tableau associatif $nouveaux_tableaux
        $nouveaux_tableaux[$tableau_original[$i]] = $nouveau_tableau;		
    }
}
$nbr_menu_mega_menu = count($nouveaux_tableaux);
echo '
<style>
.dropdown-menu__image {
    margin: 0;
    width: 30%;
}
.fr-nav__list > *:first-child:nth-last-child(2) ~ *, .fr-nav__list > *:first-child:nth-last-child(3) ~ *, .fr-nav__list > *:first-child:nth-last-child(4) ~ * {
    margin-left: 1.25rem;
}
.dropdown-menu__image img, img.arrondi {
    border-radius: 1rem;
    max-width: 100%;
}
</style>
<li class="fr-nav__item" data-fr-js-navigation-item="true">
	<button class="fr-nav__btn" aria-expanded="false" aria-controls="mega-menu-439" data-fr-js-collapse-button="true">'.$mon_menu_deroulant[1].'</button>
	<div class="fr-collapse fr-mega-menu" id="mega-menu-439" data-fr-js-collapse="true" style="--collapse:-796px;">
		<div class="fr-container fr-container--fluid fr-container-lg">
			<div class="fr-grid-row fr-grid-row-lg--gutters">
				<div class="fr-col-12 fr-mb-n3v">
					<button class="fr-link--close fr-link" aria-controls="mega-menu-439" data-fr-js-collapse-button="true">Fermer</button>
				</div>
				<div class="fr-col-12  d-flex fr-grid-row fr-grid-row-lg--gutters">'.
					(!empty($lien_image_megamenu) ? '
						<div class="dropdown-menu__image fr-col-2 ">
							<img loading="lazy" class="arrondi" src="'.$lien_image_megamenu.'" alt="">
						</div>' : ''
					).'			
					<div class="fr-mega-menu__leader fr-pl-4w fr-col-12 fr-col-md-10">'.
						$titre_megamenu.'
						<div class="fr-my-3v">'.$description_megamenu.'</div>
						'.$categorie_megamenu.'
						<div class="fr-grid-row fr-grid-row-lg--gutters fr-mt-4v">';
							for($nbr_menu=1; $nbr_menu <= $nbr_menu_mega_menu; $nbr_menu++){
								$menu_traite = 'menu'.$nbr_menu;								
								$nbr_sous_menu = count($nouveaux_tableaux[$menu_traite]);

								$titre_menu_traite = $nouveaux_tableaux[$menu_traite][0];
								$balise_titre_menu_traite = $nouveaux_tableaux[$menu_traite][1];
								echo '<div class="fr-col-12 fr-col-lg-4 ">
										<'.$balise_titre_menu_traite.' class="fr-mega-menu__category ">
											<p class="fr-nav__link">'.$titre_menu_traite.'</p>
										</'.$balise_titre_menu_traite.'>
										<ul class="fr-mega-menu__list">';
								for($i=2; $i < $nbr_sous_menu; $i++ ){
									$lien_ou_url = $i % 2;
									if ($lien_ou_url === 0){
										$titre_sous_menu = $nouveaux_tableaux[$menu_traite][$i];
									}
									else{

										$new_url = $nouveaux_tableaux[$menu_traite][$i];

										if (strpos($nouveaux_tableaux[$menu_traite][$i], '/') === 0) {
											$new_url = str_replace('/', $local_url, $nouveaux_tableaux[$menu_traite][$i]);
										}
										echo '<li><a class="fr-nav__link" href="'.$new_url.'" target="_self" '.(($url_sans_parametres === $nouveaux_tableaux[$menu_traite][$i]) ? 'aria-current="page"' : '').'>'.$titre_sous_menu.'</a></li>'; 
									}
								}
								echo '</div>';
								
							}
							

						echo'
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</li>';
//pre($mon_menu_deroulant);
//pre($nouveaux_tableaux);

















/*
<li class="fr-nav__item" data-fr-js-navigation-item="true">
	<button class="fr-nav__btn" aria-expanded="false" aria-controls="mega-menu-439" data-fr-js-collapse-button="true">Économie</button>
	<div class="fr-collapse fr-mega-menu" id="mega-menu-439" data-fr-js-collapse="true" style="--collapse:-796px;">
		<div class="fr-container fr-container--fluid fr-container-lg">
			<div class="fr-grid-row fr-grid-row-lg--gutters">
				<div class="fr-col-12 fr-mb-n3v">
					<button class="fr-link--close fr-link" aria-controls="mega-menu-439" data-fr-js-collapse-button="true">Fermer</button>
				</div>
				<div class="fr-col-12  d-flex fr-grid-row fr-grid-row-lg--gutters">
					<div class="dropdown-menu__image fr-col-2 ">
						<img loading="lazy" class="arrondi" src="plugins-dist/theme-dsfr/dse_theme/images/themes/theme-5-navigation.svg" alt="">
					</div>
					<div class="fr-mega-menu__leader fr-pl-4w fr-col-12 fr-col-md-10">
						<h2 class="fr-h4 fr-mb-2v">Économie</h2>
						<div class="fr-my-3v">La transition écologique appelle de profondes mutations du monde économique et de ses acteurs, aussi bien de la part des entreprises que des consommateurs, dans tous les secteurs.
							<br>Ces changements sont technologiques, organisationnels, stratégiques. Ils concernent les modes de production, les modes de vie et de consommation, les savoir-faire et les compétences.
							<br>Pour impulser, orienter, accompagner ces changements, une transition écologique réussie doit s’appuyer sur différents leviers et instruments à destination des acteurs économiques.
						</div>
						<a class="fr-link fr-fi-arrow-right-line fr-link--icon-right" href="theme/economie">Voir le thème</a>
						<div class="fr-grid-row fr-grid-row-lg--gutters fr-mt-4v">
							<div class="fr-col-12 fr-col-lg-4 ">
								<h3 class="fr-mega-menu__category ">
									<p class="fr-nav__link">Outils économiques</p>
								</h3>
								<ul class="fr-mega-menu__list">
									<li><a class="fr-nav__link" href="themes/economie/article/la-fiscalite-environnementale">La fiscalité environnementale</a></li>
								</ul>
							</div>
							<div class="fr-col-12 fr-col-lg-4 ">
								<h3 class="fr-mega-menu__category ">
									<p class="fr-nav__link">Production et consommation responsables</p>
								</h3>
								<ul class="fr-mega-menu__list">
									<li><a class="fr-nav__link" href="themes/economie/article/l-economie-circulaire">L’économie circulaire</a></li>
									<li><a class="fr-nav__link" href="themes/economie/article/l-economie-de-la-fonctionnalite">L’économie de la fonctionnalité</a></li>
								</ul>
								<ul class="fr-mega-menu__list">
									<li><a class="fr-nav__link" href="themes/economie/article/l-utilisation-des-ressources-naturelles">L’utilisation des ressources naturelles</a></li>
								</ul>
								<ul class="fr-mega-menu__list">
									<li><a class="fr-nav__link" href="themes/economie/article/l-affichage-environnemental">L’affichage environnemental </a></li>
								</ul>
								<ul class="fr-mega-menu__list">
									<li><a class="fr-nav__link" href="themes/economie/article/le-commerce-equitable">Le commerce équitable</a></li>
								</ul>
								<ul class="fr-mega-menu__list">
									<li><a class="fr-nav__link" href="themes/economie/article/les-depenses-pour-la-protection-de-l-environnement">Les dépenses pour la protection de l’environnement</a></li>
								</ul>
								<ul class="fr-mega-menu__list">
									<li><a class="fr-nav__link" href="themes/economie/article/les-engagements-pour-la-croissance-verte-ecv">Les engagements pour la croissance verte (ECV)</a></li>
								</ul>
								<ul class="fr-mega-menu__list">
									<li><a class="fr-nav__link" href="themes/economie/article/les-nudges-appliques-a-l-economie-verte">Les nudges appliqués à l’économie verte</a></li>
								</ul>
								<ul class="fr-mega-menu__list">
									<li><a class="fr-nav__link" href="themes/economie/article/les-poles-de-competitivite">Les pôles de compétitivité</a></li>
								</ul>
							</div>
							<div class="fr-col-12 fr-col-lg-4 ">
								<h3 class="fr-mega-menu__category ">
									<p class="fr-nav__link">Secteurs économiques</p>
								</h3>
								<ul class="fr-mega-menu__list">
									<li><a class="fr-nav__link" href="themes/economie/article/l-ecoconception-des-produits">L’écoconception des produits</a></li>
								</ul>
								<ul class="fr-mega-menu__list">
									<li><a class="fr-nav__link" href="themes/economie/article/l-energie">L’énergie</a></li>
								</ul>
								<ul class="fr-mega-menu__list">
									<li><a class="fr-nav__link" href="themes/economie/article/le-tourisme">Le tourisme</a></li>
								</ul>
								<ul class="fr-mega-menu__list">
									<li><a class="fr-nav__link" href="themes/economie/article/l-agriculture">L’agriculture</a></li>
								</ul>
								<ul class="fr-mega-menu__list">
									<li><a class="fr-nav__link" href="themes/economie/article/les-dechets">Les déchets</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</li>


megamenu; images/image_du_mega_menu.svg;titre;h2;description;catégorie;url de la catégorie;menu1;Titre 1er menu M1;Ssmenu1 M1;url_ssmenu1Menu1;menu2;Titre 2nd menu M2;Ssmenu1 M2;url_ssmenu1Menu2;Ssmenu2 M2;url_ssmenu2Menu2;Ssmenu3 M2;url_ssmenu3Menu2;Ssmenu4 M2;url_ssmenu4Menu2;Ssmenu5 M2;url_ssmenu5Menu2;Ssmenu6 M2;url_ssmenu6Menu2;Ssmenu7 M2;url_ssmenu7Menu2;Ssmenu8 M2;url_ssmenu8Menu2;menu3;Titre 3eme menu M3;Ssmenu1 M3;url_ssmenu1Menu3;Ssmenu2 M3;url_ssmenu2Menu3;menu3ss M3;url_ssmenu3Menu3;Ssmenu4 M3;url_ssmenu4Menu3*/