<?php 
//deroulant;Menu déroulant;Sous-menu1;/4;Sous-menu 2;/3;Sous-menu3;/1;Sous-menu 4;/5;Sous-menu 5;/6
    $local_url = 'https://' . $_SERVER['HTTP_HOST'].'/';
    $current_url = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];   
    $mon_menu_deroulant = explode(';', get_option($_SESSION['menu']));
    $id_menu = generate_unique_id();    
    $number_instance_count = count($mon_menu_deroulant);
    $url_sans_parametres = strtok($_SERVER["REQUEST_URI"],'?'); // Récupération de l'URL sans les paramètres  
    $meme_url = false;
    $work_on_array_mon_menu_deroulant = $mon_menu_deroulant; 
    foreach ($work_on_array_mon_menu_deroulant as &$valeur) {
        if (substr($valeur, 0, 1) == '/') {
            $valeur = $local_url . substr($valeur, 1);
        }
        if ($valeur == $current_url) {
            $meme_url = true;
        }
    }
    echo '<li class="fr-nav__item">
            <button class="fr-nav__btn" aria-expanded="false" aria-controls="menu-'.$id_menu.'" data-fr-js-collapse-button="true" '.($meme_url ? 'aria-current="page"' : '').'>'.$work_on_array_mon_menu_deroulant[1].'</button>
            <div class="fr-collapse fr-menu" id="menu-'.$id_menu.'" data-fr-js-collapse="true">
                <ul class="fr-menu__list">';
                    for($nbr_ssmenu=2; $nbr_ssmenu<=$number_instance_count - 1; $nbr_ssmenu++){
                        // on regarde si c'est le lien ou l'url
                        $lien_ou_url = $nbr_ssmenu % 2;
                        if ($lien_ou_url === 0){
                            $titre_sous_menu = $mon_menu_deroulant[$nbr_ssmenu];
                        }
                        else{
                            if (strpos($mon_menu_deroulant[$nbr_ssmenu], '/') === 0) {
                                $new_url = str_replace('/', $local_url, $mon_menu_deroulant[$nbr_ssmenu]);
                            }
                            echo '<li><a class="fr-nav__link" href="'.$new_url.'" target="_self" '.(($url_sans_parametres === $mon_menu_deroulant[$nbr_ssmenu]) ? 'aria-current="page"' : '').'>'.$titre_sous_menu.'</a></li>'; 
                        }
                    }
                echo'
                </ul>
            </div>
          </li>';
    unset($_SESSION['menu']);
?>

