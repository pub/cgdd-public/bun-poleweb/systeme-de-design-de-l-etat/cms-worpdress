<?php 
    //direct;Un simple menu;/test
    $menu = $_SESSION['menu'];    
    $mon_menu = explode(';', get_option($menu));  
    $id_menu = generate_unique_id();  
    $local_url = home_url();
    $current_url = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];  
    $url_sans_parametres = strtok($_SERVER["REQUEST_URI"],'?'); // Récupération de l'URL sans les paramètres  
    //menu simple
    if ($mon_menu[0] === 'direct'){
        
        echo '<li class="fr-nav__item" data-fr-js-navigation-item="true"><a class="fr-nav__link" href="'.$mon_menu[2].'" target="_self" '.(($url_sans_parametres === $mon_menu[2]) ? 'aria-current="page"' : '').'>'.$mon_menu[1].'</a></li>';
    }
    // menu déroulant
    if ($mon_menu[0] === 'deroulant'){
        //deroulant;Menu déroulant;Sous-menu1;/4;Sous-menu 2;/3;Sous-menu3;/1;Sous-menu 4;/5;Sous-menu 5;/6   
        $number_instance_count = count($mon_menu);
        $url_sans_parametres = strtok($_SERVER["REQUEST_URI"],'?'); // Récupération de l'URL sans les paramètres  
        $meme_url = false;
        $work_on_array_mon_menu = $mon_menu; 

        foreach ($work_on_array_mon_menu as &$valeur) {
            if (substr($valeur, 0, 1) == '/') {
                $valeur = $local_url . substr($valeur, 1);
            }
            if ($valeur == $current_url) {
                $meme_url = true;
            }
        }
        echo '<li class="fr-nav__item">
                <button class="fr-nav__btn" aria-expanded="false" aria-controls="menu-'.$id_menu.'" data-fr-js-collapse-button="true" '.($meme_url ? 'aria-current="page"' : '').'>'.$work_on_array_mon_menu[1].'</button>
                <div class="fr-collapse fr-menu" id="menu-'.$id_menu.'" data-fr-js-collapse="true">
                    <ul class="fr-menu__list">';
                        for($nbr_ssmenu=4; $nbr_ssmenu<=$number_instance_count - 1; $nbr_ssmenu++){
                            // on regarde si c'est le lien ou l'url
                            $lien_ou_url = $nbr_ssmenu % 2;
                            if ($lien_ou_url === 0){
                                $titre_sous_menu = $mon_menu[$nbr_ssmenu];
                            }
                            else{
                                
                                if ((strpos($mon_menu[$nbr_ssmenu], '/') === 0) AND ($mon_menu[$nbr_ssmenu] === '/'))  {
                                    $new_url = str_replace('/', $local_url.'/', $mon_menu[$nbr_ssmenu]);
                                } else{
                                    $new_url = $mon_menu[$nbr_ssmenu];
                                }
                                echo '<li><a class="fr-nav__link" href="'.$new_url.'" target="_self" '.(($url_sans_parametres === $mon_menu[$nbr_ssmenu]) ? 'aria-current="page"' : '').'>'.$titre_sous_menu.'</a></li>'; 
                            }
                        }
                    echo'
                    </ul>
                </div>
            </li>';
    }
    // Mégamenu
    if ($mon_menu[0] === 'megamenu'){
        // megamenu;Mégamenu;/wp-content/themes/wp-dsfr/images/image_du_mega_menu.svg;titre;h2;description;Titre url catégorie;url de la catégorie;menu1;Titre 1er menu M1;h3;Ssmenu1 M1;url_ssmenu1Menu1;menu2;Titre 2nd menu M2;h3;Ssmenu1 M2;url_ssmenu1Menu2;Ssmenu2 M2;url_ssmenu2Menu2;Ssmenu3 M2;url_ssmenu3Menu2;Ssmenu4 M2;url_ssmenu4Menu2;Ssmenu5 M2;url_ssmenu5Menu2;Ssmenu6 M2;url_ssmenu6Menu2;Ssmenu7 M2;url_ssmenu7Menu2;Ssmenu8 M2;url_ssmenu8Menu2;menu3;Titre 3eme menu M3;h3;Ssmenu1 M3;url_ssmenu1Menu3;Ssmenu2 M3;url_ssmenu2Menu3;menu3ss M3;url_ssmenu3Menu3;Ssmenu4 M3;url_ssmenu4Menu3
 
        $lien_image_megamenu = $mon_menu[3];
        $titre_megamenu = "<$mon_menu[5] class='fr-h4 fr-mb-2v'>$mon_menu[4]</$mon_menu[5]>";
        $description_megamenu = $mon_menu[6];
        $titre_categorie_megamenu = $mon_menu[7];
        $categorie_megamenu = (!empty($mon_menu[7])) ? '<a class="fr-link fr-fi-arrow-right-line fr-link--icon-right" href="'.$mon_menu[8].'">'.$titre_categorie_megamenu.'</a>' : '';

        
        $tableau_original  = $mon_menu;
        // Nouveau tableau pour stocker les valeurs
        $nouveaux_tableaux = array();
        // Parcours du tableau d'origine
        for ($i = 0; $i < count($tableau_original); $i++) {
            // Si le champ courant commence par "menu" et contient une des valeurs "menu1", "menu2" ou "menu3"
            if (strpos($tableau_original[$i], "menu") === 0 && in_array($tableau_original[$i], array("menu1", "menu2", "menu3"))) {
                // Récupération du numéro de tableau
                $numero_tableau = substr($tableau_original[$i], -1);
                // Nouveau tableau pour stocker les prochaines valeurs
                $nouveau_tableau = array();
                // Parcours des prochaines valeurs jusqu'au prochain "menu[n]"
                for ($j = $i + 1; $j < count($tableau_original); $j++) {
                    if (strpos($tableau_original[$j], "menu") === 0) {
                        break;
                    } else {
                        $nouveau_tableau[] = $tableau_original[$j];
                    }
                }
                // Stockage du nouveau tableau dans le tableau associatif $nouveaux_tableaux
                $nouveaux_tableaux[$tableau_original[$i]] = $nouveau_tableau;		
            }
        }
        $nbr_menu_mega_menu = count($nouveaux_tableaux);
        echo '
        <style>
        .dropdown-menu__image {
            margin: 0;
            width: 30%;
        }
        .fr-nav__list > *:first-child:nth-last-child(2) ~ *, .fr-nav__list > *:first-child:nth-last-child(3) ~ *, .fr-nav__list > *:first-child:nth-last-child(4) ~ * {
            margin-left: 1.25rem;
        }
        .dropdown-menu__image img, img.arrondi {
            border-radius: 1rem;
            max-width: 100%;
        }
        </style>
        <li class="fr-nav__item" data-fr-js-navigation-item="true">
            <button class="fr-nav__btn" aria-expanded="false" aria-controls="mega-menu-'.$id_menu.'" data-fr-js-collapse-button="true">'.$mon_menu[2].'</button>
            <div class="fr-collapse fr-mega-menu" id="mega-menu-'.$id_menu.'" data-fr-js-collapse="true" style="--collapse:-796px;">
                <div class="fr-container fr-container--fluid fr-container-lg">
                    <div class="fr-grid-row fr-grid-row-lg--gutters">
                        <div class="fr-col-12 fr-mb-n3v">
                            <button class="fr-link--close fr-link" aria-controls="mega-menu-'.$id_menu.'" data-fr-js-collapse-button="true">Fermer</button>
                        </div>
                        <div class="fr-col-12  d-flex fr-grid-row fr-grid-row-lg--gutters">'.
                            (!empty($lien_image_megamenu) ? '
                                <div class="dropdown-menu__image fr-col-2 ">
                                    <img loading="lazy" class="arrondi" src="'.$lien_image_megamenu.'" alt="">
                                </div>' : ''
                            ).'			
                            <div class="fr-mega-menu__leader fr-pl-4w fr-col-12 fr-col-md-10">'.
                                $titre_megamenu.'
                                <div class="fr-my-3v">'.$description_megamenu.'</div>
                                '.$categorie_megamenu.'
                                <div class="fr-grid-row fr-grid-row-lg--gutters fr-mt-4v">';
                                
                                    for($nbr_menu=1; $nbr_menu <= $nbr_menu_mega_menu; $nbr_menu++){
                                        $menu_traite = 'menu'.$nbr_menu;
                                        
                                        $nbr_sous_menu = count($nouveaux_tableaux[$menu_traite]);
                                        
                                        $titre_menu_traite = $nouveaux_tableaux[$menu_traite][0];
                                        
                                        $balise_titre_menu_traite = $nouveaux_tableaux[$menu_traite][1];
                                        echo '<div class="fr-col-12 fr-col-lg-4 ">
                                                <'.$balise_titre_menu_traite.' class="fr-mega-menu__category ">
                                                    <p class="fr-nav__link">'.$titre_menu_traite.'</p>
                                                </'.$balise_titre_menu_traite.'>
                                                <ul class="fr-mega-menu__list">';
                                        for($i=2; $i < $nbr_sous_menu; $i++ ){                                            
                                            $lien_ou_url = $i % 2;
                                            if ($lien_ou_url === 0){
                                                $titre_sous_menu = $nouveaux_tableaux[$menu_traite][$i];

                                            }
                                            else{

                                                $new_url = $nouveaux_tableaux[$menu_traite][$i];

                                                if (strpos($nouveaux_tableaux[$menu_traite][$i], '/') === 0) {
                                                    $new_url = str_replace('/', $local_url, $nouveaux_tableaux[$menu_traite][$i]);
                                                }
                                                echo '<li><a class="fr-nav__link" href="'.$new_url.'" target="_self" '.(($url_sans_parametres === $nouveaux_tableaux[$menu_traite][$i]) ? 'aria-current="page"' : '').'>'.$titre_sous_menu.'</a></li>'; 
                                            }
                                        }
                                        echo '</div>';
                                        
                                    }
                                    

                                echo'
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>';
    }
    
    unset($_SESSION['menu']);
?>