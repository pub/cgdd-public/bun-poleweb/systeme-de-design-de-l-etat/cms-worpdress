<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */
	// Activer ou désactiver le message d'alerte (voir la page de configuration)
	if ((get_option('dsfr_masquer_message_alerte') !== false) AND (get_option('dsfr_masquer_message_alerte') === 'non')) { ?>
		<div class="fr-notice fr-notice--info">
			<div class="fr-container">
				<div class="fr-notice__body">
					<p class="fr-notice__title"><?php echo get_option('dsfr_message_alerte'); ?></p>
					<button class="fr-btn--close fr-btn" title="Masquer le message" onclick="const notice = this.parentNode.parentNode.parentNode; notice.parentNode.removeChild(notice)">
						Masquer le message
					</button>
				</div>
			</div>
		</div>
<?php 
    } 
?>