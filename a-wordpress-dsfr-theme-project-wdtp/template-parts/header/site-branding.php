<?php
/**
 * Displays header site branding
 *
 * @package WordPress
 */

$blog_info    = get_bloginfo( 'name' );
$description  = get_bloginfo( 'description', 'display' );
$show_title   = ( true === get_theme_mod( 'display_title_and_tagline', true ) );
$header_class = $show_title ? 'site-title' : 'screen-reader-text';

?>

<div class="fr-header__body">
	<div class="fr-container">
		<div class="fr-header__body-row">
			<div class="fr-header__brand fr-enlarge-link">
				<div class="fr-header__brand-top">
					<div class="fr-header__logo">
						<p class="fr-logo">
							<?php 
								$value = explode( "\r\n", get_option('dsfr_texte_marianne'));
								for($i=0; $i<count($value); $i++){
									echo $value[$i].'<br>';
								}
							?>
						</p>
					</div>
					<div class="fr-header__operator">
					<?php 
						if ((get_option('dsfr_masquer_logo_secondaire') !== false) AND (get_option('dsfr_masquer_logo_secondaire') === 'non')) {
							if ((get_option('dsfr_afficher_selection_type_logo') !== false) and (get_option('dsfr_afficher_selection_type_logo') === 'fichier')) { 
								set_query_var( 'image_size', 'thumbnail' );
								get_template_part( 'template-parts/header/header', 'second-logo', '114px' );
							} else {
								echo get_option('dsfr_code_second_logo');
							}
						}							
					?>
					</div>
					<div class="fr-header__navbar">
						<?php if ((get_option('dsfr_masquer_recherche') !== false) and (get_option('dsfr_masquer_recherche') === 'non')) { ?>
							<button class="fr-btn--search fr-btn" data-fr-opened="false" aria-controls="modal-2397" id="button-2398" title="Rechercher">
								Rechercher
							</button>
						<?php } ?>
						<button class="fr-btn--menu fr-btn" data-fr-opened="false" aria-controls="modal-577" aria-haspopup="menu" id="button-578" title="Menu">
							Menu
						</button>
					</div>
				</div>
				<div class="fr-header__service">
					<a href="<?php echo get_option('siteurl'); ?>" title="Retour à la page d'accueil">
						<?php 
							if ((get_option('dsfr_masquer_le_titre') !== false) AND (get_option('dsfr_masquer_le_titre') === 'non')){	
								if ((get_option('titre_du_site_en_h1') !== false) AND (get_option('titre_du_site_en_h1') === 'oui')){											
									echo '<h1 class="fr-header__service-title">'.get_option('dsfr_titre_du_site').'</h1>';
								} else {
									if ((get_option('dsfr_home_en_h1') !== false) AND (get_option('dsfr_home_en_h1') === 'oui')){
										if ( is_front_page() ) {
											echo '<h1 class="fr-header__service-title">'.get_option('dsfr_titre_du_site').'</h1>';
										} else{
											echo '<span class="fr-header__service-title">'.get_option('dsfr_titre_du_site').'</span>';
										}
									} else {
										echo '<span class="fr-header__service-title">'.get_option('dsfr_titre_du_site').'</span>';
									}
								}
							}
						?>
					</a>
					<?php 
						if ((get_option('dsfr_masquer_le_titre') !== false) AND (get_option('dsfr_masquer_le_titre') === 'non')){	
							if ((get_option('dsfr_masquer_le_slogan') !== false) AND (get_option('dsfr_masquer_le_slogan') === 'non')){	
								echo '<p class="fr-header__service-tagline">'.get_option('dsfr_slogan_du_site').'</p>';
							}
						}
					?>
				</div>
			</div>
			<div class="fr-header__tools">
				<?php 
					// Liste d'accès rapide
					if ((get_option('dsfr_masquer_liste_acces_rapide') !== false) AND (get_option('dsfr_masquer_liste_acces_rapide') === 'non')){	 
				 		echo '
						<div class="fr-header__tools-links">';

								echo '<ul class="fr-links-group">';
								if (get_option('dsfr_lien_dacces_rapide1') !== ''){										
									$lien_ar1 = explode(";", get_option('dsfr_lien_dacces_rapide1'));
									echo '<li><a class="fr-btn fr-btn--display '.$lien_ar1[3].'" href="'.$lien_ar1[2].'">'.$lien_ar1[1].'</a></li>';
								}		
								if ((get_option('dsfr_masquer_parametres_affichage') === 'non') || (get_option('dsfr_masquer_page_contact')	 === 'non')){
									if (get_option('dsfr_lien_dacces_rapide2') !== ''){
										$lien_ar2 = explode(";", get_option('dsfr_lien_dacces_rapide2'));
										echo '<li><a class="fr-btn fr-btn--display '.$lien_ar2[3].'" href="'.$lien_ar2[2].'">'.$lien_ar2[1].'</a></li>';
									}
								}
								if (get_option('dsfr_masquer_page_contact')	 === 'non'){		
									$contact_in = array("1", "2", "4", "5");
									if (in_array(get_option('dsfr_emplacement_contact'), $contact_in)) {			
										echo '<li><a class="fr-btn fr-btn--display fr-icon-mail-line" target="_self" href='.get_option('dsfr_form_contact').'>'.get_option('dsfr_titre_url_contact').'</a></li>';
									}
								}
								if ((get_option('dsfr_masquer_parametres_affichage') !== false) AND (get_option('dsfr_masquer_parametres_affichage') === 'non')){
									echo '<li><button class="fr-btn--display fr-btn parametres_affichage" aria-controls="fr-theme-modal" aria-describedby="fr-theme-modal" data-fr-opened="false" title="Paramètres d\'affichage" data-fr-js-modal-button="true">Paramètres d\'affichage</button></li>';
								} else {
									echo '<li>';
									if (get_option('dsfr_lien_dacces_rapide3') !== ''){
										$lien_ar3 = explode(";", get_option('dsfr_lien_dacces_rapide3'));
										echo '<li><a class="fr-btn fr-btn--display '.$lien_ar3[3].'" href="'.$lien_ar3[2].'">'.$lien_ar3[1].'</a></li>';
									}
									echo '</ul>';
								}									

						echo '</div>';
					}
				?>
				
				<?php if ((get_option('dsfr_masquer_recherche') !== false) AND (get_option('dsfr_masquer_recherche') === 'non')) { ?>
					<?php $id_search = generate_unique_id();?>
					<div class="fr-header__search fr-modal" id="modal-2397" aria-labelledby="button-2398">
						<div class="fr-container fr-container-lg--fluid">
						<button class="fr-btn--close fr-btn" aria-controls="modal-2397" id="button-2405" title="Fermer">Fermer</button>
							<form action="<?php echo home_url(); ?>" method="get">
								<div class="fr-search-bar" id="search-2396" role="search">
									<label class="fr-label" for="search-2396-input">
										Rechercher
									</label>
									<input type="hidden" name="page" value="recherche">
									<input type="hidden" name="lang" value="fr">									
									<input class="fr-input" aria-describedby="search-2396-input-messages" title="Rechercher" placeholder="Rechercher" type="search" id="search-2396-input" name="s" placeholder="Rechercher" required="">
									<div class="fr-messages-group" id="search-2396-input-messages" aria-live="polite">
									</div>
									<button class="fr-btn" id="search-btn-2407" title="Rechercher">
										Rechercher
									</button>
								</div>
							</form>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>










		