<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */
?>
<header id="header"  role="banner" class="fr-header">

	<?php get_template_part( 'template-parts/header/site-branding' ); ?>
	<?php get_template_part( 'template-parts/header/site-nav' ); ?>

</header><!-- #header -->
<?php get_template_part( 'template-parts/header/site-alert' ); ?>

 
<?php
if(!is_front_page()) {
     mon_fil_dariane();
}
?>

