<?php
/**
 * Displays the site navigation.
 *
* @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */
$local_url = home_url();
    $current_url = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];  
    $url_sans_parametres = strtok($_SERVER["REQUEST_URI"],'?'); // Récupération de l'URL sans les paramètres  

if(get_option('dsfr_masquer_menu') === 'non'){
	

		echo '
		<div class="fr-header__menu fr-modal" id="modal-577" aria-labelledby="button-578">
			<div class="fr-container">	
				<button class="fr-btn--close fr-btn" aria-controls="modal-577" title="Fermer">
					Fermer
				</button>	
				<div class="fr-header__menu-links"></div>		
				<nav class="fr-nav" id="navigation-581" role="navigation" aria-label="Menu principal">
					<ul class="fr-nav__list">';

		$nbr_menu_a_afficher = get_option('dsfr_nombre_de_menu');
		for ($i=1; $i <= $nbr_menu_a_afficher; $i++){
			$menu = 'dsfr_menu_'.$i;
			$menu_explode = explode(";", get_option($menu));
			$_SESSION['menu'] = $menu;
				get_template_part( 'template-parts/header/simple_menu');
		}
		if (get_option('dsfr_masquer_page_contact')	 === 'non'){		
			$contact_in = array("1", "2", "3", "6");
			if (in_array(get_option('dsfr_emplacement_contact'), $contact_in)) {			
				echo '<li class="fr-nav__item" data-fr-js-navigation-item="true"><a class="fr-nav__link" href='.get_option('dsfr_form_contact').'  target="_self" '.(($url_sans_parametres === get_option('dsfr_form_contact')) ? 'aria-current="page"' : '').'>'.get_option('dsfr_titre_url_contact').'</a></li>';
			}
		}
			echo '
					</ul>
				</nav>
			</div>
		</div>';

}