<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */
?>
<div class="fr-skiplinks" id="top">
	<nav class="fr-container" role="navigation" aria-label="Accès rapide">
		<ul class="fr-skiplinks__list">
			<li>
				<a class="fr-link" href="#SELF#header-navigation">Menu</a>
			</li>
			<li>
				<a class="fr-link" href="#SELF#contenu">Contenu</a>
			</li>
			<li>
				<a class="fr-link" href="#SELF#search-399">Moteur de recherche</a>
			</li>
			<li>
				<a class="fr-link" href="#SELF#footer">Pied de page</a>
			</li>
		</ul>
	</nav>
</div>